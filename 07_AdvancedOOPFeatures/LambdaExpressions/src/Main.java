import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Stream;

public class Main {
    private static String staffFile = "07_AdvancedOOPFeatures/LambdaExpressions/data/staff.txt";
    private static String dateFormat = "dd.MM.yyyy";

    public static void main(String[] args) {
        ArrayList<Employee> staff = loadStaffFromFile();
        Collections.sort(staff, (s1, s2) -> {
            if (s1.getSalary().compareTo(s2.getSalary()) != 0) {
                return s1.getSalary().compareTo(s2.getSalary());
            }
            return s1.getName().compareTo(s2.getName());
        });
        int gg = 0;
        for (Employee q : staff) {
            gg++;
            System.out.println(gg + ") " + q.toString());
        }
        System.out.println();
        Calendar calendar = new GregorianCalendar();
        calendar.set(2017, Calendar.JANUARY, 1, 0, 0, 1);
        long begin2017Year = calendar.getTimeInMillis();
        calendar.set(2017, Calendar.DECEMBER, 31, 23, 23, 59);
        long end2017Year = calendar.getTimeInMillis();

        staff.stream().filter(Objects::nonNull).filter(employee -> employee.getWorkStart().getTime() > begin2017Year && employee.getWorkStart().getTime() < end2017Year)
                .sorted((employee, t1) -> t1.getSalary().compareTo(employee.getSalary()))
                .max((employee, t1) -> employee.getSalary())
                .ifPresent(employee -> {
                    System.out.println("Cотрудник с максимальной зарплатой среди тех, кто пришёл в 2017 году.\n\"" + employee.toString() + "\"");
                });
    }

    private static ArrayList<Employee> loadStaffFromFile() {
        ArrayList<Employee> staff = new ArrayList<>();
        try {
            List<String> lines = Files.readAllLines(Paths.get(staffFile));
            for (String line : lines) {
                String[] fragments = line.split("\t");
                if (fragments.length != 3) {
                    System.out.println("Wrong line: " + line);
                    continue;
                }
                staff.add(new Employee(
                        fragments[0],
                        Integer.parseInt(fragments[1]),
                        (new SimpleDateFormat(dateFormat)).parse(fragments[2])
                ));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return staff;
    }
}