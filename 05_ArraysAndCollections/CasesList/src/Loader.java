import org.w3c.dom.ls.LSOutput;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Loader {

    private final static String LIST = "list";
    private final static String EXIT = "exit";
    private final static String DELETE = "delete";
    private final static String EDIT = "edit";
    private final static String ADD = "add";
    private final static String HELP = "help";


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean stop = true;

        List<String> list = new ArrayList<>();

        System.out.println(
                "Привет! Я - простое консольное Java-приложение.\n" +
                        "Напиши в командной строке help, и я раскажу " +
                        "тебе, что я умею :)"
        );

        while (stop) {
            String dataInput = scanner.nextLine().strip();
            /*//            Pattern pattern = Pattern.compile("\\w+");*/
            Pattern pattern = Pattern.compile(" +"); // удаление лишних пробелов между словами внутри строки
            Matcher matcher = pattern.matcher(dataInput);
            String processedDataInput = matcher.replaceAll(" ");
            Pattern pattern1 = Pattern.compile("[a-zA-Z&&[^0-9_]]+"); // шаблон для ввода типа list или exit
            Matcher matcher1 = pattern1.matcher(processedDataInput);
            Pattern pattern2 = Pattern.compile("(\\w[^0-9_]+\\s([\\w[^0-9]&&[a-zA-Zа-яА-я]]).+)"); // шаблон для ввода типа add Какое-то дело
            Matcher matcher2 = pattern2.matcher(processedDataInput);
            Pattern pattern3 = Pattern.compile("(\\w[^0-9_\\s]+\\s\\d+\\s.+)"); // шаблон для ввода типа add 1 Какое-то дело или edit 4 Новое название 4-ого дела
            Matcher matcher3 = pattern3.matcher(processedDataInput);
            Pattern pattern4 = Pattern.compile("\\w[^0-9_\\s]+\\s\\d+"); //  шаблон для ввода типа delete 1
            Matcher matcher4 = pattern4.matcher(processedDataInput);

            if (matcher1.matches()) {

                if (processedDataInput.equalsIgnoreCase(LIST)) {
                    if (list.size() != 0) {
                        for (int i = 0; i < list.size(); i++) {
                            System.out.println("п. " + (i + 1) + ")\t" + list.get(i));
                        }
                    } else {
                        System.out.println("Список пустой, дел нет!");
                    }
                } else if (processedDataInput.compareToIgnoreCase(EXIT) == 0) {
                    System.out.println("Вы вышли из приложения!");
                    break;
                } else if (processedDataInput.equalsIgnoreCase(HELP)) {
                    System.out.println("Я умею создавать списки твоих дел.\n" +
                            "Напиши одну из команд для работы со своим списком:\n" +
                            "\tadd - Команда добавляет новое дело в список. Например - Add Моё первое дело\n" +
                            "\tedit - Команда для редактирования дела из списка. Например - EDIT 4 Не моё первое дело\n" +
                            "\tdelete - Команда удаляет дело из списка. Например - Delete 1\n" +
                            "\tlist - Команда позволяет увидеть весь список существующих дел. Например - List\n" +
                            "\texit - Команда для выхода из приложения. Например - exit");
                } else {
                    System.out.println("Вы ввели не верную команду!\nВоспользуйтесь командой " + HELP + ".");
                }
            } else if (matcher2.matches()) {
                String[] command = processedDataInput.split(" ");
                if (command[0].equalsIgnoreCase(ADD)) {
                    String nameTask = processedDataInput.substring(command[0].length() + 1);
                    list.add(nameTask);
                    System.out.println("Вы добавили " + "\"" + nameTask + "\"" + " в список дел!");
                } else {
                    System.out.println("Вы ввели не верную команду!\nВоспользуйтесь командой " + HELP + ".");
                }
            } else if (matcher3.matches()) {
                String[] command = processedDataInput.split(" ");

                if (command[0].equalsIgnoreCase(ADD)) {
                    int numberTask = Integer.parseInt(command[1]);
                    String nameTask = processedDataInput.substring((command[0].length() + 1) +
                            (command[1].length() + 1));
                    if (numberTask > 0 && numberTask <= list.size()) {
                        list.add((numberTask - 1), nameTask);
                        System.out.println("Вы добавили " + "\"" + nameTask + "\"" + " на " + numberTask + " место!");
                    } else {
                        list.add(nameTask);
                        System.out.println("Вы добавили " + "\"" + nameTask + "\"" + " в конец списка!");
                    }
                } else if (command[0].equalsIgnoreCase(EDIT)) {
                    int numberTask = Integer.parseInt(command[1]);
                    String newNameTask = processedDataInput.substring((command[0].length() + 1) +
                            (command[1].length() + 1));
                    if (numberTask > 0 && numberTask <= list.size()) {
                        list.set(numberTask - 1, newNameTask);
                        System.out.println("Вы изменили имя задачи на " + newNameTask);
                    } else System.out.println("Такой задачи нет, нечего редактировать!");
                } else {
                    System.out.println("Вы ввели не верную команду!\nВоспользуйтесь командой " + HELP + ".");
                }
            } else if (matcher4.matches()) {
                String[] command = processedDataInput.split(" ");
                if (command[0].equalsIgnoreCase(DELETE)) {
                    int numberTask = Integer.parseInt((command[1]));
                    if (numberTask <= list.size() && numberTask != 0) {
                        System.out.println("Вы удалили задачу " + "№ " + numberTask + "\" " + list.get(numberTask - 1) + "\"");
                        list.remove((numberTask - 1));
                    } else {
                        System.out.println("Такой задачи нет!");
                    }
                } else {
                    System.out.println("Вы ввели не верную команду!\nВоспользуйтесь командой " + HELP + ".");
                }
            } else {
                System.out.println("Вы ввели не верную команду!\nВоспользуйтесь командой " + HELP + ".");
            }

        }
    }
}