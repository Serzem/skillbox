import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoaderClpGenerator {

    private static File file1 = new File("\\file2.txt");
    private static FileWriter fileWriter1 = null;

    private static List<String> baseList = new ArrayList<>();

    private static HashSet<String> baseHashSet;
    private static TreeSet<String> baseTreeSet;

    private static long start;
    private static long duration;


    public static void main(String[] args) {

        writerFile();
        addedInList();
        Collections.sort(baseList);
        addedInHashSet();
        addedInTreeSet();

        Pattern numberPattern = Pattern.compile("[ABCEHKMOPTXY][0-9][0-9][0-9][ABCEHKMOPTXY][ABCEHKMOPTXY][1-9]*[0-9][0-9]");


        boolean exit = true;

        while (exit) {
            Scanner scanner = new Scanner(System.in);
            String number = scanner.nextLine();

            if (number.equalsIgnoreCase("exit")) {
                break;
            }

            Matcher numberMatcher = numberPattern.matcher(number);

            if (numberMatcher.matches()) {
                tryFindInBaseList(number);
                tryFindBinary(number);
                tryFindInBaseHashSet(number);
                tryFindInBaseTreeSet(number);

            } else {
                System.out.println("Введён не корректный номер!");

            }

        }

    }


    private static void tryFindInBaseList(String string) {
        System.out.print("Поиск перебором: ");
        start = System.nanoTime();
        int x = 0;
        for (String findInBase : baseList) {
            if (findInBase.equals(string)) {
                x = 1;
                duration = System.nanoTime() - start;
                break;
            }
        }

        if (x > 0) {
            System.out.println("номер найден поиск занял " + (duration) + "нс");
        } else {
            duration = System.nanoTime() - start;
            System.out.println("номер не найден поиск занял " + (duration) + "нс");
        }
    }

    private static void tryFindBinary(String string) {
        System.out.print("Бинарный поиск: ");
        start = System.nanoTime();

        if (Collections.binarySearch(baseList, string) >= 0) {
            duration = System.nanoTime() - start;
            System.out.println("номер найден поиск занял " + (duration) + "нс");
        } else {
            duration = System.nanoTime() - start;
            System.out.println("номер не найден поиск занял " + (duration) + "нс");
        }
    }


    private static void tryFindInBaseHashSet(String string) {
        System.out.print("Поиск в HashSet: ");
        start = System.nanoTime();
        if (baseHashSet.contains(string)) {
            duration = System.nanoTime() - start;
            System.out.println("номер найден поиск занял " + (duration) + "нс");
        } else {
            duration = System.nanoTime() - start;
            System.out.println("номер не найден поиск занял " + (duration) + "нс");
        }
    }

    private static void tryFindInBaseTreeSet(String string) {
        System.out.print("Поиск в TreeSet: ");
        start = System.nanoTime();
        if (baseTreeSet.contains(string)) {
            duration = System.nanoTime() - start;
            System.out.println("номер найден поиск занял " + (duration) + "нс");
        } else {
            duration = System.nanoTime() - start;
            System.out.println("номер не найден поиск занял " + (duration) + "нс");
        }
    }

    private static void addedInTreeSet() {
        baseTreeSet = new TreeSet<>(baseHashSet);
        System.out.println(baseTreeSet.size() + " baseTreeSet");

    }

    private static void addedInHashSet() {
        baseHashSet = new HashSet<>(baseList);
        System.out.println(baseHashSet.size());
    }

    private static void addedInList() {


        try {
            Scanner scanner = new Scanner(new FileInputStream("\\file2.txt"));

            while (scanner.hasNextLine()) {
                baseList.add(scanner.nextLine());
            }
            System.out.println(baseList.size() + " Общий Лист");
            scanner.close();
        } catch (FileNotFoundException bb) {
            bb.printStackTrace();
        }
    }

    private static void writerFile() {

        String[] sumbolMass = {"A", "B", "E", "K", "M", "H", "O", "P", "C", "T", "Y", "X"};

        try {
            fileWriter1 = new FileWriter(file1);

            for (int i = 0; i < sumbolMass.length; i++) {
                for (int x = 1; x < 10; x++) {
                    for (int i1 = 0; i1 < sumbolMass.length; i1++) {
                        for (int i2 = 0; i2 < sumbolMass.length; i2++) {
                            for (int y = 1; y < 200; y++) {
                                if (y < 10) {
                                    try {
                                        fileWriter1.write(("" + sumbolMass[i] + x + x + x + sumbolMass[i1] + sumbolMass[i2] + 0 + y + "\n"));
                                    } catch (IOException b) {
                                        b.printStackTrace();
                                    }
                                } else {
                                    try {
                                        fileWriter1.write(("" + sumbolMass[i] + x + x + x + sumbolMass[i1] + sumbolMass[i2] + y + "\n"));
                                    } catch (IOException b) {
                                        b.printStackTrace();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception d) {
            d.printStackTrace();
        } finally {
            try {
                fileWriter1.close();
            } catch (IOException b) {
                b.printStackTrace();
            }
        }
    }
}
