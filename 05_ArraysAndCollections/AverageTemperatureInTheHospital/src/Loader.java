public class Loader {

    private static final int COUNT_PATIENT = 30;
    private static final double MAX_TEMP = 40.;
    private static final double MIN_TEMP = 32.;
    private static final double MAX_TEMP_HEALTHY_PATIENT = 36.9;
    private static final double MIN_TEMP_HEALTHY_PATIENT = 36.2;
    private static final double MIN_TEMP_GENERATION_PATIENT = MIN_TEMP;

    public static void main(String[] args) {

        double[][] patientAndTemperature = new double[COUNT_PATIENT][2];
        double amountTemperature = 0.;
        int numberHealthyPatients = 0;

        for (int i = 0; i < COUNT_PATIENT; i++) {
            patientAndTemperature[i][0] = i + 1;

            while (patientAndTemperature[i][1] < MIN_TEMP ||
                    patientAndTemperature[i][1] > MAX_TEMP) {

                double tempGenerationPatient = MIN_TEMP_GENERATION_PATIENT + (((MAX_TEMP - MIN_TEMP) * (MAX_TEMP - MIN_TEMP) * Math.random()));
                patientAndTemperature[i][1] = tempGenerationPatient;
            }

            if (patientAndTemperature[i][1] >= MIN_TEMP_HEALTHY_PATIENT &&
                    patientAndTemperature[i][1] <= MAX_TEMP_HEALTHY_PATIENT) {

                numberHealthyPatients++;
            }
            amountTemperature += patientAndTemperature[i][1];
        }
        double averageTemperature = amountTemperature / COUNT_PATIENT;
        System.out.print("Средняя температура по больнице - ");
        System.out.format("%.2f%n", averageTemperature);
        System.out.println("Количество здоровых пациентов - " + numberHealthyPatients);
    }
}
