import java.util.HashSet;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Loader {
    private final static String HELP = "help";
    private final static String ADD = "add";
    private final static String LIST = "list";
    private final static String EXIT = "exit";

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        HashSet<String> list = new HashSet<>();

        while (true) {

            String dataInput = input.nextLine().strip();

            Pattern pattern = Pattern.compile("[A-Za-z]+\\s[\\w[\\.]*]+\\@[A-Za-z[0-9]*]+\\.[A-Za-z]{2,7}+");
            Matcher matcher = pattern.matcher(dataInput);
            Pattern pattern1 = Pattern.compile("[A-Za-z]+");
            Matcher matcher1 = pattern1.matcher(dataInput);

            String[] dataStrip = dataInput.split(" ");

            if (matcher.matches()) {
                if (dataStrip[0].equalsIgnoreCase(ADD)) {
                    String email = dataInput.substring(dataStrip[0].length() + 1);
                    if (list.contains(email)) {
                        System.out.println("Ошибка! Этот адрес уже есть в списке!\nВведите другой!");
                    } else {
                        list.add(email);
                        System.out.println(email + "\nУспешно добавлен в список!");
                    }
                } else {
                    System.out.println("Ошибка! Не верная команда!\nДля помощи введите команду \"Help\".");
                }
            } else if (matcher1.matches()) {
                if (dataInput.equalsIgnoreCase(LIST)) {
                    if (list.size() > 0) {
                        int i = 1;
                        System.out.println("Полный список e-mail");
                        for (String emailList : list) {
                            System.out.println(i + ") " + emailList);
                            i++;
                        }
                    } else {
                        System.out.println("Список пуст!");
                    }
                } else if (dataInput.equalsIgnoreCase(EXIT)) {
                    System.out.println("Вы успешно закрыли приложение!");
                    break;
                } else if (dataInput.equalsIgnoreCase(HELP)) {
                    System.out.println("Список команд:\nКоманда\n\tAdd someemail@someemail.someemail\nДобавляет e-mail в список,в соответстии с форматом.\n" +
                            "Команда\n\tlist\nВыводит список записанных e-mail адресов\n" + "Команда\n\texit\nЗакрывает приложение.\n");
                } else {
                    System.out.println("Ошибка! Не верная команда!\nДля помощи введите команду \"Help\".");
                }
            } else {
                System.out.println("Ошибка! Не верная команда!\nДля помощи введите команду \"Help\".");
            }
        }
    }
}
