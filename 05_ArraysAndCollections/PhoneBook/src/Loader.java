import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Loader {
    private final static String EXIT = "exit";
    private final static String LIST = "list";
    private final static String DIGITS = "digits";
    private final static String WORDS = "words";
    private final static String HELP = "help";


    private static String correctPhoneNumberForPrint = "";
    private static TreeMap<String, String> telBook = new TreeMap<>();
    private static String dataFormat = "";

    public static void main(String[] args) {

        System.out.println("Привет! Я твоя телефонная книга!\n");

        Scanner input = new Scanner(System.in);

        telBook.put("+1 (234) 567-8910", "FirstName LastName SecondName");

        while (true) {
            System.out.println("Введите ФИО или телефон.");
            String dataInput = input.nextLine().strip();

            checkFormatDataInput(dataInput);

            if (dataFormat.equalsIgnoreCase(WORDS)) {
                String[] nameList = dataInput.split(" ");
                if ((commandList(nameList[0]))) {
                    if (nameList[0].equalsIgnoreCase(EXIT)) {
                        break;
                    }
                } else {
                    if (!checkingAvailabilityDatabase(dataInput)) { // проверяем есть ли имя в базе
                        enteringNewDataInPhoneBook(dataInput);
                    }
                }

            } else if (dataFormat.equalsIgnoreCase(DIGITS)) {
                if (checkingPhoneNumber(dataInput)) {
                    if (!checkingAvailabilityDatabase(dataInput)) { // проверяем есть ли телефон в базе
                        enteringNewDataInPhoneBook(dataInput);
                    }
                }
            } else {
                System.out.println(dataFormat);
            }
        }

    }

    private static boolean checkingPhoneNumber(String phoneNumber) {
        checkFormatDataInput(phoneNumber);
        String telNumber = "";
        boolean phoneNumberBoolean = false;

        String correctPhoneNumber = "";

        if (dataFormat.equalsIgnoreCase(DIGITS)) {
            Pattern digitPattern = Pattern.compile("\\d");
            Matcher digitMatcher = digitPattern.matcher(phoneNumber);

            while (digitMatcher.find()) {
                String NumberFromNumber = phoneNumber.substring(digitMatcher.start(), digitMatcher.end());
                telNumber += NumberFromNumber;
            }

            if (telNumber.length() == 11) {
                int firstSymbol = Integer.parseInt(telNumber.substring(0, 1));
                if (firstSymbol == 8 || firstSymbol == 0) {
                    correctPhoneNumber = telNumber.replaceFirst("8|0", "7");
                } else {
                    correctPhoneNumber = telNumber;
                }
                correctPhoneNumberForPrint = correctPhoneNumber.replaceAll("(\\d)(\\d{3})(\\d{3})(\\d{4})", "+$1 ($2) $3-$4");
                phoneNumberBoolean = true;
            } else {
                System.out.println(dataFormat + "\nПример: 8 905 1234567 или +7 909 123-45-67 или 8(909) 123-45-67.");
            }
        } else {
            System.out.println(dataFormat + "\nПример: 8 905 1234567 или +7 909 123-45-67 или 8(909) 123-45-67.");
        }
        return phoneNumberBoolean;
    }

    private static boolean checkingNameFormat(String name) {
        boolean nameBoolean = true;
        checkFormatDataInput(name);

        if (!dataFormat.equalsIgnoreCase(WORDS)) {
            System.out.println(dataFormat + "\nПример: \"FirstName LastName SecondName\" или \"FirstName LastName\" или \"FirstName\".\nЦифры при вводе имени недопустимы.");
            nameBoolean = false;
        }
        return nameBoolean;
    }

    private static boolean checkingAvailabilityDatabase(String dataInput) {
        boolean nameOrPhoneNumberBoolean = false;
        if (telBook.containsValue(dataInput)) {
            System.out.println("Такой Абонент уже есть в книге, вот информация по нему:");
            for (String key : telBook.keySet()) {
                if (telBook.get(key).compareToIgnoreCase(dataInput) == 0) {
                    System.out.println("Телефон " + key + "\t" + "ФИО Абонента " + telBook.get(key));
                }
            }
            nameOrPhoneNumberBoolean = true;
        } else if (telBook.containsKey(dataInput)) {
            for (String key : telBook.keySet()) {
                if (key.equalsIgnoreCase(dataInput)) {
                    System.out.println("Такой номер телефона уже есть в базе, вот информация по нему:\n" + "Телефон " + key + "\t" + "ФИО Абонента " + telBook.get(key));
                    nameOrPhoneNumberBoolean = true;
                }
            }
        }
        return nameOrPhoneNumberBoolean;
    }

    private static void enteringNewDataInPhoneBook(String dataInput) {
        Scanner input = new Scanner(System.in);
        checkFormatDataInput(dataInput);

        switch (dataFormat) {
            case WORDS:
                while (true) {
                    System.out.println("Введите номер телефона!");
                    String phoneNumber = input.nextLine().strip();
                    if (commandList(phoneNumber)) {
                        if (phoneNumber.equalsIgnoreCase(EXIT)) {
                            break;
                        }
                    } else if (checkingPhoneNumber(phoneNumber)) {
                        if (telBook.containsKey(correctPhoneNumberForPrint)) {
                            System.out.println("Такой номер телефона уже есть в базе, введите другой.");
                        } else {
                            telBook.put(correctPhoneNumberForPrint, dataInput);
                            System.out.println("Вы успешно добавили в базу!\n" + "Телефон " + correctPhoneNumberForPrint + "\t" + "ФИО Абонента " + dataInput);
                            break;
                        }
                    }
                }
                break;
            case DIGITS:
                while (true) {
                    System.out.println("Введите ФИО Абонента!");
                    String name = input.nextLine().strip();
                    if (commandList(name)) {
                        if (name.equalsIgnoreCase(EXIT)) {
                            break;
                        }
                    } else if (checkingNameFormat(name)) {
                        if (telBook.containsValue(name)) {
                            System.out.println("Такой Абонент уже есть в базе, введите другие данные.");
                        } else {
                            telBook.put(dataInput, name);
                            System.out.println("Вы успешно добавили в базу!\n" + "Телефон " + dataInput + "\t" + "ФИО Абонента " + name);
                            break;
                        }
                    }
                }
                break;
        }
    }

    private static void checkFormatDataInput(String dataInput) {
        Pattern namePattern = Pattern.compile("[a-zA-Z]+\\s*[a-zA-Z]*\\s*[a-zA-Z]*");
        Matcher nameMatcher = namePattern.matcher(dataInput);

        Pattern telNumberPattern = Pattern.compile("[+\\d[-()\\s]]*");
        Matcher telNumberMatcher = telNumberPattern.matcher(dataInput);

        if (nameMatcher.matches()) {
            dataFormat = WORDS;
        } else if (telNumberMatcher.matches()) {
            dataFormat = DIGITS;
        } else {
            dataFormat = "Не верный формат.";
        }
    }

    private static boolean commandList(String dataInput) {
        boolean commandListBoolean = true;
        switch (dataInput.toLowerCase()) {
            case EXIT:
                System.out.println("Вы успешно вышли!");
                break;
            case LIST:
                int i = 1;
                for (String list : telBook.keySet()) {
                    System.out.println(i + ") " + " Телефон " + list + "\t" + "ФИО Абонента " + telBook.get(list));
                    i++;
                }
                break;
            case HELP:
                System.out.println("Доступные команды:\nexit - выход из программы или в верхнее меню\nlist - вывод на экран всей телефонной книги\nhelp - список команд/помощь.");
                break;
            default:
                commandListBoolean = false;
        }
        return commandListBoolean;
    }
}
