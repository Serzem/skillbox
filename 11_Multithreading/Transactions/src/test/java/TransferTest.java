import imitationWork.CreateAccountBase;
import imitationWork.ImitateTransfer;
import model.Bank;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

public class TransferTest {
    Bank bank;

    Thread t1;

    ArrayList<Thread> threads;


    long money;
    double random;
    long start;

    @Before
    public void setUp() throws Exception {
        bank = new Bank();

        threads = new ArrayList<>();

        /*заполняем базу банка 10-ю потоками*/
        start = System.currentTimeMillis();
        for (int i = 1; i < 11; i++) {

            t1 = new Thread(new CreateAccountBase(bank, start));
            threads.add(t1);
        }
        threads.forEach(Thread::start); // стартуем потоки

        /*контроль завершения потоков*/
        threads.forEach(thread -> {
            if (!thread.isInterrupted()) {
                try {
                    thread.join();
                } catch (InterruptedException exception) {
                    exception.printStackTrace();
                }
            }
        });

        threads.clear();

    }


    @Test
    public void imitationTransfer() {
        long actual = bank.getAllBalance();

        {
            double rand = 0.;
            String accFrom;
            String accTo;

            /*имитируем переводы*/
            for (int qq = 0; qq < 1_000_000; qq++) {
                String randomAccFrom = "" + (int) (Math.random() * 10_000_000);
                String randomAccTo = "" + (int) (Math.random() * 10_000_000);

                rand = Math.random();

                if (rand <= 0.5) {
                    accFrom = "1";
                    accTo = "2";
                } else {
                    accFrom = "2";
                    accTo = "1";
                }


                random = Math.random();
                if (random <= 0.60) {
                    money = 45_000;
                } else {
                    money = 65_000;
                }
                threads.add(new Thread(new ImitateTransfer(bank, randomAccFrom, randomAccTo, money)));
//                threads.add(new Thread(new ImitateTransfer(bank, accFrom, accTo, money)));
            }
        }

        threads.forEach(Thread::start); /*запускаем иммитацию*/

        /*контроль завершения потоков*/
        threads.forEach(thread -> {
            if (!thread.isInterrupted()) {
                try {
                    thread.join();
                } catch (InterruptedException exception) {
                    exception.printStackTrace();
                }
            }
        });

        long expect = 750_000_000_000L; /* ((100_000 + 50_000) / 2) * 10_000_000  */
        Assert.assertEquals(expect, actual, 0);
    }

    @After
    public void tierDown() throws Exception {

    }
}
