package imitationWork;

import model.Bank;

public class ImitateTransfer implements Runnable {

    Bank bank;
    String accFrom;
    String accTo;
    long money;

    public ImitateTransfer(Bank bank, String accFrom, String accTo, long money) {
        this.bank = bank;
        this.accFrom = accFrom;
        this.accTo = accTo;
        this.money = money;
    }

    @Override
    public void run() {
        bank.transfer(accFrom, accTo, money);
    }
}
