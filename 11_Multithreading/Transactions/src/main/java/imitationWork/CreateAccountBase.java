package imitationWork;

import model.Account;
import model.Bank;

import java.util.Map;

public class CreateAccountBase implements Runnable {

    Bank bank;
    long start;

    public CreateAccountBase(Bank bank, long start) {
        this.bank = bank;
        this.start = start;
    }

    @Override
    public void run() {
        long money;
        for (int i = 0; i < 1_000_000; i++) {

            if (i < 500_000) {
                money = 100_000L;
            } else {
                money = 50_000L;
            }
            bank.createAccount(money);
        }
        System.out.println("Thread " + Thread.currentThread().getName() + " закончил свою работу за " + (System.currentTimeMillis() - start));
    }

}
