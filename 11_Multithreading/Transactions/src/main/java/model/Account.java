package model;

import lombok.Getter;
import lombok.Setter;

public class Account {
    @Getter
    @Setter
    private volatile long money;

    @Getter
    @Setter
    private String accNumber;

    @Getter
    @Setter
    private boolean isFraud;

    public Account() {
    }

    public Account(String accNumber, long money, boolean isFraud) {
        this.accNumber = accNumber;
        this.money = money;
        this.isFraud = isFraud;
    }

}
