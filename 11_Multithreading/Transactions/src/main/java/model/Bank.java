package model;

import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Bank implements ReadWriteLock {
    private long unverifiableAmountTransfer = 50_000L;

    private volatile Map<String, Account> accounts = new ConcurrentHashMap<>();

    private final Random random = new Random();
    private ReadWriteLock lock = new ReentrantReadWriteLock();

    public boolean isFraud()
            throws InterruptedException {
        Thread.sleep(1000);
        return random.nextBoolean();
    }

    public synchronized void createAccount(long money) {

        String accNumber;
        Account account;

        Map<String, Account> accounts = getAccounts();
        if (accounts.isEmpty()) {
            accNumber = "1";
            account = new Account(accNumber, money, false);
            accounts.put(accNumber, account);
        } else {
            accNumber = "" + (accounts.size() + 1);
            account = new Account(accNumber, money, false);
            accounts.put(accNumber, account);
        }
        setAccounts(accounts);

    }

    /**
     * TODO: реализовать метод. Метод переводит деньги между счетами.
     * Если сумма транзакции > 50000, то после совершения транзакции,
     * она отправляется на проверку Службе Безопасности – вызывается
     * метод isFraud. Если возвращается true, то делается блокировка
     * счетов (как – на ваше усмотрение)
     */

    public void transfer(String accFrom, String accTo, long money) {

        Account accountFrom = getAccountFrom(accFrom);
        Account accountTo = getAccountTo(accTo);

        boolean b = false;

        if (money > unverifiableAmountTransfer) {
            try {
                b = isFraud();
            } catch (InterruptedException exception) {
                exception.printStackTrace();
            }
        }

        if (accFrom.compareTo(accTo) > 0) {
            synchronized (accountFrom) {
                synchronized (accountTo) {
                    doTransfer(accountFrom, accountTo, money, b);
                }
            }
        } else {
            synchronized (accountTo) {
                synchronized (accountFrom) {
                    doTransfer(accountFrom, accountTo, money, b);
                }
            }
        }
    }

    private void doTransfer(Account accFrom, Account accTo, long money, boolean fraudResolt) {

        if (accFrom.isFraud() || accTo.isFraud()) {
            System.out.println("block! " + Thread.currentThread().getName());
            return;
        }

        if (accFrom.getMoney() < money) {
            System.out.println("no money! " + Thread.currentThread().getName());
            return;
        }

        accFrom.setMoney(accFrom.getMoney() - money);
        accTo.setMoney(accTo.getMoney() + money);

        if (fraudResolt) {
            accFrom.setFraud(true);
            accTo.setFraud(true);
            System.out.println("set block! " + Thread.currentThread().getName());
        } else {
            System.out.println("transfer complete! " + Thread.currentThread().getName());
        }

        accounts.put(accTo.getAccNumber(), accTo);
        accounts.put(accFrom.getAccNumber(), accFrom);
    }

    public Account getAccountFrom(String accFrom) {
        return accounts.get(accFrom);
    }

    public Account getAccountTo(String accTo) {
        return accounts.get(accTo);
    }

    /**
     * TODO: реализовать метод. Возвращает остаток на счёте.
     */

    public synchronized Map<String, Account> getAccounts() {
        return this.accounts;
    }

    public void setAccounts(Map<String, Account> accounts) {
        this.accounts = accounts;
    }

    public long getAllBalance() {
        long balance = 0L;
        if (!getAccounts().isEmpty()) {
            Map<String, Account> ac = getAccounts();
            for (Object q : ac.keySet()) {
                Account account = ac.get(String.valueOf(q));
                balance += account.getMoney();
            }
        }
        return balance;
    }

    @NotNull
    @Override
    public Lock readLock() {
        return null;
    }

    @NotNull
    @Override
    public Lock writeLock() {
        return null;
    }
}
