package actions.connection;

import lombok.Getter;
import lombok.Setter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

public class SiteConnect {
    @Getter
    @Setter
    Document document;

    @Getter
    String siteUrl = "";

    CheckLinkStatus checkLinkStatus;


    //для записи из сети с самого сайта
    public SiteConnect(String url) throws IOException {

        siteUrl = url;
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        setDocument(Jsoup.connect(siteUrl).userAgent("Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0")
                .referrer("http://www.google.com").ignoreHttpErrors(true).get());
        System.out.println("new url SiteDownLoad " + url + "\t" + Thread.currentThread().getName());
    }


}
