package actions.connection;

import org.apache.logging.log4j.core.util.JsonUtils;
import org.jsoup.Jsoup;
import org.junit.experimental.theories.Theories;

import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.List;

public class CheckLinkStatus {

    public boolean checkUrlStatus200(String url) {
//        System.out.println("check " + url + Thread.currentThread().getName());
        try {
            Thread.sleep(150);

            if (Jsoup.connect(url).timeout(100).userAgent("Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0")
                    .referrer("http://www.google.com").ignoreHttpErrors(true).execute().statusCode() == 200) {
                //Jsoup.connect(siteUrl).userAgent("Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0")
                //                        .referrer("http://www.google.com").ignoreHttpErrors(true).get()
                return true;
            } else {
//                System.out.println("Упс.. Произошла какая-то ошибка!\n Проверьте правильность адреса!");
//                System.out.println(url);
                return false;
            }
        } catch (IllegalArgumentException | IOException | InterruptedException exception) {
//            System.out.println("re");
//            System.out.println("Упс.. Произошла ошибка (Exception - checkUrlStatus200)!\nНе верный протокол! Поддерживается толькой - http & https.");
//            System.out.println(url + " URL checkUrlStatus200");
//            exception.printStackTrace();
            return false;
        }
    }

}
