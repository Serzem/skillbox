package actions.writeFile;

//import actions.parse.ParseEntity;
import lombok.Getter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class WriteReferencesInFileTXT {

    @Getter
    List<String> sortRefWithHttp;

    @Getter
    List<String> sortRefWithoutHttp;

    String pathFileRef;

//    public WriteReferencesInFileTXT(String pathTo, ParseEntity parseEntity, String pathFrom) throws IOException {
//        sortRefWithHttp = parseEntity.getSortLinkWithHttp();
//        sortRefWithoutHttp = parseEntity.getSortLinkWithoutHttp();
//        pathFileRef = pathTo;
//        writeRefInFile(pathTo, sortRefWithHttp, sortRefWithoutHttp, pathFrom);
//    }

    private void writeRefInFile(String pathTo, List<String> sortRefWithHttp, List<String> sortRefWithoutHttp, String pathFrom) throws IOException {
        File file = new File(pathTo + ".txt");


//        pathFrom = pathFrom.strip();
//        if (pathFrom.endsWith("/")) {
//            pathFrom = pathFrom.substring(0, pathFrom.length() - 1);
//        }
//        else {
//            pathFrom = pathFrom + "/";
//        }

        if (file.createNewFile()) {
            FileWriter fileWriter = new FileWriter(file);

            if (sortRefWithHttp.size() > 0 || sortRefWithoutHttp.size() > 0) {
                if (pathFrom.endsWith("/")) {
                    fileWriter.write(pathFrom + "\n");

                    for (String qq : sortRefWithHttp) {
                        fileWriter.write("\t" + qq + "\n");
                    }

                    for (String qq2 : sortRefWithoutHttp) {
                        if (qq2.length() > 1) {
                            String qq3 = qq2.substring(1);
                            fileWriter.write("\t" + pathFrom + qq3 + "\n");
                        }
                    }
                    System.out.println(pathFrom);
                } else {
                    fileWriter.write(pathFrom + "/" + "\n");

                    for (String qq : sortRefWithHttp) {
                        fileWriter.write("\t" + qq + "\n");
                    }

                    for (String qq2 : sortRefWithoutHttp) {
                        if (qq2.length() > 1) {
                            fileWriter.write("\t" + pathFrom + qq2 + "\n");
                        }
                    }
                    System.out.println(pathFrom);
                }

            }


//            if (sortRefWithHttp.size() > 0) {
//                fileWriter.write(pathFrom + "\n");
//                for (String qq : sortRefWithHttp) {
//                    fileWriter.write("\t" + qq + "\n");
//                }
//            }
//
//            if (sortRefWithoutHttp.size() > 0) {
//                pathFrom = pathFrom.strip();
//                if (pathFrom.endsWith("/")) {
//                    pathFrom = pathFrom.substring(0, pathFrom.length() - 1);
//
//                } else {
//                    pathFrom = pathFrom + "/";
//                }
//            }

            fileWriter.flush();
            fileWriter.close();

        } else {
            System.out.println("Overwriting a file!");
            FileWriter fileWriter = new FileWriter(file);

            if (sortRefWithHttp.size() > 0 || sortRefWithoutHttp.size() > 0) {
                if (pathFrom.endsWith("/")) {
                    fileWriter.write(pathFrom + "\n");

                    for (String qq : sortRefWithHttp) {
                        fileWriter.write("\t" + qq + "\n");
                    }

                    for (String qq2 : sortRefWithoutHttp) {
                        if (qq2.length() > 1) {
                            String qq3 = qq2.substring(1);
                            fileWriter.write("\t" + qq3 + "\n");
                        }
                    }
                    System.out.println(pathFrom);
                } else {
                    fileWriter.write(pathFrom + "/" + "\n");

                    for (String qq : sortRefWithHttp) {
                        fileWriter.write("\t" + qq + "\n");
                    }

                    for (String qq2 : sortRefWithoutHttp) {
                        if (qq2.length() > 1) {
                            fileWriter.write("\t" + qq2 + "\n");
                        }
                    }
                    System.out.println(pathFrom);
                }

            }
//            if (sortRefWithHttp.size() > 0) {
//                fileWriter.write(pathFrom + "\n");
//                for (String qq : sortRefWithHttp) {
//                    fileWriter.write("\t" + qq + "\n");
//                }
//            }

            fileWriter.flush();
            fileWriter.close();
        }
    }
}
