package actions.writeFile;

import lombok.Getter;
import org.jsoup.nodes.Document;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class WriteSiteInFileHTML {
    @Getter
    String pathTo;

    @Getter
    Document document;

    public WriteSiteInFileHTML(Document document) {
        this.document = document;
    }

    //* метод записи в файл скачанной страницы
    public void writeSite(String pathTo) throws IOException {
        File file = new File(pathTo + ".html");
        FileWriter fileWriter;
        if (file.createNewFile()) {
            fileWriter = new FileWriter(file);
            fileWriter.write(document.toString());
            System.out.println("запись");
            fileWriter.flush();
            fileWriter.close();
        } else {
            System.out.println("Overwriting a file!");
            fileWriter = new FileWriter(file);
            fileWriter.write(document.toString());
            System.out.println("запись");
            fileWriter.flush();
            fileWriter.close();
        }
    }
}
