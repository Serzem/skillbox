package actions.parse.parseFilters;

import lombok.Getter;
import lombok.Setter;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ParseAttrHref {

    @Getter
    @Setter
    List<String> hrefList = new ArrayList<>();

    @Getter
    private final String FILTER_ELEMENT_ATTR = "href";

    public ParseAttrHref(String url) throws IOException {
        ParseTagA parseTagA = new ParseTagA(url);
        Elements e;
        if (parseTagA.getElements() != null) {
            e = parseTagA.getElements();
            setHrefList(sortElementsList(e));
        }
    }

    //парсит Elements по атрибуту href и заполняет список из ссылок, которые есть на странице
    private List<String> sortElementsList(Elements elements) {
        List<String> sort = new ArrayList<>();
        elements.forEach(element -> {
            sort.add(element.attr(FILTER_ELEMENT_ATTR));
        });
        return sort;
    }
}
