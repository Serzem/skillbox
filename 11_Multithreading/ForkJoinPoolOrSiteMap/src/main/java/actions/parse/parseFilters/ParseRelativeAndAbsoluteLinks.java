package actions.parse.parseFilters;

import lombok.Getter;
import lombok.Setter;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParseRelativeAndAbsoluteLinks {

    @Getter
    @Setter
    List<String> relativeLinkList = new CopyOnWriteArrayList<>();

    @Getter
    @Setter
    List<String> absoluteLinkList = new CopyOnWriteArrayList<>();

    private final String SEARCH = "?";
    private final String HASH = "#";

    public ParseRelativeAndAbsoluteLinks(String url) throws IOException {
        ParseAttrHref parseAttrHref = new ParseAttrHref(url);
        List<String> list;

        if (parseAttrHref.getHrefList() != null) {
            list = parseAttrHref.getHrefList();
            setRelativeLinkList(sortHrefRelativeList(list));
            setAbsoluteLinkList(sortHrefAbsoluteLinks(list, url));
        }
    }

//    public ParseRelativeLinks(List<String> listHref) {
//        setRelativeLinkList(sortHrefList(listHref));
//    }

//    private void setRelativeLinkList(List<String> list) {
//        Pattern patternDirectory = Pattern.compile("^/");
//        Matcher matcher;
//        for (String qq : list) {
//            if (qq.contains(SEARCH) || qq.contains(HASH)) {
//                continue;
//            }
//
//            matcher = patternDirectory.matcher(qq);
//            while (matcher.find()) {
//                relativeLinkList.add(qq);
//            }
//        }
//    }

    private List<String> sortHrefRelativeList(List<String> hrefList) {
        List<String> sortHrefList = new ArrayList<>();
        Pattern patternDirectory = Pattern.compile("^/");
        Matcher matcher;
        for (String qq : hrefList) {
            if (qq.contains(SEARCH) || qq.contains(HASH)) {
                continue;
            }

            matcher = patternDirectory.matcher(qq);
            while (matcher.find()) {
                sortHrefList.add(qq);
            }
        }
        return sortHrefList;
    }

    public List<String> sortHrefAbsoluteLinks(List<String> list, String url) throws MalformedURLException {
        String protocol_1 = "^http";
        String protocol_2 = "^ftp";

        List<String> allAbsLinkList = new ArrayList<>();

        Pattern patternDirectory = Pattern.compile(protocol_1);
        Pattern patternDirectory2 = Pattern.compile(protocol_2);
        Matcher matcher;

        for (String qq : list) {
            if (qq.contains(SEARCH) || qq.contains(HASH)) {
                continue;
            }

            matcher = patternDirectory.matcher(qq);

            while (matcher.find()) {

                allAbsLinkList.add(qq);
            }

            matcher = patternDirectory2.matcher(qq);
            while (matcher.find()) {
                allAbsLinkList.add(qq);
            }

        }

        allAbsLinkList = sortHrefAbsoluteLinksByDomain(allAbsLinkList, url);

        return allAbsLinkList;
    }

    public List<String> sortHrefAbsoluteLinksByDomain(List<String> list, String url) throws MalformedURLException {

        List<String> absLinksSortedList = new ArrayList<>();

        for (String s : list) {
            String nameDomain = new URL(url).getAuthority();
            if (new URL(s).getAuthority().equals(nameDomain) && new URL(s).getPath().lastIndexOf("/") > 0) {
                absLinksSortedList.add(s);
            }
        }
        return absLinksSortedList;
    }

}
