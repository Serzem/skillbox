package actions.parse.parseFilters;

import actions.connection.SiteConnect;
import lombok.Getter;
import lombok.Setter;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;

public class ParseTagA {

    @Getter
    @Setter
    Elements elements = new Elements();

    public ParseTagA(String url) throws IOException {

        SiteConnect siteConnect = new SiteConnect(url);
        Document document;
        if (siteConnect.getDocument() != null) {
            document = siteConnect.getDocument();
            setElements(sortElementsParseTagA(document));
        }
    }

    //парсит страницу сайта или скачанный в html файл сайт по тегу <a>
    public Elements sortElementsParseTagA(Document document) {
        return document.select("a");
    }
}
