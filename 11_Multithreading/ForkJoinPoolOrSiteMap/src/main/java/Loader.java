import actions.connection.SiteConnect;
//import actions.parse.ParseEntity;
import actions.writeFile.WriteSiteInFileHTML;
import entity.SiteMap;
import entity.WebPage;
import util.NotValidateCertificate;

import java.util.*;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;
import java.util.concurrent.RecursiveTask;


public class Loader {
    static final String URL_FROM = "https://lenta.ru"; //https://skillbox.ru https://lenta.ru    https://skillbox.ru/multimedia/  https://www.proveeam.ru/contact  profession-sound-designer/
    static final String PATH_TO = "data//dataHTML//site"; /*$PROJECT_DIR$*/

    static final String PATH_TO1 = "data//dataHTML//line2//site_";

    static final String PATH_TO2 = "data//dataHTML//line2//line3//site_";

    static final String pathForRef = "data//ref//file";

    static final String pathForRefLine2 = "data//ref//line2//file";

    static final String pathForRefLine3 = "data//ref//line2//line3//file";


    public static void main(String[] args) {
        new NotValidateCertificate();

        Calendar calendar = new GregorianCalendar();
        long begin = calendar.getTimeInMillis();
        SiteMap siteMap;
        ForkJoinPool forkJoinPool = new ForkJoinPool();
        forkJoinPool.invoke(siteMap = new SiteMap(URL_FROM));
//        forkJoinPool.invoke(new SiteMap(URL_FROM).fork());

//        new ForkJoinPool().invoke(siteMap);


//        try {
//            SiteMap siteMap = new SiteMap(URL_FROM);
//            siteMap.createSiteMap(URL_FROM);

//            System.out.println();
//            System.out.println("\nFULL SORT LIST - getListCompletedPages()\n");
//            siteMap.getListCompletedPages().keySet().forEach(System.out::println);
//            System.out.println("\nFULL SORT LIST - getListCompletedPages()\n");
//
        System.out.println("\nFULL SORT LIST - getLinksGoOut()\n");
        siteMap.getLinksGoOut().forEach(System.out::println);
        System.out.println("\nFULL SORT LIST - getLinksGoOut()\n");

        System.out.println("\nEnd - getListCompletedPages() " + siteMap.getListCompletedPages().size());
        System.out.println("\nEnd - getLinks() " + siteMap.getLinksGoOut().size());
        System.out.println("\nFULL BED LINKS LIST\n");
        System.out.println(siteMap.getBadLinkList().size());
        System.out.println();
//
//            for (String ff : siteMap.getListCompletedPages().keySet()) {
//                if (!siteMap.getLinks().contains(ff)) {
//                    System.out.println("Этой ссылки нет в LINKS " + ff);
//                }
//            }
//
//
//        } catch (Exception e) {
//            System.out.println("THIS IS EXCEPTION MAIN!!!");
//            e.printStackTrace();
//        }

        Calendar calendar1 = new GregorianCalendar();
        long end = calendar1.getTimeInMillis();
        System.out.println((((end - begin) / 1000.) / 60.) + " минут");
    }

//    @Override
//    protected void compute(){
//        Calendar calendar = new GregorianCalendar();
//        long begin = calendar.getTimeInMillis();
//
//        try {
//            SiteMap siteMap = new SiteMap();
//            siteMap.createSiteMap(URL_FROM);
//            System.out.println();
//            System.out.println("\nFULL SORT LIST - getListCompletedPages()\n");
//            siteMap.getListCompletedPages().keySet().forEach(System.out::println);
//            System.out.println("\nFULL SORT LIST - getListCompletedPages()\n");
//
//            System.out.println("\nFULL SORT LIST - getLinks()\n");
//            siteMap.getLinks().forEach(System.out::println);
//            System.out.println("\nFULL SORT LIST - getLinks()\n");
//
//            System.out.println("\nEnd - getListCompletedPages() " + siteMap.getListCompletedPages().size());
//            System.out.println("\nEnd - getLinks() " + siteMap.getLinks().size());
//            System.out.println("\nFULL BED LINKS LIST\n");
//            System.out.println(siteMap.getBadLinkList().size());
//            System.out.println();
//
//            for (String ff : siteMap.getListCompletedPages().keySet()) {
//                if (!siteMap.getLinks().contains(ff)) {
//                    System.out.println("Этой ссылки нет в LINKS " + ff);
//                }
//            }
//        } catch (Exception e) {
//            System.out.println("THIS IS EXCEPTION MAIN!!!");
//            e.printStackTrace();
//        }
//
//        Calendar calendar1 = new GregorianCalendar();
//        long end = calendar1.getTimeInMillis();
//        System.out.println((((end - begin) / 1000.) / 60.) + " минут");
//    }

}
