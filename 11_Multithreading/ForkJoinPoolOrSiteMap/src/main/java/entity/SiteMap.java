package entity;

import actions.connection.CheckLinkStatus;
import actions.connection.SiteConnect;
//import actions.parse.ParseEntity;
import lombok.Getter;

import java.io.IOException;
import java.sql.SQLOutput;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class SiteMap extends RecursiveAction {
    /**
     * RecursiveAction
     **/

    String protocol;
    String domain;

    @Getter
    private String url;

    @Getter
    SiteConnect siteConnect;

    //    @Getter
    Map<String, WebPage> listCompletedPages = new ConcurrentHashMap<>();

    //    @Getter
    Set<String> linksForGo = new ConcurrentSkipListSet<>();

    @Getter
    Set<String> linksGoOut = new ConcurrentSkipListSet<>();

    //    Set<String> links = new HashSet<>();
    @Getter
    Set<String> badLinkList = new ConcurrentSkipListSet<>();

    private WebPage webPage;

    private volatile int i = 0;
    private AtomicInteger ii = new AtomicInteger(i);

    public SiteMap(String url) {
        this.url = url;
    }


    private WebPage createWebPage(String url) throws IOException, InterruptedException {
        WebPage webPage = new WebPage(url);
        webPage.parseWebPage();
        if (webPage.getPassMark()) {
            setCompletedPagesInList(url, webPage);
        }
        return webPage;
    }

    //1
    public synchronized Set<String> getLinksForGo() {
        return linksForGo;
    }

    //1
    public synchronized Map<String, WebPage> getListCompletedPages() {
        return listCompletedPages;
    }

    // 1
    private synchronized void setCompletedPagesInList(String url, WebPage webPage) {
        listCompletedPages.putIfAbsent(url, webPage);
    }

    // 1
    private void setLinksForGo(Set<String> lfg) {
        linksForGo.addAll(lfg);
    }

    private synchronized void setLinksForGo(String url) {
        if (!aBooleanLinksGoOut(url)) {
            linksForGo.add(url);
        }
    }

    private synchronized void setLinkOnBadLinkList(String url) {
        badLinkList.add(url);
    }

    // 1
    private void setBadLinkList(Set<String> sbll) {
        badLinkList.addAll(sbll);
    }

    // 1
    private void setLinksGoOut(String url) {
//        if (aBooleanLinksForGo(url)) {
//            linksForGo.removeIf(i -> i.equals(url));
//        }
        removeLinkFromGoList(url);
        linksGoOut.add(url);
    }

    // 1
    private void removeLinkFromGoList(String url) {
        if (aBooleanLinksForGo(url)) {
            linksForGo.removeIf(i -> i.equals(url));
        }
    }


    private boolean checkStatusLink(String url) {
        return new CheckLinkStatus().checkUrlStatus200(url);
    }

    private boolean aBooleanBadLinkList(String url) {
        return getBadLinkList().contains(url);
    }

    private boolean aBooleanLinksGoOut(String url) {
        return getLinksGoOut().contains(url);
    }

    private boolean aBooleanLinksForGo(String url) {
        return getLinksForGo().contains(url);
    }

    private boolean aBooleanlistCompletedPages(String url) {
        return getListCompletedPages().containsKey(url);
    }

    private void methodForCompute(WebPage webPage) {
        String url = webPage.getUrlAddress();
        webPage.setPassMark(false);
        System.out.println("Началась работа с WebPage - " + url + " , i = " + i + "!" + " " + Thread.currentThread().getName());
        Set<String> forGo = new HashSet<>();
        Set<String> forBad = new HashSet<>();
        for (String g : webPage.getNotSortAllLinks()) {
            if (!url.equals(g)) {
                boolean b = checkStatusLink(g);
                if (b) {
                    forGo.add(g);
                } else {
                    forBad.add(g);
                }
            }
        }
        System.out.println("Закончилась работа с WebPage - " + url + " , i = " + i + "!" + " " + Thread.currentThread().getName() + "\n");

        synchronized (webPage) {
            System.out.println("synchronized Начался круг - " + i + "!" + " " + Thread.currentThread().getName());
            setLinksForGo(forGo);
            setBadLinkList(forBad);
            setLinksGoOut(url);
            setCompletedPagesInList(url, webPage);
            System.out.println("synchronized Закончился круг - " + i + "!" + " " + Thread.currentThread().getName());
        }
    }


    @Override
    protected void compute() {
      Set <String> q = new ForkJoinPool().invoke(new WebPage(url));
        System.out.println("GGGGGGGGGGGGGGGGGGG");
    }


//        SiteMap siteMap = new SiteMap(url);
//
//        ForkJoinTask w = new WebPage(url);
//
//        while (true) {
//            if (getListCompletedPages().isEmpty()) {
//                System.out.println("Когда пустой лист!");
//                webPage = new WebPage(url);
//                webPage.invoke();
//                boolean st = checkStatusLink(url);
//                if (st) {
//                    System.out.println();
//                    webPage.setPassMark(false);
//                    System.out.println("Началась работа с WebPage - " + url + " , i = " + i + "!" + " " + Thread.currentThread().getName());
//                    Set<String> forGo = new HashSet<>();
//                    Set<String> forBad = new HashSet<>();
//                    for (String g : webPage.getNotSortAllLinks()) {
//                        if (!url.equals(g)) {
//                            boolean b = checkStatusLink(g);
//                            if (b) {
//                                forGo.add(g);
//                            } else {
//                                forBad.add(g);
//                            }
//                        }
//                    }
//                    System.out.println("Закончилась работа с WebPage - " + url + " , i = " + i + "!" + " " + Thread.currentThread().getName());
//
//                    synchronized (webPage) {
//                        System.out.println("synchronized Начался круг - " + i + "!" + " " + Thread.currentThread().getName());
//
//                        setLinksForGo(forGo);
//                        setBadLinkList(forBad);
//                        setLinksGoOut(url);
//                        setCompletedPagesInList(url, webPage);
//                        System.out.println("synchronized Закончился круг - " + i + "!" + " " + Thread.currentThread().getName() + "\n");
//                    }
//                    System.out.println(getLinksForGo().size());
//
//                } else {
//                    if (ii.get() == 5) {
//                        System.out.println("break SM " + " url - " + url + " " + Thread.currentThread().getName());
//                        break;
//                    }
//                    System.out.println("rere SM " + " url - " + url + " " + Thread.currentThread().getName());
//                    ii.getAndIncrement();
//                }
//            } else {
//// когда уже есть страницы
//                Iterator<String> iterator = getLinksForGo().iterator();
//                while (iterator.hasNext()) {
//                    int start = getLinksForGo().size();
//                    int end;
//
//                    String h = iterator.next();
//                    if (getBadLinkList().contains(h) || getLinksGoOut().contains(h)) {
//                        continue;
//                    }
//
//                    System.out.println("Когда не пустой лист!");
//                    webPage = new WebPage(h);
//                    Map<String, WebPage> mWebPage = getListCompletedPages();
//                    if (!mWebPage.containsKey(h)) {
//                        System.out.println();
//
//                        webPage.setPassMark(false);
//                        System.out.println("Началась работа с WebPage - " + h + " , i = " + i + "!" + " " + Thread.currentThread().getName());
//                        Set<String> forGo = new HashSet<>();
//                        Set<String> forBad = new HashSet<>();
//                        for (String g : webPage.getNotSortAllLinks()) {
//                            if (!url.equals(g)) {
//                                boolean b = checkStatusLink(g);
//                                if (b) {
//                                    forGo.add(g);
//                                } else {
//                                    forBad.add(g);
//                                }
//                            }
//                        }
//                        System.out.println("Закончилась работа с WebPage - " + h + " , i = " + i + "!" + " " + Thread.currentThread().getName());
//
//
//                        synchronized (webPage) {
//                            setLinksForGo(forGo);
//                            setBadLinkList(forBad);
//                            setLinksGoOut(url);
//                            setCompletedPagesInList(url, webPage);
//                        }
//
//                    }
//                    System.out.println("123123");
//                    end = getLinksForGo().size();
//                    System.out.println("Start while - " + start + " End " + end + " разниза между Start - End " + (end - start) + " " + Thread.currentThread().getName() + "\n");
//                }
//
//                if (getLinksForGo().size() == 0) {
//                    System.out.println("FINISH !!!!!!!!!!");
//                    break;
//                }
//
//            }
//
//        }
//
////        siteMapForkJoinTask.join();
//    }
}

//        if (st) {
//            if (getListCompletedPages().isEmpty()) {
//                webPage = new WebPage(url);
//                webPage.invoke();
//                if (webPage.getPassMark()) {
//                    setLinksGoOut(url);
//                    webPage.setPassMark(false);
//
//
//                    System.out.println("synchroяnized Закончился круг - " + i + "!" + " " + Thread.currentThread().getName());
//
//                }
////                    }
//            } else {
//
//                fork();
//                int iSize = getLinksForGo().size();
//                int iSizeN = getLinksForGo().size();
//                while (iSize > 0) {
//                    Iterator<String> iterator = getLinksForGo().iterator();
//
//                    while (iterator.hasNext()) {
//                        iSizeN = getLinksForGo().size();
//                        String h = iterator.next();
//
//                        if (!aBooleanlistCompletedPages(h)) {
//                            webPage = new WebPage(h);
//
//                            webPage.invoke();
//                            if (webPage.getPassMark()) {
//                                setLinksGoOut(h);
//                                webPage.setPassMark(false);
//                                Set<String> forGo = new HashSet<>();
//                                Set<String> forBad = new HashSet<>();
//                                for (String g : webPage.getNotSortAllLinks()) {
//                                    if (!h.equals(g)) {
//                                        boolean b = checkStatusLink(g);
//                                        if (b) {
//                                            forGo.add(g);
//                                        } else {
//                                            forBad.add(g);
//                                        }
//                                    }
//                                }
//                                setLinksForGo(forGo);
//                                setBadLinkList(forBad);
//                                setCompletedPagesInList(url, webPage);
//
//                            }
//                        }
//                        iSize = getLinksForGo().size();
//                        System.out.println("iSize while - " + iSize + " iSizeN " + iSizeN + " разниза между iSize - iSizeN " + (iSize - iSizeN) + " " + Thread.currentThread().getName());
//                    }
//                }
//                join();
//            }
//        } else {
//            System.out.println("rere SM " + " url - " + url + " " + Thread.currentThread().getName());
//        }




