package entity;

import actions.connection.CheckLinkStatus;
import actions.parse.parseFilters.ParseRelativeAndAbsoluteLinks;
import lombok.Getter;
import lombok.Setter;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveAction;
import java.util.concurrent.RecursiveTask;

public class WebPage extends RecursiveTask<Set<String>> {

    @Getter
    String domain;

    @Getter
    String protocol;

    @Getter
    String urlAddress;

    public boolean passMark;

    @Getter
    @Setter
    Set<String> notSortAllLinks = new ConcurrentSkipListSet<>();

//    public WebPage() {
//    }

    public WebPage(String url) {
        try {
//            invoke();
            urlAddress = url;
            this.protocol = new URL(url).getProtocol();
            this.domain = new URL(url).getAuthority();
            this.passMark = true;
//            notSortAllLinks.add(url);
            parseWebPage();
        } catch (MalformedURLException mUrlExc) {
            mUrlExc.printStackTrace();
        }
    }

    public void parseWebPage() {
        try {
            ParseRelativeAndAbsoluteLinks praal = new ParseRelativeAndAbsoluteLinks(this.urlAddress);
            if (praal.getAbsoluteLinkList() != null && !praal.getAbsoluteLinkList().isEmpty()) {
                setNotSortAllLinkss(praal.getAbsoluteLinkList());
            }
            if (praal.getRelativeLinkList() != null && !praal.getRelativeLinkList().isEmpty()) {
                List<String> fl = formattingListInternalLinks(praal.getRelativeLinkList());
                setNotSortAllLinkss(fl);
            }

        } catch (IOException e) {
            this.passMark = false;
        }
    }

    private void setNotSortAllLinkss(List<String> list) {
        Set<String> q = new HashSet<>(list);
        setNotSortAllLinks(q);
    }

    private List<String> formattingListInternalLinks(List<String> relativeLinkList) {
        List<String> formattingListInternalLinks = new ArrayList<>();
        for (String rl : relativeLinkList) {
            StringBuilder sb = new StringBuilder();
            formattingListInternalLinks.add(sb.append(this.protocol).append("://").append(this.domain).append(rl).toString());
        }
        return formattingListInternalLinks;
    }

    public synchronized boolean getPassMark() {
        return this.passMark;
    }

    public synchronized void setPassMark(boolean b) {
        this.passMark = b;
    }

    @Override
    protected Set <String> compute() {
//        WebPage webPage1 = new WebPage(urlAddress);
        Set<String> q = new ConcurrentSkipListSet<>();

        List<WebPage> subTask = new ArrayList<>();

        for (String links : this.getNotSortAllLinks()) {
            WebPage webPage = new WebPage(links);
            webPage.fork();
            subTask.add(webPage);
            System.out.println(Thread.currentThread().getName() + " 1");
        }

        for (WebPage webPage : subTask) {
            webPage.join();
            q.addAll(webPage.getNotSortAllLinks());
            System.out.println(Thread.currentThread().getName() + " 2");
        }
        System.out.println(q.size());
return q;
    }


}
