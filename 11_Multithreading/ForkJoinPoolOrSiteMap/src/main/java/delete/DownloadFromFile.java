package delete;

import lombok.Getter;
import lombok.Setter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.File;
import java.io.IOException;

public class DownloadFromFile {
    @Getter
    @Setter
    Document document2;

    @Getter
    String path;

    //*  для записи из локального файла
    public DownloadFromFile(File file) throws IOException {
        this.path = file.getPath();
        setDocument2(Jsoup.parse(file, "UTF-8"));
    }
}
