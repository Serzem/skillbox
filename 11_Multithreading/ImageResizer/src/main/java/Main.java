import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;

public class Main {
    private static int newWidth = 300;

    public static void main(String[] args) {
        String srcFolder = "data\\src";
        String dstFolder = "data\\dst";

        File srcDir = new File(srcFolder);

        long start = System.currentTimeMillis();

        File[] files = srcDir.listFiles(); // массив из файлов которые лежат в папке

        int countProcessors = Runtime.getRuntime().availableProcessors() / 2; //колв-во процессоров с защитой от виртуальных процессоров

        int lengthArray = files.length / countProcessors; // длинна массива для потока

        if ((lengthArray * countProcessors) < files.length || lengthArray < 1) {
            lengthArray += 1;
        }

        int startPosition = 0;
        File[] files1 = new File[lengthArray];

        int arrayLengthControl = files.length;


        for (int i = 0; i < countProcessors; i++) {

            if (arrayLengthControl < files1.length) {
                files1 = new File[arrayLengthControl];
            }

            System.arraycopy(files, startPosition, files1, 0, files1.length);
            ImageResizer resizer = new ImageResizer(files1, newWidth, dstFolder, start, "" + (i + 1));

            new Thread(resizer).start();

            startPosition += files1.length;

            arrayLengthControl -= files1.length;
        }
    }
}
