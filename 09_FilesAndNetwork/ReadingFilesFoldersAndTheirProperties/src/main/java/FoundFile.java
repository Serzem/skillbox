import ProjectExceptions.ExceptionObjOnPathDNExists;
import lombok.Getter;
import lombok.Setter;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileAttribute;
import java.util.ArrayList;
import java.util.List;

public class FoundFile {
    @Setter
    @Getter
    private List<BigDecimal> fileSize = new ArrayList<>();

    private MyFileVisitor myFileVisitor;

    public FoundFile(Path path) throws IOException, ExceptionObjOnPathDNExists {
        if (path.toString().length() < 1) {
            throw new ExceptionObjOnPathDNExists(path);
        }
        myFileVisitor = new MyFileVisitor(this);
        Files.walkFileTree(path, myFileVisitor);
    }

    public BigDecimal getSizeAllFiles() {
        BigDecimal sAF = BigDecimal.valueOf(0);
        for (BigDecimal fS : fileSize) {
            sAF = sAF.add(fS);
        }
        return sAF;
    }

    public void createDirectory(Path dir) throws IOException {
        Files.createDirectory(dir, (FileAttribute<?>) this.myFileVisitor);
    }

    public void copyFile(Path from, Path to) throws IOException {
        Files.copy(from, to);
    }

    public boolean getCheckFileExist(Path path) {
        return Files.exists(path);
    }


}
