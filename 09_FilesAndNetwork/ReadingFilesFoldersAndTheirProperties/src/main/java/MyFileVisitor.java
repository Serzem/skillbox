import java.math.BigDecimal;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

public class MyFileVisitor extends SimpleFileVisitor<Path> {

    List<BigDecimal> fileSize = new ArrayList<>();
    FoundFile foundFile;

    public MyFileVisitor(FoundFile foundFile) {
        this.foundFile = foundFile;
    }

    @Override
    public FileVisitResult preVisitDirectory(Path path, BasicFileAttributes attributes){
        return null;
    }

    @Override
    public FileVisitResult visitFile(Path path, BasicFileAttributes attributes) {
        fileSize.add(BigDecimal.valueOf(attributes.size()));
        foundFile.getSizeAllFiles();
        foundFile.setFileSize(fileSize);
        return FileVisitResult.CONTINUE;
    }

}
