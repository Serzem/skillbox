import ProjectExceptions.ExceptionObjOnPathDNExists;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.*;
import java.util.Arrays;
import java.util.Scanner;

public class Loader {
    private static final BigDecimal KILOBYTE = BigDecimal.valueOf(1048576L);
    private static final BigDecimal MEGABYTES = BigDecimal.valueOf(1048576L);
    private static final BigDecimal GIGABYTE = BigDecimal.valueOf(1073741824L);
    private static final BigDecimal TERABYTE = new BigDecimal("1099511627776");

    private static Logger logger;

    private static final String PATH_TEST = "W:\\Java RUSH\\SkillBOX\\Обучение\\Java разработчик с нуля\\lesson6\\09_FilesAndNetwork\\test";

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        logger = LogManager.getRootLogger();
        FoundFile foundFile;
        String input;
        Path way;

        System.out.println("Введите путь до папки: ");
        for (; ; ) {
            input = scanner.nextLine();
            way = Path.of(input);
            if (way.toString().equalsIgnoreCase("exit")) {
                break;
            }
            try {
                foundFile = new FoundFile(way);
                System.out.print("Размер папки " + way + " cоставляет ");
                String forInfo = "";
                BigDecimal count = foundFile.getSizeAllFiles();
                if (count.compareTo((KILOBYTE)) < 0) {
                    forInfo = count + " Байт";
                    System.out.println(forInfo);
                } else if (count.compareTo(MEGABYTES) < 0) {
                    count = count.divide(KILOBYTE, 2, RoundingMode.HALF_UP);
                    forInfo = count + " КБ";
                    System.out.println(forInfo);
                } else if (count.compareTo(GIGABYTE) < 0) {
                    count = count.divide(MEGABYTES, 2, RoundingMode.HALF_UP);
                    forInfo = count + " МБ";
                    System.out.println(forInfo);
                } else if (count.compareTo(TERABYTE) < 0) {
                    count = count.divide(GIGABYTE, 2, RoundingMode.HALF_UP);
                    forInfo = count + " ГБ";
                    System.out.println(forInfo);
                } else {
                    count = count.divide(TERABYTE, 2, RoundingMode.HALF_UP);
                    forInfo = count + " ТБ";
                    System.out.println(forInfo);
                }
                logger.info("Размер папки " + way + " cоставляет " + forInfo);

            } catch (ExceptionObjOnPathDNExists excp) {
                logger.error(excp.toString());
                Arrays.stream(excp.getStackTrace()).forEach(logger::error);
            } catch (Exception e) {
                logger.fatal(e.toString());
                Arrays.stream(e.getStackTrace()).forEach(logger::fatal);
            }
        }
        scanner.close();
    }
}
