package util;

import util.parse.urlOrHtmlFile.ParseConnections;
import util.parse.urlOrHtmlFile.ParseLine;
import util.parse.urlOrHtmlFile.ParsingStations;
import lombok.Getter;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.*;
import java.util.*;

public class WriteFileJson {
    @Getter
    private Map<String, String> datalines; /* name line + number line*/
    @Getter
    private Map<String, Map> dataStations; /* number line = positionSt + nameSt*/
    @Getter
    private Set<Map> dataConnections; /* {number Line + nameSt}*/
    @Getter
    File file;

    public WriteFileJson(ParseLine parseLine, ParsingStations parsingStations, ParseConnections parseConnections) {
        getDataForWrite(parseLine.getNameLineAndNumberLine(),
                parsingStations.getNumLineAndMapPosOnLineAndNameSt(),
                parseConnections.getConnections());
    }

    public void writeDataInFileJson(String path, String nameFile) throws IOException {
        CreateFileJson createFileJson = new CreateFileJson(path, nameFile);
        this.file = createFileJson.getFilePath().toFile();
        JSONObject stOnLine = new JSONObject();

        for (String station : dataStations.keySet()) {
            JSONArray arraySt = new JSONArray();
            for (Object fff : dataStations.get(station).keySet()) {
                arraySt.add(dataStations.get(station).get(fff));
            }
            stOnLine.put(station, arraySt);
        }
        JSONArray arrayLines = new JSONArray();
        for (String line : datalines.keySet()) {
            JSONObject numberAndNameLine = new JSONObject();
            numberAndNameLine.put("number", datalines.get(line));
            numberAndNameLine.put("name", line);
            arrayLines.add(0, numberAndNameLine);
        }

        JSONArray arrayConnections = new JSONArray();

        for (Iterator<Map> it = dataConnections.iterator(); it.hasNext(); ) {
            JSONArray arrayConnection = new JSONArray();
            Map connect = it.next();
            for (Object g : connect.keySet()) {
                JSONObject connectionObj = new JSONObject();
                String nameSt = g.toString();
                if (g.toString().length() > 3){
                    String [] gg = g.toString().split(" ");
                    nameSt = gg[0];
                }
                connectionObj.put("line", nameSt);
                connectionObj.put("station", connect.get(g.toString()));
                arrayConnection.add(connectionObj);
            }
            arrayConnections.add(0, arrayConnection);
        }

        JSONObject writeInBase = new JSONObject();
        writeInBase.put("stations", stOnLine);
        writeInBase.put("lines", arrayLines);
        writeInBase.put("connections", arrayConnections);

        if (file.canWrite()) {
            FileWriter fileWriter = new FileWriter(file);
//            fileWriter.write(writeInBase.toString());
            JSONObject.writeJSONString(writeInBase, fileWriter);
            fileWriter.flush();
        }
    }

    private void getDataForWrite(Map<String, String> dataLines, Map<String, Map> dataStations, Set<Map> dataConnections) {
        this.datalines = dataLines;
        this.dataStations = dataStations;
        this.dataConnections = dataConnections;
    }
}
