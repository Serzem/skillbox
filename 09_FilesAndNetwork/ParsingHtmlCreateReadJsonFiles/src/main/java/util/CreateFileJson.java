package util;

import lombok.Getter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CreateFileJson {
    @Getter
    Path filePath;

    public CreateFileJson(String path, String nameFile) throws IOException {
        createFileJson(Paths.get(path), nameFile);
    }

    private void createFileJson(Path path, String nameFile) throws IOException {
        String newPath = path.toString() + "\\" + nameFile + ".json";
        if (!Files.exists(path)) {
            Files.createDirectory(path);
            filePath = Files.createFile(Paths.get(newPath));
        } else {
            if (!Files.exists(Paths.get(newPath))) {
                filePath = Files.createFile(Paths.get(newPath));
            } else {
                filePath = Paths.get(newPath);
            }
        }
    }
}
