package util.getData;

import lombok.Getter;
import lombok.Setter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class FromJsonFile {

    @Setter
    @Getter
    String data;

    public FromJsonFile(String pathFile) throws IOException {
        StringBuilder builder = new StringBuilder();
        List<String> lines = Files.readAllLines(Paths.get(pathFile));
        lines.forEach(builder::append);
        setData(builder.toString());

    }
}
