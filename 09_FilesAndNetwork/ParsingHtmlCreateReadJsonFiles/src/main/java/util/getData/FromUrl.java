package util.getData;

import lombok.Getter;
import lombok.Setter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.*;

public class FromUrl {

    @Setter
    @Getter
    Document doc;

    public FromUrl(String url) throws IOException {
        setDoc(Jsoup.connect(url).maxBodySize(0).get());
//        System.out.println(doc);
    }

//    public void createFileHtml() throws Exception{
//        String COPY_TO = "data";
//        if (!Files.exists(Paths.get(COPY_TO))){
//            Files.createDirectory(Paths.get(COPY_TO));
//            Files.createFile(Paths.get(COPY_TO + "\\" + "metro" + ".html"));
//
//            FileInputStream fileInputStream = new FileInputStream(String.valueOf(doc));
//            FileOutputStream fileOutputStream = new FileOutputStream(COPY_TO + "\\" + "metro" + ".html");
//            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
//FileReader fileReader = new FileReader("");
//            bufferedOutputStream.flush();
//
//        }
//    }
}
