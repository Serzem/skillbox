package util.getData;

import lombok.Getter;
import lombok.Setter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.File;
import java.io.IOException;

public class FromHtmlFile {
    @Setter
    @Getter
    Document doc;

    public FromHtmlFile(String pathFile) throws IOException {
        File file = new File(pathFile);
        setDoc(Jsoup.parse(file, "UTF-8", "home.com"));
//        System.out.println(doc);
    }
}
