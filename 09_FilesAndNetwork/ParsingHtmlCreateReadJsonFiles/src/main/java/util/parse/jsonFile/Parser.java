package util.parse.jsonFile;

import pojo.StationIndex;
import util.getData.FromJsonFile;
import lombok.Getter;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class Parser {
    @Getter
    StationIndex stationIndex;
    ParseLines parseLines;
    ParseStations parseStations;
    ParseConnections parseConnections;

    public Parser(FromJsonFile getDataFromFile) throws Exception {
        this.stationIndex = new StationIndex();
        parseFile(getDataFromFile.getData());
    }

    public void parseFile(String data) throws Exception {
        JSONParser parser = new JSONParser();
        JSONObject jsonData = (JSONObject) parser.parse(data);

        JSONArray linesArray = (JSONArray) jsonData.get("lines");
        parseLines = new ParseLines(this.stationIndex);
        parseLines.parseLine(linesArray);

        JSONObject stationsObject = (JSONObject) jsonData.get("stations");
        parseStations = new ParseStations(this.stationIndex);
        parseStations.parseStations(stationsObject);

        JSONArray connectionsArray = (JSONArray) jsonData.get("connections");
        parseConnections = new ParseConnections(this.stationIndex);
        parseConnections.parseConnections(connectionsArray);
    }
}
