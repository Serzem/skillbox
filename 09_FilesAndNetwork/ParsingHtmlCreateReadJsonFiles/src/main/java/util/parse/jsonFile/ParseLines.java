package util.parse.jsonFile;

import pojo.StationIndex;
import util.core.Line;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class ParseLines {
    StationIndex stationIndex;

    public ParseLines(StationIndex stationIndex) {
        this.stationIndex = stationIndex;
    }

    public void parseLine(JSONArray linesArray) {
        linesArray.forEach(lineObject -> {
            JSONObject lineJsonObject = (JSONObject) lineObject;
            Line line = new Line(
                    (lineJsonObject.get("number").toString()),
                    (String) lineJsonObject.get("name")
            );
            stationIndex.addLine(line);
        });
    }
}
