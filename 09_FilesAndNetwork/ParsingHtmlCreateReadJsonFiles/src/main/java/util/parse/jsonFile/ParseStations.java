package util.parse.jsonFile;

import pojo.StationIndex;
import util.core.Line;
import util.core.Station;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class ParseStations {
    StationIndex stationIndex;

    public ParseStations(StationIndex stationIndex) {
        this.stationIndex = stationIndex;
    }

    public void parseStations(JSONObject stationsObject) {
        stationsObject.keySet().forEach(lineNumberObject ->
        {
            String lineNumber =  lineNumberObject.toString();
            Line line = stationIndex.getLine(lineNumber);
            JSONArray stationsArray = (JSONArray) stationsObject.get(lineNumberObject);
            stationsArray.forEach(stationObject ->
            {
                Station station = new Station((String) stationObject, line);
                stationIndex.addStation(station);
                line.addStation(station);
            });
        });
    }
}
