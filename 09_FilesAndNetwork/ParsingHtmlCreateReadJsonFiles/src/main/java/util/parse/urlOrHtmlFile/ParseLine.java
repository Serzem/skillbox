package util.parse.urlOrHtmlFile;

import lombok.Getter;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.HashMap;
import java.util.Map;

public class ParseLine extends ParseEntirePage {
    @Getter
    Map<String, String> nameLineAndNumberLine; /*name line + number line*/

    public ParseLine(Document document) {
        super(document);

        nameLineAndNumberLine = parseLineAndNumberLine(elements);
    }

    private Map parseLineAndNumberLine(Elements elements) {
        Map<String, String> asd = new HashMap<>();
        String q = elements.select("span.js-metro-line[data-line]").html();
        String[] qq = q.split("\n");
        for (String bb : qq) {
            asd.put(bb, elements.select("span.js-metro-line:contains(" + bb + ")").attr("data-line"));
        }
        return asd;
    }
}
