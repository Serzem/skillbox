package util.parse.urlOrHtmlFile;

import lombok.Getter;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.*;

public class ParseConnections extends ParseEntirePage {

    @Getter
    Set<Map> connections;

    public ParseConnections(Document document) {
        super(document);

        this.connections = parseConnections(elements);
    }

    private Set parseConnections(Elements elements) {
        Map<String, String> asd;
        Set<Map> connect = new HashSet<>();
        String numberLine;

        Elements elements2 = elements.select("div.js-metro-stations[data-line]");
        for (Element element : elements2) {
            numberLine = element.attr("data-line");
            Elements elements3 = element.select("a");
            for (Element element3_1 : elements3) {
                if (!element3_1.select("span.t-icon-metroln").attr("title").equals("")) {
                    Elements elements4 = element3_1.select("span.name, span.t-icon-metroln");
                    asd = new TreeMap<>();
                    for (Element element4_1 : elements4) {

                        if (!element4_1.select("span.name").html().equals("")) {
                            String nameSt = element4_1.select("span.name").html();
                            asd.put(numberLine, nameSt);
                        }
                        if (!element4_1.select("span[title]").attr("title").equals("")) {
                            String nameSt = element4_1.select("span[title]").attr("title");
                            nameSt = nameSt.substring(nameSt.indexOf('«') + 1, nameSt.indexOf('»'));

                            String[] className = element4_1.select("span[title]").first().className().split("ln-");
                            String crossingNumberLine = className[1];
                            if (asd.containsKey(crossingNumberLine)) {
                                asd.put(crossingNumberLine + " " + nameSt, nameSt);
                            } else {
                                asd.put(crossingNumberLine, nameSt);
                            }
                        }
                    }
                    connect.add(asd);
                }
            }
        }
        return connect;
    }
}
