package util.parse.urlOrHtmlFile;

import lombok.Getter;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.Map;
import java.util.TreeMap;

public class ParsingStations extends ParseEntirePage {
    @Getter
    Map<String, Map> numLineAndMapPosOnLineAndNameSt; /*number line + (position on line + name Stations)*/


    public ParsingStations(Document document) {
        super(document);
        numLineAndMapPosOnLineAndNameSt = parseNumberLinePositionOnLineStations(elements);
    }

    private Map parseNumberLinePositionOnLineStations(Elements elements) {
        Map<String, Map> numLineAndMapPosOnLineAndNameSt = new TreeMap<>();
        Map<Integer, String> positionOnLineAndNameSt;
        Elements elements1;
        elements1 = elements.select("div.js-metro-stations");
        for (Element element : elements1) {
            String numberLine = element.select("div.js-metro-stations[data-line]").attr("data-line");
            String allNamesStations = element.select("span.name").html();
            String[] arrayNamesStations = allNamesStations.split("\n");
            int positionOnLine = 1;
            positionOnLineAndNameSt = new TreeMap<>();
            for (String nameSt : arrayNamesStations) {
                positionOnLineAndNameSt.put(positionOnLine, nameSt);
                numLineAndMapPosOnLineAndNameSt.put(numberLine, positionOnLineAndNameSt);
                positionOnLine++;
            }
        }
        return numLineAndMapPosOnLineAndNameSt;
    }
}
