import util.WriteFileJson;
import util.getData.FromHtmlFile;
import util.getData.FromJsonFile;
import util.getData.FromUrl;
import util.parse.urlOrHtmlFile.ParseConnections;
import util.parse.urlOrHtmlFile.ParseLine;
import util.parse.urlOrHtmlFile.ParsingStations;
import util.parse.jsonFile.Parser;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Loader {

    private static final String ADRESS_SITE = "https://www.moscowmap.ru/metro.html#lines";
    private static final String ADRESS_FILE = "data\\metro.html";

    private static final String JSON_FILE_ABS_ADRESS = "data1\\test123.json";

    private static final String COPY_TO = "data1\\"; /*src\main\resources\*/

    public static void main(String[] args) {
        try {
            FromUrl getDataFromUrl = new FromUrl(ADRESS_SITE);
            WriteFileJson writeFileJson = new WriteFileJson(new ParseLine(getDataFromUrl.getDoc()),
                    new ParsingStations(getDataFromUrl.getDoc()),
                    new ParseConnections(getDataFromUrl.getDoc()));
            writeFileJson.writeDataInFileJson(COPY_TO, "test123_45");
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            FromHtmlFile getDataFromHtmlFile = new FromHtmlFile(ADRESS_FILE);
            WriteFileJson writeFileJson = new WriteFileJson(new ParseLine(getDataFromHtmlFile.getDoc()),
                    new ParsingStations(getDataFromHtmlFile.getDoc()),
                    new ParseConnections(getDataFromHtmlFile.getDoc()));
            writeFileJson.writeDataInFileJson(COPY_TO, "test123");

            Parser parser = new Parser(new FromJsonFile(JSON_FILE_ABS_ADRESS));
            parser.getStationIndex().getLine("11A").getStations().forEach(System.out::println);
            System.out.println(parser.getStationIndex().getAllLineAndStationsCount());
            System.out.println(parser.getStationIndex().getConnections().size());
            int i = 1;
            for (Object qq : parser.getStationIndex().getConnections().keySet()) {
                System.out.println(i + ") " + qq + ": " + parser.getStationIndex().getConnections().get(qq));
                i++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
