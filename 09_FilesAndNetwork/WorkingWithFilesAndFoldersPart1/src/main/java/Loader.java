import Actions.FoundFile;
import ProjectExceptions.ExceptionObjOnPathDNExists;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Scanner;

public class Loader {
    private static final BigDecimal KILOBYTE = BigDecimal.valueOf(1048576L);
    private static final BigDecimal MEGABYTES = BigDecimal.valueOf(1048576L);
    private static final BigDecimal GIGABYTE = BigDecimal.valueOf(1073741824L);
    private static final BigDecimal TERABYTE = new BigDecimal("1099511627776");

    private static Logger logger;

//    private static final String PATH_TEST = "W:\\test";
//    private static final String PATH_TEST2 = "W:\\test2";

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        logger = LogManager.getRootLogger();
        FoundFile foundFile;
        String input;
        Path way;

        try {

            for (; ; ) {
                System.out.println("Что сделать?");
                input = scanner.nextLine();
                if (input.equalsIgnoreCase("узнать размер файла/папки")) { /*W:\test*/

                    System.out.println("Введите путь до папки: ");
                    for (; ; ) {
                        input = scanner.nextLine();
                        way = Path.of(input);
                        if (way.toString().equalsIgnoreCase("exit")) {
                            break;
                        }

//                        try {
                        foundFile = new FoundFile(way);
                        System.out.print("Размер папки " + way + " cоставляет ");
                        String forInfo = "";
                        BigDecimal count = foundFile.getSizeAllFiles();
                        if (count.compareTo((KILOBYTE)) < 0) {
                            forInfo = count + " Байт";
                            System.out.println(forInfo);
                        } else if (count.compareTo(MEGABYTES) < 0) {
                            count = count.divide(KILOBYTE, 2, RoundingMode.HALF_UP);
                            forInfo = count + " КБ";
                            System.out.println(forInfo);
                        } else if (count.compareTo(GIGABYTE) < 0) {
                            count = count.divide(MEGABYTES, 2, RoundingMode.HALF_UP);
                            forInfo = count + " МБ";
                            System.out.println(forInfo);
                        } else if (count.compareTo(TERABYTE) < 0) {
                            count = count.divide(GIGABYTE, 2, RoundingMode.HALF_UP);
                            forInfo = count + " ГБ";
                            System.out.println(forInfo);
                        } else {
                            count = count.divide(TERABYTE, 2, RoundingMode.HALF_UP);
                            forInfo = count + " ТБ";
                            System.out.println(forInfo);
                        }
                        logger.info("Размер папки " + way + " cоставляет " + forInfo);

//                        } catch (ExceptionObjOnPathDNExists excp) {
//                            logger.error(excp.toString());
//                            Arrays.stream(excp.getStackTrace()).forEach(logger::error);
//                        } catch (Exception e) {
//                            logger.fatal(e.toString());
//                            Arrays.stream(e.getStackTrace()).forEach(logger::fatal);
//                        }
                    }

                } else if (input.equalsIgnoreCase("копировать файл")) {
                    System.out.println("Введите путь откуда копировать: ");
                    input = scanner.nextLine();
                    way = Path.of(input);
                    if (!Files.exists(way)) {
                        throw new ExceptionObjOnPathDNExists(way);
                    }
                    System.out.println("Введите путь куда копировать: ");
                    String inputTo = scanner.nextLine();
                    Path wayTo = Path.of(inputTo);

                    if (Files.exists(wayTo)) {
                        System.out.println("Файл уже существует!\nПерезаписать?");
                        input = scanner.nextLine();
                        if (input.equalsIgnoreCase("yes")) {
                            foundFile = new FoundFile(way);
                            foundFile.setPathTo(wayTo);
                            foundFile.copyFile(way, wayTo);
                        }
                    } else {
                        foundFile = new FoundFile(way);
                        foundFile.setPathTo(wayTo);
                        foundFile.createDirAndCopyFile(way, wayTo);

                    }
                }
            }
        } catch (ExceptionObjOnPathDNExists except) {
            except.printStackTrace();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                Thread.sleep(100);
            } catch (InterruptedException intE) {
                intE.printStackTrace();
            }
        }

    }
}
