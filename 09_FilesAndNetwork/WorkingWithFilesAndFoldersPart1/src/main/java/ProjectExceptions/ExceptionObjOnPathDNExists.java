package ProjectExceptions;

import java.nio.file.Path;

public class ExceptionObjOnPathDNExists extends Exception {
    public ExceptionObjOnPathDNExists(Path path) {
        super("Объект по указанному пути - " + "\"" + path.toString() + "\" не обнаружен!");
    }
}
