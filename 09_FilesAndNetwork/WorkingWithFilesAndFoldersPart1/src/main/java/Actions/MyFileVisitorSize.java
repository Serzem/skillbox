package Actions;

import Actions.FoundFile;
import ProjectExceptions.ExceptionObjOnPathDNExists;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MyFileVisitorSize extends SimpleFileVisitor<Path> {

    List<BigDecimal> fileSize = new ArrayList<>();
    FoundFile foundFile;

    public MyFileVisitorSize(FoundFile foundFile) {
        this.foundFile = foundFile;
    }

    @Override
    public FileVisitResult preVisitDirectory(Path path, BasicFileAttributes attributes) {
        return FileVisitResult.CONTINUE;
    }


    @Override
    public FileVisitResult visitFile(Path path, BasicFileAttributes attributes) {
        fileSize.add(BigDecimal.valueOf(attributes.size()));
//        foundFile.getSizeAllFiles();
        foundFile.setFileSize(fileSize);
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
//        Objects.requireNonNull(dir);

        if (exc != null) {
            throw exc;
        } else {
            return FileVisitResult.CONTINUE;
        }
    }

}
