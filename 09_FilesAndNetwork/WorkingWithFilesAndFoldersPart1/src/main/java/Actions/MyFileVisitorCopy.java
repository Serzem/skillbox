package Actions;


import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

public class MyFileVisitorCopy extends SimpleFileVisitor<Path> {
    FoundFile foundFile;

    public MyFileVisitorCopy(FoundFile foundFile) {
        this.foundFile = foundFile;
    }

    @Override
    public FileVisitResult preVisitDirectory(Path path, BasicFileAttributes attrs) throws IOException {
        File folder = new File(path.toString());
        File[] listOfFiles = folder.listFiles();
        Path pathTo;

        if (listOfFiles != null) {
            for (File file : listOfFiles) {
                pathTo = foundFile.getPathTo();

                Path newPathToFile = pathTo.relativize(Paths.get(file.getPath()));
                newPathToFile = Paths.get(pathTo + "\\" + newPathToFile.toString().substring(2));

                Path newPathToFolder = pathTo.relativize(Paths.get(file.getParent()));
                pathTo = Paths.get(pathTo + "\\" + newPathToFolder.toString().substring(2));

                if (!Files.exists(pathTo)) {
                    Files.createDirectories(pathTo);
                }
                if (Files.isDirectory(Paths.get(file.getPath()))) {
                    Files.createDirectories(newPathToFile);
                } else {
                    Files.copy(file.toPath(), pathTo.resolve(file.getName()), StandardCopyOption.REPLACE_EXISTING);
                }
            }
        }
        return FileVisitResult.CONTINUE;
    }
}
