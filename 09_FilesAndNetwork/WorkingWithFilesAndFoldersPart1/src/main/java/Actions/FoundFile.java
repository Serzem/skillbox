package Actions;

import ProjectExceptions.ExceptionObjOnPathDNExists;
import lombok.Getter;
import lombok.Setter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileAttribute;
import java.util.*;

public class FoundFile {
    @Setter
    @Getter
    private List<BigDecimal> fileSize = new ArrayList<>();

    @Getter
    @Setter
    private MyFileVisitorSize myFileVisitorSize = new MyFileVisitorSize(this);
    private MyFileVisitorCopy myFileVisitorCopy = new MyFileVisitorCopy(this);
    private Set<FileVisitOption> fileVisitOptions = new HashSet<>();

    @Getter
    @Setter
    private Path pathTo;

    @Setter
    @Getter
    private List<String> dataFile = new ArrayList<>();

    public FoundFile(Path path) throws IOException, ExceptionObjOnPathDNExists {
        if (path.toString().length() < 1) {
            throw new ExceptionObjOnPathDNExists(path);
        }
        Files.walkFileTree(path, myFileVisitorSize);
    }

    public BigDecimal getSizeAllFiles() {
        BigDecimal sAF = BigDecimal.valueOf(0);
        for (BigDecimal fS : fileSize) {
            sAF = sAF.add(fS);
        }
        return sAF;
    }

    public void copyFile(Path from, Path to) throws IOException {
            Files.walkFileTree(from, myFileVisitorCopy);
    }

    public void createDirAndCopyFile(Path from, Path to) throws IOException {
        Files.createDirectory(to);
        System.out.println("Создали новую директорию! - " + to);
        copyFile(from,to);
    }

}
