package Actions;

import lombok.Getter;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;

public class ParseSiteDownload {
    @Getter
    ArrayList<String> absUrlImages = new ArrayList<>();
    @Getter
    Elements elements = new Elements();

    public void setImgTagFromSiteDocument(Document document) {
        this.elements = document.select("img.g-picture");
    }

    public void setAbsUrlImagesInList(Document document) {
        setImgTagFromSiteDocument(document);
        this.elements.forEach(o -> {
            absUrlImages.add(o.absUrl("src"));
        });
    }
}
