package Actions;

import lombok.Getter;
import lombok.Setter;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;

public class CopyAllImageFromSite {
    @Setter
    @Getter
    ArrayList<String> urlImgLinkList = new ArrayList<>();

    @Getter
    ArrayList<String> nameImageList = new ArrayList<>();

    ParseSiteDownload parseSiteDownload = new ParseSiteDownload();

    String nameFromUrlImage;

    public CopyAllImageFromSite(SiteDownload siteDownload) throws IOException {
        parseSiteDownload.setAbsUrlImagesInList(siteDownload.getDocument());
        setUrlImgLinkList(parseSiteDownload.getAbsUrlImages());
        setNameImageList(urlImgLinkList);
    }

    public void copyAllImageFromSiteTo(String pathTo) throws IOException {
        for (String urlImage : urlImgLinkList) {
            InputStream in = new URL(urlImage).openStream();
            nameFromUrlImage = getNameFromUrlImage(urlImage);
            if (!Files.exists(Paths.get(pathTo))) {
                Files.createDirectory(Paths.get(pathTo));
                Files.copy(in, Paths.get(pathTo + "\\" + nameFromUrlImage), StandardCopyOption.REPLACE_EXISTING);
            } else {
                Files.copy(in, Paths.get(pathTo + "\\" + nameFromUrlImage), StandardCopyOption.REPLACE_EXISTING);
            }
        }
    }

    private String getNameFromUrlImage(String urlImage) {
        while (urlImage.contains("/")) {
            urlImage = urlImage.substring(urlImage.indexOf("/") + 1);
        }
        return urlImage;
    }

    private void setNameImageList(ArrayList<String> nameList) {
        nameList.forEach(o -> {
            nameImageList.add(getNameFromUrlImage(o));
        });
    }
}
