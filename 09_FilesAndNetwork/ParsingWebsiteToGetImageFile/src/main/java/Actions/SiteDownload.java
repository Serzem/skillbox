package Actions;

import lombok.Getter;
import lombok.Setter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

public class SiteDownload {
    @Getter
    @Setter
    Document document;

    @Getter
    String siteUrl = "";

    public SiteDownload(String url) throws IOException {
        siteUrl = url;
        setDocument(Jsoup.connect(siteUrl).get());
    }
}
