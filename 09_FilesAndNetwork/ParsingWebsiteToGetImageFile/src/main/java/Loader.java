import Actions.CopyAllImageFromSite;
import Actions.SiteDownload;


public class Loader {
    static final String URL_FROM = "https://lenta.ru";
    static final String URL_TO = "data"; /*$PROJECT_DIR$*/

    public static void main(String[] args) {

        try {
            CopyAllImageFromSite copyAllImageFromSite = new CopyAllImageFromSite(new SiteDownload(URL_FROM));
            copyAllImageFromSite.copyAllImageFromSiteTo(URL_TO);
            copyAllImageFromSite.getNameImageList().forEach(System.out::println);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
