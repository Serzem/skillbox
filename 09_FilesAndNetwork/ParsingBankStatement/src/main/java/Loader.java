
import bankStatement.processedDataBankStatement.*;

public class Loader {
    private static final String WAY_TO_DATA = "data/movementList.csv";


    public static void main(String[] args) {

        ListCompaniesOperationsReceiptExpense lcore = new ListCompaniesOperationsReceiptExpense(WAY_TO_DATA);
        System.out.println("Сумма доходов: " + lcore.formateDecimalForPrint(lcore.getTotalAllRevenues()) + " руб.");
        System.out.println("Сумма расходов: " + lcore.formateDecimalForPrint(lcore.getTotalAllExpenditure()) + " руб.\n");
        System.out.println("Суммы расходов по организациям:");
        for (String gg : lcore.amountExpensesOrganization()) {
            System.out.println(gg);
        }
    }
}
