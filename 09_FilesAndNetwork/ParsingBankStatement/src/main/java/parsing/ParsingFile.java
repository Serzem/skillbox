package parsing;

import bankStatement.BankStatement;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class ParsingFile {
    String[] fragment;
    List<String> columnList;

    public List<BankStatement> parsingFile(String wayFile) {
        ArrayList<BankStatement> bankStatements = new ArrayList<>();
        try {
            List<String> lines = Files.readAllLines(Paths.get(wayFile));
            for (String line : lines) {
                fragment = line.split(",");
                columnList = new ArrayList<>();
                for (String s : fragment) {
                    if (isQmarksPartText(s)) {
                        String lastPart = columnList.get(columnList.size() - 1);
                        columnList.set(columnList.size() - 1, lastPart + "." + s);
                        columnList.set(columnList.size() - 1, getRemoveQuotationMarks(columnList.get(columnList.size() - 1)));
                    } else {
                        columnList.add(s);
                    }
                }

                if (columnList.size() != 8) {
                    System.out.println("Wrong line: " + line);
                    continue;
                }
                bankStatements.add(new BankStatement(
                        columnList.get(0),
                        columnList.get(1),
                        columnList.get(2),
                        columnList.get(3),
                        columnList.get(4),
                        columnList.get(5),
                        columnList.get(6),
                        columnList.get(7)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bankStatements;
    }

    private boolean isQmarksPartText(String text) {
        String trimText = text.trim();
        return trimText.indexOf("\"") == trimText.lastIndexOf("\"") && trimText.endsWith("\"");
    }

    private String getRemoveQuotationMarks(String text) {
        StringBuilder newText = new StringBuilder();
        String[] splitText = text.split("\"");
        for (String fragment : splitText) {
            newText.append(fragment);
        }
        return newText.toString();
    }
}
