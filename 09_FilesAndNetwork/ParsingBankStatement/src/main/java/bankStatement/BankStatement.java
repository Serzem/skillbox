package bankStatement;

import lombok.Getter;
import lombok.Setter;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class BankStatement {

    @Setter
    @Getter
    private String typeAccount;
    @Setter
    @Getter
    private String numberAccount;
    @Setter
    @Getter
    private String currencyAccount;
    @Setter
    @Getter
    private String dateOperation;
    @Setter
    @Getter
    private String transactionReference;

    @Setter
    @Getter
    private String descriptionOperations;
    @Setter
    @Getter
    private String income;
    @Setter
    @Getter
    private String expenditure;

    public BankStatement(String typeAccount, String numberAccount, String currencyAccount, String dateOperation,
                         String transactionReference, String descriptionOperations, String income, String expenditure){
        this.typeAccount = typeAccount;
        this.numberAccount = numberAccount;
        this.currencyAccount = currencyAccount;
        this.dateOperation = dateOperation;
        this.transactionReference = transactionReference;
        this.descriptionOperations = descriptionOperations;
        this.income = income;
        this.expenditure = expenditure;
    }
}
