package bankStatement.processedDataBankStatement;

import bankStatement.BankStatement;
import bankStatement.dataProcessingOperations.OperationSelectingCounterpartyExpense;
import bankStatement.dataProcessingOperations.OperationSelectingCounterpartyIncome;
import lombok.Getter;
import lombok.Setter;
import parsing.ParsingFile;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class ListCompaniesOperationsReceiptExpense {
    @Getter
    @Setter
    private Map<String, List> incomeMap = new TreeMap<>(); //название фирмы + список приходов от неё
    @Getter
    @Setter
    private Map<String, List> expenditureMap = new TreeMap<>() ; //название фирмы + список расходов по ней

    OperationSelectingCounterpartyIncome osci = new OperationSelectingCounterpartyIncome();
    OperationSelectingCounterpartyExpense osce = new OperationSelectingCounterpartyExpense();
    String nameCompany;

   public ListCompaniesOperationsReceiptExpense(String wayFile) {
        ParsingFile parsingFile = new ParsingFile();
        List<BankStatement> bankStatements = parsingFile.parsingFile(wayFile);
        for (BankStatement forOperations : bankStatements) {
            if ((nameCompany = osci.getCompanyNameIncome(forOperations.getDescriptionOperations())).length() > 0) {
                if (incomeMap.isEmpty() || !incomeMap.containsKey(nameCompany)) {
                    List<String> listMoneyIncome = new ArrayList<>();
                    listMoneyIncome.add(forOperations.getIncome());
                    incomeMap.put(nameCompany, listMoneyIncome);
                } else if (incomeMap.containsKey(nameCompany)) {
                    List listMoneyIncome = incomeMap.get(nameCompany);
                    listMoneyIncome.add(forOperations.getIncome());
                    incomeMap.put(nameCompany, listMoneyIncome);
                }
            }
            if ((nameCompany = osce.getCompanyNameExpenditure(forOperations.getDescriptionOperations())).length() > 0) {
                if (expenditureMap.isEmpty() || !expenditureMap.containsKey(nameCompany)) {
                    List<String> listMoneyExpenditure = new ArrayList<>();
                    listMoneyExpenditure.add(forOperations.getExpenditure());
                    expenditureMap.put(nameCompany, listMoneyExpenditure);
                } else if (expenditureMap.containsKey(nameCompany)) {
                    List listMoneyExpenditure = expenditureMap.get(nameCompany);
                    listMoneyExpenditure.add(forOperations.getExpenditure());
                    expenditureMap.put(nameCompany, listMoneyExpenditure);
                }
            }
        }
    }

    public double getTotalAllRevenues() {
        double q = 0;
        for (String name : incomeMap.keySet()) {
            for (Object revenues : incomeMap.get(name)) {
                q += Double.parseDouble(revenues.toString());
            }
        }
        return q;
    }

    public double getTotalAllExpenditure() {
        double q = 0;
        for (String name : expenditureMap.keySet()) {
            for (Object expenditure : expenditureMap.get(name)) {
                q += Double.parseDouble(expenditure.toString());
            }
        }
        return q;
    }

    public String formateDecimalForPrint(double money) {
        DecimalFormat decimalFormat = new DecimalFormat("###,###.##");

        return decimalFormat.format(money);
    }

    public List<String> amountExpensesOrganization() {
        List<String> listOrg = new ArrayList<>();
        for (String name1 : expenditureMap.keySet()) {
            double q = 0;
            for (Object expenditure : expenditureMap.get(name1)) {
                q += Double.parseDouble(expenditure.toString());
            }
            listOrg.add(name1 + "\t\t" + formateDecimalForPrint(q) + " руб.");
        }
        return listOrg;
    }
}
