package bankStatement.dataProcessingOperations;

public class OperationSelectingCounterpartyIncome {

     public String getCompanyNameIncome(String text) {
        String[] splitText = text.split("( {2,1000})");
        int g = '/';
        StringBuilder bb = new StringBuilder();
        for (String s : splitText) {
            while (s.contains("/") && s.indexOf(g) != (s.length() - 1)) { /*&& s.indexOf(g) != (s.length() - 1)*/
                s = s.substring(s.indexOf(g) + 1);
                if (s.contains("/")) {
                    bb.append(" ").append(s, 0, s.indexOf(g));
                } else {
                    bb.append(" ").append(s);
                    break;
                }
            }
        }
        bb = new StringBuilder(bb.toString().trim());
        return bb.toString();
    }
}
