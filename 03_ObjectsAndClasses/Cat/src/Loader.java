
public class Loader {
    public static void main(String[] args) {
        Cat.getCountCat();
        Cat cat = new Cat();
        cat.setName("Вася");
        System.out.println("Есть хвост? " + cat.hasTail());
        System.out.println(cat.getName() + " окрас " +cat.getColour());
        cat.setCatColorType(CatColorType.BLUE);
        System.out.println(cat.getName() + " окрас " +cat.getColour());

        Cat cat1 = new Cat();
        cat1.setName("Петя");
        System.out.println("Есть хвост? " + (cat1.hasTail() ? "da" : "net"));
        System.out.println();

        System.out.println("Сколько всего котэ? " + cat.getCountCatValue());
        System.out.println();

        System.out.println(cat.getName() + " весит " + String.format("%,.2f", cat.getWeight()));
        cat.feed(999 - cat.getWeight());
        System.out.println("*****");
        System.out.println(cat.getStatus());
        System.out.println(cat.getCountCatValue());
        Cat.getCountCat();
        System.out.println("*****");

        System.out.println(cat.getName() + " весит " + cat.getWeight() + " и " + cat.getStatus());
        System.out.println("*****");
        System.out.println(cat.getStatus());
        System.out.println(cat.getCountCatValue());
        Cat.getCountCat();
        System.out.println("*****");
        cat.feed(10.0);
        System.out.println("*****");
        System.out.println(cat.getStatus());
        System.out.println(cat.getCountCatValue());
        Cat.getCountCat();
        System.out.println("*****");
        cat.drink(1.);
        System.out.println("*****");
        System.out.println(cat.getStatus());
        System.out.println(cat.getCountCatValue());
        Cat.getCountCat();
        System.out.println("*****");
        cat.pee();
        System.out.println("*****");
        System.out.println(cat.getStatus());
        System.out.println(cat.getCountCatValue());
        Cat.getCountCat();
        System.out.println("*****");
        cat.meow();
        System.out.println("*****");
        System.out.println(cat.getStatus());
        System.out.println(cat.getCountCatValue());
        Cat.getCountCat();
        System.out.println("*****");
        System.out.println();

        System.out.println("Сколько всего котэ? " + cat.getCountCatValue());
        System.out.println(cat.getName() + " Живой? " + cat.isALive());
        System.out.println(cat1.getName() + " " + cat1.getStatus() + ", его вес: "
                + String.format("%,.2f", cat1.getWeight()));
        cat1.drink(9000 - cat1.getWeight());
        System.out.println(cat1.getName() + " " + cat1.getStatus() + ", его вес: "
                + String.format("%,.2f", cat1.getWeight()));
        cat1.feed(1.);
        System.out.println(cat1.getName() + " " + cat1.getStatus() + ", его вес: "
                + String.format("%,.2f", cat1.getWeight()));
        cat1.pee();
        cat1.meow();
        cat1.feed(1.);
        cat1.drink(1.);

        Cat.getCountCat();

        Cat cat2 = new Cat();
        cat2.setName("Том");
        System.out.println("Есть хвост? " + (cat2.hasTail() ? "da" : "net"));

        System.out.println(cat2.getName() + " имеет вес: " + String.format("%,.2f", cat2.getWeight()));
        Cat.getCountCat();
        System.out.println("*****");
        System.out.println(cat2.getStatus());
        System.out.println(cat2.getCountCatValue());
        Cat.getCountCat();
        System.out.println("*****");
        cat2.pee();
        System.out.println("*****");
        System.out.println(cat2.getStatus());
        System.out.println(cat2.getCountCatValue());
        Cat.getCountCat();
        System.out.println("*****");
        cat2.meow();
        System.out.println("*****");
        System.out.println(cat2.getStatus());
        System.out.println(cat2.getCountCatValue());
        Cat.getCountCat();
        System.out.println("*****");
        System.out.println(cat2.getName() + " " + cat2.getStatus() + ", его вес: "
                + String.format("%,.2f", cat2.getWeight()));

        Cat cat3 = new Cat();
        System.out.println("Есть хвост? " + (cat3.hasTail() ? "da" : "net"));
        System.out.println("Сколько всего котэ? " + cat3.getCountCatValue());
        Cat.getCountCat();

        Cat cat4 = getKitten();
        System.out.println("Есть хвост? " + (cat4.hasTail() ? "da" : "net"));
        System.out.println(cat4.getWeight());
        Cat.getCountCat();

        Cat cat5 = getKitten();
        System.out.println("Есть хвост? " + (cat5.hasTail() ? "da" : "net"));
        System.out.println(cat5.getWeight());
        Cat.getCountCat();

        Cat cat6 = new Cat();
        cat6.setName("Тутси");
//        System.out.println("Есть хвост? " + (cat6.hasTail() ? "da" : "net"));
//        System.out.println(cat6.getColour());
        cat6.setCatColorType(CatColorType.BLACK);
//        System.out.println(cat6.getColour());
//        System.out.println("Живой? " + cat6.isALive());
        System.out.println(cat6.getName() + " Живой? " + cat6.isALive());
        System.out.println(cat6.getName() + " весит " + String.format("%,.2f", cat6.getWeight()));
        System.out.println(cat6.getName() + " окрас " +cat6.getColour());
        System.out.println(cat6.getName() + " Есть хвост? " + (cat6.hasTail() ? "da" : "net"));
        System.out.println(cat6.getName() + " оригинальный вес " + String.format("%,.2f", cat6.getOriginWeight()));
        System.out.println(cat6.getName() + " выпила " + String.format("%,.2f", cat6.getSummDrink()));
        System.out.println(cat6.getName() + " съела " + String.format("%,.2f", cat6.getSummFeed()));
        System.out.println();
        Cat.getCountCat();

        Cat cat7 = new Cat(cat6);
        System.out.println("copy " + cat7.getName() + " Живой? " + cat7.isALive());
        System.out.println("copy " + cat7.getName() + " весит " + String.format("%,.2f", cat7.getWeight()));
        System.out.println("copy " + cat7.getName() + " окрас " +cat7.getColour());
        System.out.println("copy " + cat7.getName() + " Есть хвост? " + (cat7.hasTail() ? "da" : "net"));
        System.out.println("copy " + cat7.getName() + " оригинальный вес " + String.format("%,.2f", cat7.getOriginWeight()));
        System.out.println("copy " + cat7.getName() + " выпила " + String.format("%,.2f", cat7.getSummDrink()));
        System.out.println("copy " + cat7.getName() + " съела " + String.format("%,.2f", cat7.getSummFeed()));
        System.out.println();
        Cat.getCountCat();

        Cat cat8 = new Cat();
        cat8.getCopyCat(cat7);
        System.out.println("copy " + cat8.getName() + " Живой? " + cat8.isALive());
        System.out.println("copy " + cat8.getName() + " весит " + String.format("%,.2f", cat8.getWeight()));
        System.out.println("copy " + cat8.getName() + " окрас " +cat8.getColour());
        System.out.println("copy " + cat8.getName() + " Есть хвост? " + (cat8.hasTail() ? "da" : "net"));
        System.out.println("copy " + cat8.getName() + " оригинальный вес " + String.format("%,.2f", cat8.getOriginWeight()));
        System.out.println("copy " + cat8.getName() + " выпила " + String.format("%,.2f", cat8.getSummDrink()));
        System.out.println("copy " + cat8.getName() + " съела " + String.format("%,.2f", cat8.getSummFeed()));
        System.out.println();
        Cat.getCountCat();

    }

    private static Cat getKitten() {
        return new Cat(1100.0);
    }
}