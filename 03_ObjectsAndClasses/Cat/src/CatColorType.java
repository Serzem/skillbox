public enum CatColorType {
    WHITE,
    BLACK,
    ORANGE,
    GREY,
    BLUE,
    SILVER,
    GOLD
}
