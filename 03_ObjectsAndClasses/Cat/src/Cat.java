import java.util.Map;

public class Cat {

    private double originWeight;
    private double weight;

    private static final double WEIGHT_MIN = 1000.0;
    private static final double WEIGHT_MAX = 9000.0;
    private static final int EYES_COUNT = 2;

    private String name;

    private double summFeed;
    private double summDrink;

    private static int countCat;

    private boolean live;

    private boolean tail = (0.5 * Math.random() > 0.1);

    private CatColorType catColorType = CatColorType.WHITE;

    public Cat() {
        setWeight();
        setOriginWeight();
        incrementCountCatValue();
        live = true;
        System.out.println("Нашли нового котэ!");
    }

    public CatColorType getColour() {
        return this.catColorType;
    }

    public void setCatColorType(CatColorType catColorType) {
        this.catColorType = catColorType;
    }

    public Cat(double weight) {
        this();
        this.weight = weight;
    }

    public Cat(Cat cat) {
        this.live = cat.live;
        this.catColorType = cat.catColorType;
        this.weight = cat.weight;
        this.originWeight = cat.originWeight;
        this.name = cat.name;
        this.tail = cat.tail;
        this.summDrink = cat.summDrink;
        this.summFeed = cat.summFeed;
        incrementCountCatValue();
    }

    public void getCopyCat(Cat cat) {
        this.live = cat.live;
        this.catColorType = cat.catColorType;
        this.weight = cat.weight;
        this.originWeight = cat.originWeight;
        this.name = cat.name;
        this.tail = cat.tail;
        this.summDrink = cat.summDrink;
        this.summFeed = cat.summFeed;
    }

    public int getCountCatValue() {
        return countCat;
    }

    public void incrementCountCatValue() {
        countCat += 1;
    }

    public static void getCountCat() {
        System.out.println("Общее количество котэ: " + countCat);
    }

    public void setName(String name) {
        this.name = name;
        System.out.println("Дали котэ кличку - " + this.name);
    }

    public String getName() {
        return this.name;
    }

    public void pee() {
        if (isALive()) {
            weight = weight - weight / 10;
        } else {
            System.out.println(getName() + " " + getStatus() + "!" + " Мёртвые не ходят в туалет!");
        }
        isALive();
    }

    public double getSummFeed() {
        System.out.print(getName() + " съел: ");
        return summFeed;
    }

    public double getSummDrink() {
        System.out.print(getName() + " выпил: ");
        return summDrink;
    }

    public void meow() {
        if (isALive()) {
            weight = weight - 1;
            System.out.println(getName() + " Meow");
        } else {
            System.out.println(getName() + " " + getStatus() + "!" + " Мёртвые не мяукают!");
        }
        isALive();
    }

    public void feed(Double amount) {
        if (isALive()) {
            weight = weight + amount;
            summFeed += amount;
        } else {
            System.out.println(getName() + " " + getStatus() + "!" + " Мёртвые не едят!");
        }
        isALive();
    }

    public void drink(Double amount) {
        if (isALive()) {
            weight = weight + amount;
            summDrink += amount;
        } else {
            System.out.println(getName() + " " + getStatus() + "!" + " Мёртвые не пьют!");
        }
        isALive();
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight() {
        weight = 1500.0 + 3000.0 * Math.random();
    }

    public Double getOriginWeight() {
        return this.originWeight;
    }

    public void setOriginWeight() {
        originWeight = weight;
    }

    public boolean isALive() {
        if (weight >= WEIGHT_MIN && weight <= WEIGHT_MAX && live) {
            return true;
        } else if (live) {
            countCat -= 1;
            return this.live = false;
        } else {
            return false;
        }

    }

    public boolean hasTail() {
        return tail;
    }

    public String getStatus() {
        if (weight < WEIGHT_MIN) {
            return "Dead";
        } else if (weight > WEIGHT_MAX) {
            return "Exploded";
        } else if (weight > originWeight && isALive()) {
            return "Sleeping";
        } else {
            return "Playing";
        }
    }
}