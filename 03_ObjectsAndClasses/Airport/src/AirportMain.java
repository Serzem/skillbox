import com.skillbox.airport.Airport;
import com.skillbox.airport.*;

import java.util.*;
import java.util.stream.Stream;

import static com.skillbox.airport.Flight.Type.ARRIVAL;
import static com.skillbox.airport.Flight.Type.DEPARTURE;

public class AirportMain {

    public static void main(String[] args) {
        Airport airport = Airport.getInstance();
        System.out.println("Полный список моделей самолётов: " + airport.getAllAircrafts());
        System.out.println("Всего самолётов: " + airport.getAllAircrafts().size());

        int summ = 0;

        for (int i = 0; i < airport.getTerminals().size(); i++) {
            summ += airport.getTerminals().get(i).getParkedAircrafts().size();
        }
        System.out.println("Всего самолётов в аэропорту: " + summ);
        System.out.println();

        Calendar calendar = new GregorianCalendar();
        long timeNow = calendar.getTimeInMillis();
        long twoHour = ((1000 * 60) * 60) * 2;
        long timeDeparture = timeNow + twoHour;

        airport.getTerminals().stream().filter(Objects::nonNull).map(Terminal::getFlights).filter(Objects::nonNull)
                .flatMap(flights -> flights.stream().filter(x -> (x.getDate().getTime() >= timeNow && x.getDate().getTime() <= timeDeparture && x.getType() != ARRIVAL))).forEach(System.out::println);
    }


}
