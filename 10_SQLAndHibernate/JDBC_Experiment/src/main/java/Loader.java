import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Loader {


    public static void main(String[] args) {
        String url = "jdbc:mysql://localhost:3306/skillbox?useSSL=false&serverTimezone=UTC";
        String name = "root";
        String pass = "testtest";

        String sql_1;
        String sql_2;

        try {
            Connection connection = DriverManager.getConnection(url, name, pass);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT name," +
                    " count(*)/(TIMESTAMPDIFF(MONTH,min(s.subscription_date),max(s.subscription_date))) as avgMonth FROM courses c " +
                    "JOIN subscriptions s ON c.id = s.course_id where year(s.subscription_date) = 2018 GROUP BY c.name;");

            while (resultSet.next()) {
                sql_1 = resultSet.getString("name");
                sql_2 = resultSet.getString("avgMonth");
                System.out.println(sql_1 + "\t" + sql_2);
            }

            resultSet.close();
            statement.close();
            connection.close();
        } catch (Exception exp) {
            exp.printStackTrace();
        }

    }

}
