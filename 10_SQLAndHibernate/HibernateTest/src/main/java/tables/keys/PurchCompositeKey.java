package tables.keys;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@EqualsAndHashCode
@ToString
public class PurchCompositeKey implements Serializable {
    @Column(name = "student_name", insertable = false, updatable = false)
    private String studentName;

    @Column(name = "course_name", insertable = false, updatable = false)
    private String courseName;

    public PurchCompositeKey() {
    }

    public PurchCompositeKey(String studentName, String courseName) {
        this.studentName = studentName;
        this.courseName = courseName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PurchCompositeKey courseName1 = (PurchCompositeKey) o;
        if (studentName != courseName1.studentName) return false;
        return courseName == courseName1.courseName;
    }

    @Override
    public int hashCode() {
        return Objects.hash(studentName, courseName);
    }
}
