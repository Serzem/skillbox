package tables.keys;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
@EqualsAndHashCode
@ToString
public class CompositeKey implements Serializable {

    @Column(name = "student_id", insertable = false, updatable = false)
    @Getter
    @Setter
    private Integer studentId;

    @Column(name = "course_id", insertable = false, updatable = false)
    @Getter
    @Setter
    private Integer courseId;
}
