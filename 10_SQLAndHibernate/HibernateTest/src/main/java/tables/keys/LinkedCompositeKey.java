package tables.keys;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@EqualsAndHashCode
@ToString
public class LinkedCompositeKey implements Serializable {


    @Column(name = "student_id", insertable = false, updatable = false)
    private Integer studentId;

    @Column(name = "course_id", insertable = false, updatable = false)
    private Integer courseId;

    public LinkedCompositeKey() {
    }

    public LinkedCompositeKey(Integer studentId, Integer courseId) {
        this.studentId = studentId;
        this.courseId = courseId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LinkedCompositeKey courseId1 = (LinkedCompositeKey) o;
        if (studentId != courseId1.studentId) return false;
        return courseId == courseId1.courseId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(studentId, courseId);
    }

}
