package tables.types;

public enum CourseType {
    DESIGN,
    PROGRAMMING,
    MARKETING,
    MANAGEMENT,
    BUSINESS
}
