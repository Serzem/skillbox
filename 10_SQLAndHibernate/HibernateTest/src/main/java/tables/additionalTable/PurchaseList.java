package tables.additionalTable;

import lombok.Getter;
import lombok.Setter;
import tables.keys.PurchCompositeKey;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "purchaselist")
public class PurchaseList implements Serializable {

    @EmbeddedId
    private PurchCompositeKey id;

    @Column(name = "student_name", insertable = false, updatable = false)
    @Getter
    @Setter
    private String studentName;

    @Column(name = "course_name", columnDefinition = "name", insertable = false, updatable = false)
    @Getter
    @Setter
    private String courseName;


    @Getter
    @Setter
    private Integer price;

    @Getter
    @Setter
    @Column(name = "subscription_date")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date subscriptionDate;

}
