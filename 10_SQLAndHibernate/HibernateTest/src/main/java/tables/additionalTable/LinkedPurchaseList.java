package tables.additionalTable;


import lombok.Getter;
import lombok.Setter;
import tables.keys.LinkedCompositeKey;
import tables.models.Course;
import tables.models.Student;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "linked_purchaseList")
public class LinkedPurchaseList implements Serializable {

    @EmbeddedId
    private LinkedCompositeKey id;

    @MapsId("studentId")
    @ManyToOne
    @Getter
    @Setter
    @JoinColumn(name = "student_id", insertable = false, updatable = false)
    private Student studentId;


    @MapsId("courseId")
    @ManyToOne
    @Getter
    @Setter
    @JoinColumn(name = "course_id", insertable = false, updatable = false)
    private Course coursetId;


    public LinkedPurchaseList() {
    }

    public LinkedPurchaseList(LinkedCompositeKey linkedCompositeKey) {
        this.id = linkedCompositeKey;
    }

    @Column(name = "student_name")
    @Getter
    @Setter
    private String studentName;

    @Column(name = "course_name")
    @Getter
    @Setter
    private String courseName;


    @Getter
    @Setter
    private Integer price;

    @Temporal(value = TemporalType.TIMESTAMP)
    @Column(name = "subscription_date")
    @Getter
    @Setter
    private Date subscriptionDate;
}
