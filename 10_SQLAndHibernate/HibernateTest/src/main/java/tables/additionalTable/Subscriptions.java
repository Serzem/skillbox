package tables.additionalTable;

import lombok.Getter;
import lombok.Setter;
import tables.keys.SubCompositeKey;
import tables.models.Course;
import tables.models.Student;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "subscriptions")
public class Subscriptions implements Serializable {

    @EmbeddedId
    private SubCompositeKey id;

    @ManyToOne(cascade = CascadeType.ALL)
    @Getter
    @Setter
    @JoinColumn(name = "student_id", insertable = false, updatable = false)
    private Student student;

    @ManyToOne(cascade = CascadeType.ALL)
    @Getter
    @Setter
    @JoinColumn(name = "course_id", insertable = false, updatable = false)
    private Course course;


    @Setter
    @Getter
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "subscription_date")
    private Date subscriptionDate;

}
