package tables.models;

import lombok.Getter;
import lombok.Setter;
import tables.additionalTable.PurchaseList;
import tables.additionalTable.Subscriptions;
import tables.types.CourseType;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "courses")
public class Course implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    private Integer id;

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private Integer duration;

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "enum")
    @Getter
    @Setter
    private CourseType type;

    @Getter
    @Setter
    private String description;

    @ManyToOne(cascade = CascadeType.ALL)
    @Getter
    @Setter
    private Teacher teacher;

    @Column(name = "students_count")
    @Getter
    @Setter
    private Integer studentCount;

    @Getter
    @Setter
    private Integer price;

    @Column(name = "price_per_hour")
    @Getter
    @Setter
    private Float pricePerHour;

    @Getter
    @Setter
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "Subscriptions",
            joinColumns = {@JoinColumn(name = "course_id")}
            ,
            inverseJoinColumns = {@JoinColumn(name = "student_id")})
    private List<Student> students;


    @OneToMany(mappedBy = "course",fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @Getter
    @Setter
    private List<Subscriptions> subscriptions;

    @OneToMany
    @Getter
    @Setter
    @JoinColumn(name = "course_name", referencedColumnName = "name")
    private List<PurchaseList> purchaseListsCourse;

}
