package tables.models;


import lombok.Getter;
import lombok.Setter;
import tables.additionalTable.PurchaseList;
import tables.additionalTable.Subscriptions;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "students")
public class Student implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    private Integer id;

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private Integer age;

    @Column(name = "registration_date")
    @Getter
    @Setter
    @Temporal(TemporalType.TIMESTAMP)
    private Date registrationDate;

    @OneToMany(mappedBy = "student", cascade = CascadeType.ALL)
    @Getter
    @Setter
    private List<Subscriptions> subscriptions;

    @OneToMany
    @Getter
    @Setter
    @JoinColumn(name = "student_name", referencedColumnName = "name")
    private List<PurchaseList> purchaseList;

}
