import org.hibernate.SessionFactory;
import org.hibernate.mapping.PrimaryKey;
import tables.additionalTable.LinkedPurchaseList;
import tables.additionalTable.PurchaseList;
import tables.keys.LinkedCompositeKey;
import tables.keys.PurchCompositeKey;
import tables.keys.SubCompositeKey;
import tables.models.Course;
import tables.additionalTable.Subscriptions;
import tables.models.Student;
import tables.models.Teacher;
import org.hibernate.Session;
import utils.HibernateUtil;

import java.util.List;

public class Loader {

    public static void main(String[] args) {
        SessionFactory factory = HibernateUtil.getSessionFactory();

        Session session = factory.getCurrentSession();
        session.beginTransaction();

        Course course = session.get(Course.class, 1);
        System.out.println("\n" + course.getName());

        List<PurchaseList> purchaseListList = course.getPurchaseListsCourse();
        System.out.println("\ncourse.getPurchaseListsCourse()\n");
        purchaseListList.stream().map(o -> o.getStudentName() + "\t" + o.getCourseName() + "\t" +
                o.getPrice() + "\t" + o.getSubscriptionDate()).forEach(System.out::println);
        System.out.println("[][]][][][][][][][][]][][][][][][][][]][][][][][][]");

        List<Subscriptions> courseSubscriptions = course.getSubscriptions();
        System.out.println("\ncourse.getSubscriptions()\n");
        for (Subscriptions cS : courseSubscriptions) {
            System.out.println("^^^^^^^^^^^^^^^^^^^");
            System.out.println("\ncourse.getSubscriptions().getCourse()\n");
            System.out.println("" + cS.getCourse().getName() + "\t" + cS.getStudent().getName() + "\t" + cS.getCourse().getType() + "\t" + cS.getSubscriptionDate() + "\n");
            System.out.println("\n cS.getStudent().getSubscriptions().getCourse \n");
            cS.getStudent().getSubscriptions().stream().map(Subscriptions::getCourse).map(x -> x.getId() + "\t" + x.getName() + "\t" + x.getType()).forEach(System.out::println);
            System.out.println("_____________________");

        }
        System.out.println("*********");

        courseSubscriptions.stream().map(Subscriptions::getStudent).map(x -> x.getName() + " " + x.getId() + "\t" + x.getRegistrationDate()).forEach(System.out::println);
        System.out.println();

        Teacher teacher = session.get(Teacher.class, 10);
        System.out.println("teacher name \t" + teacher.getName() + "\n");

        List<Course> coursesT = teacher.getCourse();
        System.out.println("teacher.getCourse()\n");
        coursesT.stream().map(x -> x.getId() + "\t" + x.getName() + "\t" + x.getType()).forEach(System.out::println);


        for (Course sub : coursesT) {
            System.out.println("{}{}{}{}{}{}{}{}{}");
            List<Subscriptions> tp = sub.getSubscriptions();
            System.out.println("\nКурс\t" + sub.getName() + "\t!!!!!!!!!!\n\tУченики");
            for (Subscriptions tp1 : tp) {
                System.out.println(tp1.getStudent().getName());
            }
        }
        System.out.println("||||||||||||||||||||||||||||||||||||||||");

        Student student = session.get(Student.class, 1);
        System.out.println(student.getName() + "\tid " + student.getId() + "\n");

        List<PurchaseList> purchaseList = student.getPurchaseList();
        System.out.println("\n" + "student.getPurchaseList()\t size - " + purchaseList.size() + "\n");
        for (PurchaseList asd : purchaseList) {
            System.out.println(asd.getPrice() + "\t" + asd.getStudentName() + "\t" + asd.getCourseName());
        }
        System.out.println();


        /*заполнение таблицы*/

        String hqlCourseTable = "From " + Course.class.getSimpleName();
        List<Course> courseList = session.createQuery(hqlCourseTable).getResultList();

        String hqlStudentTable = "From " + Student.class.getSimpleName();
        List<Student> studentList = session.createQuery(hqlStudentTable).getResultList();

        for (int stId = 0; stId <= (studentList.size() + 1); stId++) {
            for (int courseId = 0; courseId <= (courseList.size() + 1); courseId++) {
                Subscriptions subscriptions = session.get(Subscriptions.class, new SubCompositeKey(stId, courseId));
                if (subscriptions != null) {
                    String stName = subscriptions.getStudent().getName();
                    String courseName = subscriptions.getCourse().getName();
                    PurchaseList purchaseList1 = session.get(PurchaseList.class, new PurchCompositeKey(stName, courseName));

                    LinkedPurchaseList linkPurchaList = new LinkedPurchaseList(new LinkedCompositeKey(stId, courseId));

                    linkPurchaList.setStudentId(session.get(Student.class, stId));
                    linkPurchaList.setCoursetId(session.get(Course.class, courseId));
                    linkPurchaList.setStudentName(purchaseList1.getStudentName());
                    linkPurchaList.setCourseName(purchaseList1.getCourseName());
                    linkPurchaList.setPrice(purchaseList1.getPrice());
                    linkPurchaList.setSubscriptionDate(purchaseList1.getSubscriptionDate());

                    session.saveOrUpdate(linkPurchaList);

                }
            }
        }
        session.getTransaction().commit();

        session.close();

        HibernateUtil.shutDown();
    }
}
