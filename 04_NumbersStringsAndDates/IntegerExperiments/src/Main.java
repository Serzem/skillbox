public class Main {
    public static void main(String[] args) {
        Container container = new Container();
        container.count += 7843;
        sumDigits(container.count);
    }

    public static Integer sumDigits(Integer number) {
        //@TODO: write code here

        // first way
        int summNumber = 0;
        for (int i = 0; i < number.toString().length(); i++) {
            String q = number.toString().substring(i, i + 1);
            int qq = Integer.parseInt(q);
            summNumber += qq;
        }
        System.out.println(summNumber);

        //second way
        summNumber = 0;
        for (int i = 0; i < number.toString().length(); i++) {
            String stringPartOfNumber = number.toString().substring(i, i + 1);
            int intPartOfNumber = Integer.parseInt(Character.toString(stringPartOfNumber.charAt(0)));
//            int intPartOfNumber = Integer.parseInt(Character.toString(stringPartOfNumber.toString().substring(i,i+1).charAt(0)));
            summNumber += intPartOfNumber;
        }
        System.out.println(summNumber);

        //thirt way
        summNumber = 0;
        for (int i = 0; i < number.toString().length(); i++) {
            char charPartOfNumber = number.toString().charAt(i);
            int intPartOfNumber = Integer.parseInt(Character.toString(charPartOfNumber));
            summNumber += intPartOfNumber;
        }
        System.out.println(summNumber);

        return 0;
    }
}
