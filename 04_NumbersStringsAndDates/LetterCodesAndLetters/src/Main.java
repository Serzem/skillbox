import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        ArrayList<Integer> symb_Lower_Lat = new ArrayList<>();
        ArrayList<Integer> symb_Upper_Lat = new ArrayList<>();

        ArrayList<Integer> symb_Lower_Ru = new ArrayList<>();
        ArrayList<Integer> symb_Upper_Ru = new ArrayList<>();

        for (int i = 'a'; i <= 'z'; i++) {
            symb_Lower_Lat.add(i);
        }

        for (int i = 'A'; i <= 'Z'; i++) {
            symb_Upper_Lat.add(i);
        }

        for (int i = 'а'; i <= 'я'; i++) {
            symb_Lower_Ru.add(i);
        }

        for (int i = 'А'; i <= 'Я'; i++) {
            symb_Upper_Ru.add(i);
        }
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~" + "\n" + "* Латинская раскладка *" + "\n" + "~~~~~~~~~~~~~~~~~~~~~~~");
        for (int i = 0; i < symb_Lower_Lat.size(); i++) {
            System.out.println((char) ((int) symb_Upper_Lat.get(i)) + " - " + symb_Upper_Lat.get(i) + "\t" + "|" +
                    "\t" + (char) (int) symb_Lower_Lat.get(i) + " - " + symb_Lower_Lat.get(i));
        }
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~" + "\n" + "*** Русская раскладка ***" + "\n" + "~~~~~~~~~~~~~~~~~~~~~~~~~");

        for (int i = 0; i < symb_Lower_Ru.size(); i++) {
            System.out.println((char) ((int) symb_Upper_Ru.get(i)) + " - " + symb_Upper_Ru.get(i) + "\t" + "|" +
                    "\t" + (char) (int) symb_Lower_Ru.get(i) + " - " + symb_Lower_Ru.get(i));
        }

    }
}
