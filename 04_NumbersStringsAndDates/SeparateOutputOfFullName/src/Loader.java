import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Loader {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String fullName = scanner.nextLine();

        Pattern pattern = Pattern.compile("[[А-я][A-Z][a-z]]+");
        Matcher matcher = pattern.matcher(fullName);
        List<String> list = new ArrayList<>();

        while (matcher.find()) {
            list.add(0, fullName.substring(matcher.start(), matcher.end()));
        }

        if (list.size() < 4 && list.size() != 0) {
            String name = "";
            String middleName = "";
            String surname = "";
            if (list.size() > 2) {
                surname = list.get(list.size() - 3);
                middleName = list.get(list.size() - 2);
                name = list.get(list.size() - 1);

            } else if (list.size() == 2) {
                surname = list.get(list.size() - 2);
                name = list.get(list.size() - 1);

            } else if (list.size() == 1) {
                name = list.get(list.size() - 1);
            }

            System.out.println("Имя -|" + name);
            System.out.println("Отчество -|" + middleName);
            System.out.println("Фамилия -|" + surname);
        } else {
            System.out.println("\nВведите данные корректно!\n \n    Пример ввода\n \n Пётр" +
                    " Васильевич Сидоров");
        }
    }
}



