import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Loader {
    public static void main(String[] args) {
        // https://www.yahoo.com/news/leonardo-dicaprio-big-middle-finger-084052124.html
        String text = "Grant, a History Channel miniseries airing over three nights beginning on Memorial Day (May 25), is an overt—and timely—reclamation project. His reputation having faded over the past century because, as many here assert, the South’s “Lost Cause” rewriting of Civil War history invariably downplayed his accomplishments, Ulysses S. Grant is restored by this informative and entertaining TV documentary to the prototypical modern American hero. Based on Ron Chernow’s critically acclaimed 2017 biography of the same name, it’s a stirring tribute to an individual who embodied America’s finest ideals: hard work, determination, courage, resolve, and belief in democracy and equality for all, no matter the color of their skin.\n" +
                "\n" +
                "Executive produced by Leonardo DiCaprio, and featuring participation from numerous historians, writers and servicemen, including Chernow, Ta-Nehisi Coates and David Petraeus, Grant is a non-fiction tale about the intertwined self-definition of a man and a nation. Born on April 27, 1822, Grant grew up the working-class son of an Ohio tanner and merchant, and found his first calling as an accomplished horseman. Disinterested in taking over the family business, and having garnered the nickname “Useless Grant” as a kid, he was sent—without being asked—to West Point, where a typo bestowed him with the middle initial “S” (rather than “H,” for Hiram), thereby resulting in the more patriotic “US Grant” moniker. The reconfiguration of Grant’s name would continue once he joined President Abraham Lincoln’s Civil War army, his initials eventually coming to stand for “Unconditional Surrender” Grant due to his habit of securing definitive victory over his adversaries.\n" +
                "\n" +
                "Lance Armstrong Proves He’s No Michael Jordan—and Still a Huge Asshole\n" +
                "\n" +
                "‘Barkskins’ Is the Next Great TV Epic—and Perfect for Those Missing ‘Game of Thrones.’\n" +
                "\n" +
                "The evolution of Grant’s handle goes hand-in-hand with the upwards trajectory of his life. Post-military school graduation, Grant entered the infantry, and soon fell in love with and married Julia Dent, the daughter of a family that owned slaves—a situation that caused some friction for Grant and his own abolitionist clan. Triumphs in the Mexican-American War proved that he was preternaturally cool under pressure, but in the years immediately following that conflict, Grant left the service and fell on hard times, to the point of taking various odd jobs just to make sure his family didn’t starve. Even at his most destitute, however, he hewed to his convictions, freeing his only slave, William Jones—given to him by his father-in-law.\n" +
                "\n" +
                "The Civil War altered Grant’s fortunes forever, and after establishing the man’s backstory, this series roots itself in the commander’s rise up the ranks via a series of impressive and daring campaigns that confirmed his imposing mettle, intelligence, and strategic shrewdness. On the battlefields against a Confederate Army led by his West Point classmate Robert E. Lee, Grant exhibited canny tactical acumen and equally formidable tenacity, taking immense gambits (such as at Vicksburg, hailed as his “masterpiece,” where he seized control of the Mississippi River) and often pursuing enemies into hostile territory in order to attain decisive wins. Grant began to develop into a legend in the thick of warfare, and it’s there that Grant spends the majority of its time, recounting in exhaustive detail the many clashes that marked his Civil War tenure, and the famously daring and clever maneuvers that allowed him to eventually secure victory for the Union.\n" +
                "\n" +
                "Melding talking-head interviews and narrated excerpts from its subject’s memoirs with copious dramatic restagings of key events in his life, Grant’s formal approach takes some getting used to, especially at the outset. Fortunately, it settles into a rhythm, with its staged sequences providing momentum and weight to interviewees’ informative commentary about Grant’s exploits and mindset. From the catastrophic victory at Shiloh, to the heroic rescue at Chattanooga, to the bloody conflict in the Wilderness of Virginia, Grant’s recreations aren’t always as grand as one might like, resorting to soundbite-y dialogue and wannabe-mythic posing. Yet they’re sturdy and coherent complements to the show’s academic speakers, and they’re augmented considerably by excellent graphical maps and diagrams that lay out the specifics of Grant’s brilliant operations.\n" +
                "\n" +
                "In the aftermath of his Civil War service (and his beloved President Lincoln’s assassination), Grant was elected America’s 18th commander-in-chief, and while in office, he became renowned for spearheading Reconstruction, creating the Justice Department, and using that arm of the government to battle and prosecute the Ku Klux Klan. Though slandered throughout his life as a drunk, a butcher and a corrupt would-be dictator (the last slur courtesy of an administration dogged by scandal), Grant makes the convincing case that he was, first and foremost, a noble patriot. A staunch defender of the Union, he was convinced of the necessity for emancipation for African-American slaves, and of the evil of the Confederacy, whose members he often referred to as “rebels” and “traitors” to the grand democratic experiment of the United States.\n" +
                "\n" +
                "In this regard, Grant is an active attempt to rehabilitate the historical record, positing Confederate adversary Robert E. Lee as a symbol of the intolerant, aristocratic, treasonous old guard, and Grant as an emblem of a more open, just, unified modern America. Grant’s disgust for the Confederacy and the rancidness it stood for is on full display throughout this series, which pointedly contends that—good ol’ boy revisionism be damned—it was slavery, not simply the more euphemistic “states’ rights,” which drove the South to secede and take up arms against the Union. At the same time, Grant’s compassion and levelheadedness also remains front and center, epitomized by the lenient terms of surrender he ultimately offered to the defeated Lee, which helped him secure support throughout the South in the years following the end of the war.\n" +
                "\n" +
                "Grant’s prolonged focus on the lieutenant general’s most famous wartime decisions means that the series is directly aimed at those with a fondness for in-depth military history. Nonetheless, the context it provides about Grant’s life, both as a young man and as an eight-year resident of the Oval Office, deepens its argument about the titanic nature of his achievements, and the greatness of his character—both of which make him, no matter the vantage point, one of the true, indispensable founders of the American republic.";

        // https://catchenglish.ru/teksty/teksty-vyshe-srednej-slozhnosti/about-myself-3.html
        String text1 = "Hello! I am Jane. I am 21. I am a student. I consider it to be a special time in my life. Student life is always full of fun, new impressions and knowledge." +
                " I like being a student. Each day I learn something new and communicate with my friends. We meet almost every day." +
                " Sometimes we stay at the university after classes to prepare our homework for the next day or just to talk about our student life.\n" +
                "\n" +
                "I like spending time with my friends. We often visit each other. " +
                "I can talk with them for hours. They can help me and support me in any situation. I can say the same about my parents, with whom I live." +
                " My mother is a very wise woman. She understands me. We are not only close relatives, but also close friends. We have the same favourite colours — green and blue." +
                " These are the colours of nature and the sky. Our family hobby is travelling. We like seeing new places, meeting new people, exchanging our impressions." +
                " We often travel in summer and in winter. I adore Turkey, Egypt, and France. These countries have their own traditions, unique nature and culture.\n" +
                "\n" +
                "My other hobbies are music and theatre. I often visit the theatre. I sympathize with the characters on the stage." +
                " I try to understand them and, finally, I find it easier to solve my own problems watching the play. I understand my family and friends better." +
                " I am grateful to them for being so close to me, for their understanding and support.";

        int amountWords = 0;
        int amountWords1 = 0;

        Pattern pattern = Pattern.compile("[A-z]*[,]*[’]*[—]*[-]*[A-z]++");
        Matcher matcher = pattern.matcher(text);
        Matcher matcher1 = pattern.matcher(text1);

        while (matcher.find()) {
            System.out.println(text.substring(matcher.start(), matcher.end()));
            amountWords++;
        }

        System.out.println("————————————————————————————————————");

        while (matcher1.find()){
            System.out.println(text1.substring(matcher1.start(),matcher1.end()));
            amountWords1++;
        }
        System.out.println("————————————————————————————————————");
        System.out.println("Количество слов в первом тексте "  + amountWords);
        System.out.println("Количество слов во втором тексте " + amountWords1);
    }

}
