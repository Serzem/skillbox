import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Loader {

    public static void main(String[] args) {

        System.out.println();
        String text = "Вася заработал 5000 рублей, Петя - 7563.11 рубля, а Маша - 30000,121 рублей 1000.";
        System.out.println(text);
        String textSplit = text.replaceAll(",",".");
        double amount = 0.0;
        Pattern pattern = Pattern.compile("[0-9]+[0-9][?.]?[0-9]?[0-9]");
        Matcher matcher = pattern.matcher(textSplit);

        while (matcher.find()) {
            double foundResult =  Double.parseDouble(textSplit.substring(matcher.start(), matcher.end()));
            amount += foundResult;
        }

        System.out.println("Сумма заработка " + new BigDecimal(amount).setScale(2, RoundingMode.CEILING) + " рублей.");

    }
}