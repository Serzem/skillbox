
public class Main {
    public static void main(String[] args) {

        System.out.println("*********");
        System.out.println("byte Min value " + Byte.MIN_VALUE);
        System.out.println("byte Max value " + Byte.MAX_VALUE);

        System.out.println("*********");
        System.out.println("short Min value " + Short.MIN_VALUE);
        System.out.println("short Max value " + Short.MAX_VALUE);

        System.out.println("*********");
        System.out.println("integer Min value " + Integer.MIN_VALUE);
        System.out.println("integer Max value " + Integer.MAX_VALUE);

        System.out.println("*********");
        System.out.println("long Min value " + Long.MIN_VALUE);
        System.out.println("long Max value " + Long.MAX_VALUE);

        System.out.println("*********");
        System.out.println("float Min value " + Float.MAX_VALUE * -1);
        System.out.println("float Max value " + Float.MAX_VALUE);

        System.out.println("*********");
        System.out.println("double Min value " + Double.MAX_VALUE * -1);
        System.out.println("double Max value " + Double.MAX_VALUE);
        System.out.println("*********");
    }
}
