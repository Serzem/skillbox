
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Loader {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String phoneNumber = scanner.nextLine();
        String correctPhoneNumber = "";

        Pattern pattern = Pattern.compile("\\d+");
        Matcher matcher = pattern.matcher(phoneNumber);
        String convertedPhoneNumber = "";

        while (matcher.find()) {
            String numberFromNumber = phoneNumber.substring(matcher.start(), matcher.end());
            convertedPhoneNumber += numberFromNumber;
        }

        if (convertedPhoneNumber.length() == 11) {
            int firstSymbol = Integer.parseInt(convertedPhoneNumber.substring(0, 1));
            if (firstSymbol == 8) {
                correctPhoneNumber = convertedPhoneNumber.replaceFirst("8", "7");
            } else correctPhoneNumber = convertedPhoneNumber;

            System.out.println(correctPhoneNumber);
        } else System.out.println("Введите корректный номер телефона в любом формате!\n Например: 8 905 1234567 или +7 909 123-45-67.");

    }
}
//+7 909 123-45-67
//
//        +7 (989) 1234567
//
//        7-909-123-45-67
//
//        8 905 1234567