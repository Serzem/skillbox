package Accounts;
import Persons.*;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class PaymentAccount extends JointAccount {

    private String accountNumber = "";

    private static Map<String, String> paymentAccountNumber = new HashMap<>(); // accountNumber, idMember

    public PaymentAccount() {
    }

    public PaymentAccount(Person person) {
        createNewPaymentAccountNumber(person);
    }

    protected void withdrawAccountWithCommission(Person person, String accNumber, String money) {
    }

    private void createNewPaymentAccountNumber(Person person) {
        setPaymentAccountNumber(); //генерируем номер расчетного счета
        person.setAccountNumberAndBalance(accountNumber, BigDecimal.valueOf(0));
        person.setPaymentAccountNumber(accountNumber);
        setPaymentAccountNumber(accountNumber, person.getId()); //база всех расчетных счетов
        JointAccount.setPersonOnListMember(person); //обновляем инфу в общей базе клиентов
        System.out.println("Новый расчётный счёт № " + accountNumber + " для клиента - " + person.getFullName() + " " + person.getId() + " открыт!");
    }

    private void setPaymentAccountNumber() {
        boolean q = true;
        while (true) {
            int number = (int) (Math.random() * 10_000_000);
            if (number < 10) {
                accountNumber = "40701" + "810" + "1" + "4444" + "000000" + number;
            } else if (number < 100) {
                accountNumber = "40701" + "810" + "1" + "4444" + "00000" + number;
            } else if (number < 1000) {
                accountNumber = "40701" + "810" + "1" + "4444" + "0000" + number;
            } else if (number < 10_000) {
                accountNumber = "40701" + "810" + "1" + "4444" + "000" + number;
            } else if (number < 100_000) {
                accountNumber = "40701" + "810" + "1" + "4444" + "00" + number;
            } else if (number < 1_000_000) {
                accountNumber = "40701" + "810" + "1" + "4444" + "0" + number;
            } else if (number >= 1_000_000 && number < 9_999_999) {
                accountNumber = "40701" + "810" + "1" + "4444" + number;
            }

            if (checkPaymentAccountNumberInBase(accountNumber)) {
                break;
            } else if (!checkPaymentAccountNumberInBase(accountNumber)) {
                System.out.println("Что-то не то!");
                break;
            }
        }
    }

    protected static boolean checkPaymentAccountNumberInBase(String accNumber) {
        boolean q = false;
        if (!paymentAccountNumber.containsKey(accNumber)) {
            q = true;
        } else if (paymentAccountNumber.containsKey(accNumber) && paymentAccountNumber.size() >= 9_999_999) {
            System.out.println("База этого филиала пепеполнена!\nПора открывать дополнительный!");
        }
        return q;
    }

    public void checkingAccountsBalance(Person person) {
        super.checkingAccountsBalance(person);
    }

    protected static boolean checkPaymentAcc(String accNumber) {
        if (paymentAccountNumber.containsKey(accNumber)) {
            return true;
        } else {
            return false;
        }
    }

    public void putMoneyOnAccBalance(Person person, String accNumber, String money) {
        super.putMoneyOnAccBalance(person, accNumber, money);
    }

    public void withdrawAccountWithoutCommission(Person person, String accNumber, String money) {
        if (checkingFormatAmountsDouble(money)) {
            double amount1 = Double.parseDouble(money);
            takeMoneyOnAccountBalance(person, accNumber, BigDecimal.valueOf(amount1));
        } else if (checkingFormatAmountsInteger(money)) {
            int amount1 = Integer.parseInt(money);
            takeMoneyOnAccountBalance(person, accNumber, BigDecimal.valueOf(amount1));
        } else {
            System.out.println("Не верный формат суммы!");
        }
    }

    public void transferFromAccountToAccount(Person person, String accNumberFrom, String accNumberTo, String money) {
        super.transferFromAccountToAccount(person, accNumberFrom, accNumberTo, money);
    }

    private void setPaymentAccountNumber(String accountNumber, String idMember) {
        paymentAccountNumber.put(accountNumber, idMember);
    }

    protected void getOnPrintPersonAllPaymentAcc(Person person) {
        if (paymentAccountNumber.containsValue(person.getId())) {
            System.out.println(paymentAccountNumber.keySet());
//            paymentAccountNumber.get(person.getId());
        } else {
            System.out.println("Расчётных счетов с таким айди нет");
        }
    }
}
