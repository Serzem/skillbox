package Accounts;
import Persons.*;
import java.math.BigDecimal;
import java.util.*;

public class DepositAccount extends JointAccount {

    private String accountNumber = "";

    private static Map<String, String> depositAccountNumber = new HashMap<>(); // accountNumber, idMember

    public static final int DEPOSITTIMECOUNT = 30;

    public DepositAccount() {
    }

    public DepositAccount(Person person) {
        createNewDepositAccountNumber(person);
    }

    protected void withdrawAccountWithoutCommission(Person person, String accNumber, String money){}
    protected void withdrawAccountWithCommission(Person person, String accNumber, String money){}

    private void createNewDepositAccountNumber(Person person) {
        setAccountRandomNumber(); //генерируем номер депозитного счета
        person.setAccountNumberAndBalance(accountNumber, BigDecimal.valueOf(0));
        person.setDepositAccountNumber(accountNumber);
        setDepositAccountNumber(accountNumber, person.getId()); //база всех депозитных счетов
        JointAccount.setPersonOnListMember(person); //обновляем инфу в общей базе клиентов
        System.out.println("Новый депозитный счет для клиента - " + person.getFullName() + " " + person.getId() + " открыт!");
    }

    private static void setDepositAccountNumber(String accountNumber, String idMember) {
        depositAccountNumber.put(accountNumber, idMember);
    }

    private void setAccountRandomNumber() {

        while (true) {
            int number = (int) (Math.random() * 10_000_000);
            if (number < 10) {
                accountNumber = "42303" + "810" + "1" + "4444" + "000000" + number;
            } else if (number < 100) {
                accountNumber = "42303" + "810" + "1" + "4444" + "00000" + number;
            } else if (number < 1000) {
                accountNumber = "42303" + "810" + "1" + "4444" + "0000" + number;
            } else if (number < 10_000) {
                accountNumber = "42303" + "810" + "1" + "4444" + "000" + number;
            } else if (number < 100_000) {
                accountNumber = "42303" + "810" + "1" + "4444" + "00" + number;
            } else if (number < 1_000_000) {
                accountNumber = "42303" + "810" + "1" + "4444" + "0" + number;
            } else if (number >= 1_000_000 && number < 9_999_999) {
                accountNumber = "42303" + "810" + "1" + "4444" + number;
            }

            if (checkDepositAccountNumberInBase(accountNumber)) {
                break;
            } else if (!checkDepositAccountNumberInBase(accountNumber)) {
                System.out.println("Что-то не то!");
                break;
            }
        }
    }

    protected static boolean checkDepositAccountNumberInBase(String accNumber) {
        boolean q = false;
        if (!depositAccountNumber.containsKey(accNumber)) {
            q = true;
        } else if (depositAccountNumber.containsKey(accNumber) && depositAccountNumber.size() >= 9_999_999) {
            System.out.println("База этого филиала пепеполнена!\nПора открывать дополнительный!");
        }
        return q;
    }

    public void putMoneyOnDeposit(Person person, String accNumberFrom, String accNumberTo, String money) {
        if (checkPersonSnilsInBase(person.getSnilsNumber())) {
            if (checkingFormatInvoiceNumber(accNumberFrom) && checkingFormatInvoiceNumber(accNumberTo)) {
                if (checkingAvailabilityAccountsInDatabasesPayAndDep(accNumberFrom, accNumberTo)) {
                    if (checkingFormatAmountsDouble(money)) {
                        double amount1 = Double.parseDouble(money);
                        putMoneyOnDeposit(person, accNumberFrom, accNumberTo, BigDecimal.valueOf(amount1));
                    } else if (checkingFormatAmountsInteger(money)) {
                        int amount1 = Integer.parseInt(money);
                        putMoneyOnDeposit(person, accNumberFrom, accNumberTo, BigDecimal.valueOf(amount1));
                    } else {
                        System.out.println("Не верный формат суммы!");
                    }
                }
            }
        } else {
            System.out.println("Такого клиента нет в базе! ABS");
        }
    }

    private void putMoneyOnDeposit(Person person, String accNumberFrom, String accNumberTo, BigDecimal money) {
        if (getIsPossibleWithdrawMoneyWithoutCommission(person, accNumberFrom, money)) {
            Map <String, BigDecimal> personAccAndBalance = new TreeMap(person.getMapAccountBalance());
            person.setAccountNumberAndBalance(accNumberFrom, personAccAndBalance.get(accNumberFrom).subtract(money));
            person.setAccountNumberAndBalance(accNumberTo,personAccAndBalance.get(accNumberTo).add(money));
            Calendar calendar = new GregorianCalendar();
            calendar.getTimeInMillis();
//            calendar.set(2020, Calendar.AUGUST, 01, 20, 1, 1);
            long timeDeposit = calendar.getTimeInMillis();
            if (person.getDepositIdAndDepositNumberAndDepositTimeIn().isEmpty()) {
                String idDeposit = "" + 1;
                person.setDepositNumberAndDepositTimeIn(accNumberTo, timeDeposit);
                person.setDepositId(idDeposit, person.getDepositNumberAndDepositTimeIn());
//                System.out.println("Открыт депозит № " + idDeposit);
//                System.out.println(person.getDepositIdAndDepositNumberAndDepositTimeIn().get(idDeposit));
            } else {
                String idDeposit = "" + (person.getDepositIdAndDepositNumberAndDepositTimeIn().size() + 1);
                person.setDepositNumberAndDepositTimeIn(accNumberTo, timeDeposit);
                person.setDepositId(idDeposit, person.getDepositNumberAndDepositTimeIn());
//                System.out.println("Открыт депозит № " + idDeposit);
//                System.out.println(person.getDepositIdAndDepositNumberAndDepositTimeIn().get(idDeposit));
            }
        } else {
            System.out.println("Недостаточно средств для создания депозита!");
        }
    }

    public void getMoneyFromDeposit(String idDeposit, Person person, String accNumberTo, String accNumberFrom) {
        // при условии что депозит отлежал своё время перечисляет депозит на указанный пользователем счёт
        if (checkPersonSnilsInBase(person.getSnilsNumber())) {
            if (checkingFormatInvoiceNumber(accNumberTo)) {
//                person.getMoneyFromDeposit(idDeposit, accNumberTo, accNumberFrom);

                Calendar calendar = new GregorianCalendar();
                long dataFromDeposit = calendar.getTimeInMillis();

//                System.out.println(accNumberFrom);

                long timeOndeposit2 = (((((dataFromDeposit - person.getDepositNumberAndDepositTimeIn().get(accNumberFrom)) / 1000) / 60) / 60) / 24);

                if (timeOndeposit2 <= DepositAccount.DEPOSITTIMECOUNT) {
                    System.out.println("Депозит ещё не отлежался!");
                } else {
                    BigDecimal money = person.getMapAccountBalance().get(accNumberFrom);
                    person.setAccountNumberAndBalance(accNumberFrom, person.getMapAccountBalance().get(accNumberFrom).subtract(money));
                    person.setAccountNumberAndBalance(accNumberTo, person.getMapAccountBalance().get(accNumberTo).add(money));
//                    depositId.clear();
//                    depositNumberAndDepositTimeIn.clear();
                    System.out.println("Депозит закрыт!");
                }
            }
        } else {
            System.out.println("Такого клиента нет в базе! ABS");
        }
    }

    protected void getOnPrintPersonAllDepositAcc(Person person) {
        if (depositAccountNumber.containsValue(person.getId())) {
            System.out.println(depositAccountNumber.keySet());
        } else {
            System.out.println("Расчётных счетов с таким айди нет");
        }
    }

    @Override
    public void checkingAccountsBalance(Person person) {
        super.checkingAccountsBalance(person);
    }
}
