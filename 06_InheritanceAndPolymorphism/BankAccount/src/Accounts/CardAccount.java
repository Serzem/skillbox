package Accounts;

import Persons.*;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class CardAccount extends JointAccount {

    private String accountNumber = "";
    private final double COMISSIONAMOUNT = 1.0;

    private static Map<String, String> cardAccountNumber = new HashMap<>(); // accountNumber, idMember

    public CardAccount() {
    }

    public CardAccount(Person person) {
        createNewCardAccountNumber(person);
    }

    protected void withdrawAccountWithoutCommission(Person person, String accNumber, String money) {
    }

    private void createNewCardAccountNumber(Person person) {
        setCardAccountNumber(); //генерируем номер карточного счета
        person.setAccountNumberAndBalance(accountNumber, BigDecimal.valueOf(0));
        person.setCardAccountNumber(accountNumber);
        setCardAccountNumber(accountNumber, person.getId()); //база всех карточных счетов
        JointAccount.setPersonOnListMember(person); //обновляем инфу в общей базе клиентов
        System.out.println("Новый карточный счёт для клиента - " + person.getFullName() + " " + person.getId() + " открыт!");
    }

    private void setCardAccountNumber(String accountNumber, String idMember) {
        cardAccountNumber.put(accountNumber, idMember);
    }

    private void setCardAccountNumber() {
        boolean q = true;
        while (true) {
            int number = (int) (Math.random() * 10_000_000);
            if (number < 10) {
                accountNumber = "40817" + "810" + "1" + "4444" + "000000" + number;
            } else if (number < 100) {
                accountNumber = "40817" + "810" + "1" + "4444" + "00000" + number;
            } else if (number < 1000) {
                accountNumber = "40817" + "810" + "1" + "4444" + "0000" + number;
            } else if (number < 10_000) {
                accountNumber = "40817" + "810" + "1" + "4444" + "000" + number;
            } else if (number < 100_000) {
                accountNumber = "40817" + "810" + "1" + "4444" + "00" + number;
            } else if (number < 1_000_000) {
                accountNumber = "40817" + "810" + "1" + "4444" + "0" + number;
            } else if (number >= 1_000_000 && number < 9_999_999) {
                accountNumber = "40817" + "810" + "1" + "4444" + number;
            }

            if (checkCardAccountNumberInBase(accountNumber)) {
                break;
            } else if (!checkCardAccountNumberInBase(accountNumber)) {
                System.out.println("Что-то не то!");
                break;
            }
        }
    }

    protected static boolean checkCardAccountNumberInBase(String accNumber) {
        boolean q = false;
        if (!cardAccountNumber.containsKey(accNumber)) {
            q = true;
        } else if (cardAccountNumber.containsKey(accNumber) && cardAccountNumber.size() >= 9_999_999) {
            System.out.println("База этого филиала пепеполнена!\nПора открывать дополнительный!");
        }
        return q;
    }

    protected void checkingAccountsBalance(Person person) {
        super.checkingAccountsBalance(person);
    }

    public void putMoneyOnAccBalance(Person person, String accNumber, String money) {
        super.putMoneyOnAccBalance(person, accNumber, money);
    }

    public void withdrawAccountWithCommission(Person person, String accNumber, String money) {
        if (checkingFormatAmountsDouble(money)) {
            double amount1 = Double.parseDouble(money);
            double comission = (amount1 / 100) * COMISSIONAMOUNT;
            amount1 = (amount1 + comission);
            takeMoneyOnAccountBalance(person, accNumber, BigDecimal.valueOf(amount1));
        } else if (checkingFormatAmountsInteger(money)) {
            double amount1 = Integer.parseInt(money);
            double comission = (amount1 / 100) * COMISSIONAMOUNT;
            amount1 = (amount1 + comission);
            takeMoneyOnAccountBalance(person, accNumber, BigDecimal.valueOf(amount1));
        } else {
            System.out.println("Не верный формат суммы!");
        }
    }

    public void transferFromAccountToAccount(Person person, String accNumberFrom, String accNumberTo, String money) {
        super.transferFromAccountToAccount(person, accNumberFrom, accNumberTo, money);
    }

    protected static boolean checkCardAcc(String accNumber) {
        if (cardAccountNumber.containsKey(accNumber)) {
            return true;
        } else {
            return false;
        }
    }

    protected void getOnPrintPersonAllCardAcc(Person person) {
        if (cardAccountNumber.containsValue(person.getId())) {
            System.out.println(cardAccountNumber.keySet());
//            cardAccountNumber.get(person.getId());
        } else {
            System.out.println("Карточных счетов с таким айди нет");
        }
    }
}
