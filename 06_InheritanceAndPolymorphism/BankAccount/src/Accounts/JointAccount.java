package Accounts;

import Persons.*;

import java.math.BigDecimal;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

 abstract public class JointAccount {

    Person person = new Person();


    private static Map<Integer, Person> listMember = new HashMap<>(); // snils + Person

    private static Set<String> listIdAccount = new HashSet<>(); // list idAccount

    abstract protected void withdrawAccountWithoutCommission(Person person, String accNumber, String money);

    abstract protected void withdrawAccountWithCommission(Person person, String accNumber, String money);

    protected static boolean checkingFormatInvoiceNumber(String invoiceNumber) {
        //проверка,что номер счета введен корректно, т.е. состоит из только из цифр, которых менее и не более 20.
        boolean check = true;
        Pattern checkFormatNumberPattern = Pattern.compile("[0-9]*");
        Matcher checkFormatNumberMatcher = checkFormatNumberPattern.matcher(invoiceNumber);

        if (checkFormatNumberMatcher.matches() && invoiceNumber.length() != 20) {
            check = false;
            System.out.println("Не верный формат счета");
        } else if (!checkFormatNumberMatcher.matches()) {
            check = false;
            System.out.println("Не верный формат счета");
        }
        return check;
    }

    protected static boolean checkingFormatAmountsInteger(String amount) {
        //проверка введенной суммы, если сумма целое число.
        boolean check = true;
        Pattern checkFormatAmountPatternInteger = Pattern.compile("[0-9]+");
        Matcher checkFormatAmountMatcherInteger = checkFormatAmountPatternInteger.matcher(amount);

        if (!checkFormatAmountMatcherInteger.matches()) {
            check = false;
        }
        return check;
    }

    protected static boolean checkingFormatAmountsDouble(String amount) {
        //проверка введенной суммы, с плавающей точкой
        boolean check = true;
        Pattern checkFormatAmountPatternDouble = Pattern.compile("[0-9]+\\.[0-9][0-9]");
        Matcher checkFormatAmountMatcherDouble = checkFormatAmountPatternDouble.matcher(amount);

        if (!checkFormatAmountMatcherDouble.matches()) {
            check = false;
        }
        return check;
    }

    public static boolean checkPersonSnilsInBase(int snilsNumber) {
        return listMember.containsKey(snilsNumber);
    }

    public static void setIdInListIdAccount(String id) {
        listIdAccount.add(id);
    }

    protected static void setPersonOnListMember(Person person) {
        //добавляем нового клиента в общую базу банка
        listMember.put(person.getSnilsNumber(), person);
    }

    public static void createNewPaymentAccount(Person person) {
        PaymentAccount paymentAccount = new PaymentAccount(person);
    }

    public static void createNewDepositAccount(Person person) {
        DepositAccount depositAccount = new DepositAccount(person);
    }

    public static void createNewCardAccount(Person person) {
        CardAccount cardAccount = new CardAccount(person);
    }

    protected void putMoneyOnAccBalance(Person person, String accNumber, String money) {
        if (checkingFormatAmountsDouble(money)) {
            double amount1 = Double.parseDouble(money);

            Map<String, BigDecimal> accountsNumberAndBalance = new TreeMap<>(person.getMapAccountBalance());
            if (accountsNumberAndBalance.containsKey(accNumber) && (PaymentAccount.checkPaymentAcc(accNumber) || CardAccount.checkCardAcc(accNumber))) {
                person.setAccountNumberAndBalance(accNumber, accountsNumberAndBalance.get(accNumber).add(BigDecimal.valueOf(amount1)));
            } else {
                System.out.println("Пополнить можно только расчётный или карточный счёт!\nСчёт не найден, выберете другой!");
            }

        } else if (checkingFormatAmountsInteger(money)) {
            int amount1 = Integer.parseInt(money);

            Map<String, BigDecimal> accountsNumberAndBalance = new TreeMap<>(person.getMapAccountBalance());
            if (accountsNumberAndBalance.containsKey(accNumber) && (PaymentAccount.checkPaymentAcc(accNumber) || CardAccount.checkCardAcc(accNumber))) {
                person.setAccountNumberAndBalance(accNumber, accountsNumberAndBalance.get(accNumber).add(BigDecimal.valueOf(amount1)));
            } else {
                System.out.println("Пополнить можно только расчётный или карточный счёт!\nСчёт не найден, выберете другой!");
            }
        } else {
            System.out.println("Не верный формат суммы!");
        }
    }

    protected void takeMoneyOnAccountBalance(Person person, String accNumberFrom, BigDecimal money) {
        if (getIsPossibleWithdrawMoneyWithoutCommission(person, accNumberFrom, money)) {
            Map<String, BigDecimal> personAccAndBalance = new TreeMap<>(person.getMapAccountBalance());
            BigDecimal accNewBalance = personAccAndBalance.get(accNumberFrom).subtract(money);
            person.setAccountNumberAndBalance(accNumberFrom, accNewBalance);
        } else {
            System.out.println("Недостаточно средств для списания!");
        }
    }

    protected void transferFromAccountToAccount(Person person, String accNumberFrom, String accNumberTo, String money) {
        if (checkPersonSnilsInBase(person.getSnilsNumber())) {
            if (checkingFormatInvoiceNumber(accNumberFrom) && checkingFormatInvoiceNumber(accNumberTo)) {
                if (checkingAvailabilityAccountsInDatabasesPayAndDep(accNumberFrom, accNumberTo)) {
                    if (checkingFormatAmountsDouble(money)) {
                        double amount1 = Double.parseDouble(money);

                        if (JointAccount.getIsPossibleWithdrawMoneyWithoutCommission(person, accNumberFrom, BigDecimal.valueOf(amount1))) {
                            Map<String, BigDecimal> accountsNumberAndBalance = new TreeMap<>(person.getMapAccountBalance());
                            person.setAccountNumberAndBalance(accNumberFrom, accountsNumberAndBalance.get(accNumberFrom).subtract(BigDecimal.valueOf(amount1)));
                            person.setAccountNumberAndBalance(accNumberTo, accountsNumberAndBalance.get(accNumberTo).add(BigDecimal.valueOf(amount1)));
                        } else {
                            System.out.println("Недостаточно средств для перевода!");
                        }

                    } else if (checkingFormatAmountsInteger(money)) {
                        int amount1 = Integer.parseInt(money);

                        if (JointAccount.getIsPossibleWithdrawMoneyWithoutCommission(person, accNumberFrom, BigDecimal.valueOf(amount1))) {
                            Map<String, BigDecimal> accountsNumberAndBalance = new TreeMap<>(person.getMapAccountBalance());
                            person.setAccountNumberAndBalance(accNumberFrom, accountsNumberAndBalance.get(accNumberFrom).subtract(BigDecimal.valueOf(amount1)));
                            person.setAccountNumberAndBalance(accNumberTo, accountsNumberAndBalance.get(accNumberTo).add(BigDecimal.valueOf(amount1)));
                        } else {
                            System.out.println("Недостаточно средств для перевода!");
                        }
                    } else {
                        System.out.println("Не верный формат суммы!");
                    }
                }
            }
        } else {
            System.out.println("Такого клиента нет в базе! ABS");
        }
    }

    public static boolean checkingIdInBase(String id) {
        return listIdAccount.contains(id);
    }

    protected void checkingAccountsBalance(Person person) {
        //выводит на просмотр остатки по всем счетам клиента
        if (checkPersonSnilsInBase(person.getSnilsNumber())) {
            if (!person.getMapAccountBalance().isEmpty()) {
                person.getAllAccountBalance();
            } else {
                System.out.println("У клиента нет счета");
            }
        } else {
            System.out.println("Такого клиента нет в базе!");
        }
    }

    protected boolean checkingAvailabilityAccountsInDatabasesPayAndDep(String accNumberPay, String accNumberDep) {
        //проверка наличия счетов в базах, когда нам нужно наличие счетов в двух базах
        if (PaymentAccount.checkPaymentAccountNumberInBase(accNumberPay) && (DepositAccount.checkDepositAccountNumberInBase(accNumberDep) || CardAccount.checkCardAccountNumberInBase(accNumberDep))) {

            return true;
        } else if (CardAccount.checkCardAccountNumberInBase(accNumberPay) && PaymentAccount.checkPaymentAccountNumberInBase(accNumberDep)) {
            return true;
        } else {
            System.out.println("Таких счетов нет в базе");
            return false;
        }
    }

    protected static boolean getIsPossibleWithdrawMoneyWithoutCommission(Person person, String accNumberFrom, BigDecimal money) {
        // говорит о возможности списать указанную сумму со счёта
        Map<String, BigDecimal> personAccAndBalance = new TreeMap<>(person.getMapAccountBalance());
        return (personAccAndBalance.get(accNumberFrom).subtract(money).compareTo(BigDecimal.ZERO) == 0) ||
                (personAccAndBalance.get(accNumberFrom).subtract(money).compareTo(BigDecimal.ZERO) > 0);
    }

    public static Person findMember(int snilsNumber) {

        if (listMember.containsKey(snilsNumber)) {
            return listMember.get(snilsNumber);
        } else {
            Person person = new Person();
            System.out.println("Такого клиента нет в базе");
            return person;
        }
    }

    public static void getOnPrintAllClient(){
        System.out.println(listMember);
    }
}
