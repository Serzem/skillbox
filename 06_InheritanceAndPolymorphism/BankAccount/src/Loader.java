

import Persons.*;
import Accounts.CardAccount;
import Accounts.DepositAccount;
import Accounts.JointAccount;
import Accounts.PaymentAccount;

import java.util.Scanner;

public class Loader {
    public static void main(String[] args) {

       Scanner scanner = new Scanner(System.in);
        boolean stop = true;
        Person person1 = new Person();

        while (stop) {
            System.out.println("Привет! Что нужно сделать?");
            String dataInput = scanner.nextLine();


            if (dataInput.equalsIgnoreCase("exit")) {
                System.out.println("Вы вышли из приложения!");
                break;
            } else if (dataInput.equalsIgnoreCase("help")) {
                System.out.println("help");
                System.out.println("exit");
                System.out.println("open new payment account");
                System.out.println("open deposit acc");
                System.out.println("top up account");
                System.out.println("make a transfer of funds");
                System.out.println("Take money from Card account");
                System.out.println("withdraw money from the Deposit");
                System.out.println("see all list in bank");
                System.out.println("see all acc this Member");
                System.out.println("find client in data base");
                System.out.println("put Money On Deposit");


            } else if (dataInput.equalsIgnoreCase("open new payment account")) {

                System.out.println("Напишите Ваше fullName");
                dataInput = scanner.nextLine();
                System.out.println("Напишите Ваш СНИЛС");
                String dataInput2 = scanner.nextLine();
                person1 = new Person(dataInput, Integer.parseInt(dataInput2));
                PaymentAccount paymentAccount = new PaymentAccount(person1);
                boolean stop2 = true;

                while (stop2) {
                    System.out.println("Какие ещё операции необходимо совершить клиенту?");
                    dataInput = scanner.nextLine();
                    if (dataInput.equalsIgnoreCase("see all acc this Member")) {
                        System.out.println(person1.getMapAccountBalance());
                    } else if (dataInput.equalsIgnoreCase("open deposit acc")) {
                        DepositAccount depositAccount = new DepositAccount(person1);
                    } else if (dataInput.equalsIgnoreCase("open card acc")) {
                        CardAccount cardAccount = new CardAccount(person1);
                    }
                    else if (dataInput.equalsIgnoreCase("exit")) {
                        System.out.println("Вышли из карточки клиента");
                        stop2 = false;
                    } else if (dataInput.equalsIgnoreCase("make a transfer of funds from PaymentACC on CardACC")) {
                        System.out.println("Укажите номер счета с которого необходимо списать средства!");
                        String dataInputFromAcc = person1.getPaymentAccountNumber();
                        System.out.println(dataInputFromAcc);
                        System.out.println("Укажите номер счета на который необходимо перечислить денежные средства!");
                        String dataInputInAccTo = person1.getCardAccountNumber();
                        System.out.println("Введите сумму перевода!");
                        String dataInputMoney = scanner.nextLine();
                        paymentAccount.transferFromAccountToAccount(person1, dataInputFromAcc, dataInputInAccTo, dataInputMoney);
                        person1.getAllAccountBalance();
                    } else if (dataInput.equalsIgnoreCase("top up account")) {
                        System.out.println("Укажите какой счёт необходимо пополнить?");
                        String dataInputTopUpAcc = scanner.nextLine();
                        System.out.println("Укажите сумму пополнения!");
                        String dataInputMoney = scanner.nextLine();
                        paymentAccount.putMoneyOnAccBalance(person1, dataInputTopUpAcc, dataInputMoney);
                        person1.getAllAccountBalance();
                    } else if (dataInput.equalsIgnoreCase("Take money from Card account")) {
                        CardAccount cardAccount = new CardAccount();
                        System.out.println("С какого счёта снять деньги?");
                        String accNumber = person1.getCardAccountNumber();
                        System.out.println("Какая сумма?");
                        String moneyAmount = scanner.nextLine();
                        cardAccount.withdrawAccountWithCommission(person1, accNumber, moneyAmount);
                        person1.getAllAccountBalance();
                    } else if (dataInput.equalsIgnoreCase("withdraw money from the Deposit")) {
                        DepositAccount depositAccount = new DepositAccount();
                        System.out.println("Укажите айди вклада");
                        String idDep = scanner.nextLine();
                        System.out.println("Укажите куда хотите перечислить вклад");
                        String accTo = person1.getPaymentAccountNumber();
                        System.out.println(accTo);
                        System.out.println("Откуда взять деньги?");
                        String accFrom = person1.getDepositAccountNumber();
                        System.out.println(accFrom);
                        depositAccount.getMoneyFromDeposit(idDep, person1, accTo, accFrom);
                    } else if (dataInput.equalsIgnoreCase("put Money On Deposit")) {
                        DepositAccount depositAccount = new DepositAccount();
                        System.out.println("Откуда списать деньги?");
                        String accFrom = person1.getPaymentAccountNumber();
                        System.out.println("Куда перечислить деньги?");
                        String accTo = person1.getDepositAccountNumber();
                        System.out.println("Сколько денег положить на депозит?");
                        String money = scanner.nextLine();
                        depositAccount.putMoneyOnDeposit(person1, accFrom, accTo, money);
                    }
                }

            } else if (dataInput.equalsIgnoreCase("see all acc this Member")) {
                System.out.println(person1.getMapAccountBalance());
            } else if (dataInput.equalsIgnoreCase("find client in data base")) {
                System.out.println("Введите Ваш СНИЛС");
                String snilsNumber = scanner.nextLine();
                person1 = JointAccount.findMember(Integer.parseInt( snilsNumber));
            } else if (dataInput.equalsIgnoreCase("make a transfer of funds from PaymentACC on CardACC")) {
                PaymentAccount paymentAccount = new PaymentAccount();
                System.out.println("Укажите номер счета с которого необходимо списать средства!");
                String dataInputFromAcc = person1.getPaymentAccountNumber();
                System.out.println(dataInputFromAcc);
                System.out.println("Укажите номер счета на который необходимо перечислить денежные средства!");
                String dataInputInAccTo = person1.getCardAccountNumber();
                System.out.println("Введите сумму перевода!");
                String dataInputMoney = scanner.nextLine();
                paymentAccount.transferFromAccountToAccount(person1, dataInputFromAcc, dataInputInAccTo, dataInputMoney);
                person1.getAllAccountBalance();
            } else if (dataInput.equalsIgnoreCase("open deposit acc")) {
                DepositAccount depositAccount = new DepositAccount(person1);
            } else if (dataInput.equalsIgnoreCase("top up account")) {
                System.out.println("Укажите какой счёт необходимо пополнить?");
                String dataInputTopUpAcc = scanner.nextLine();
                System.out.println("Укажите сумму пополнения!");
                String dataInputMoney = scanner.nextLine();
                PaymentAccount paymentAccount = new PaymentAccount(person1);
                paymentAccount.putMoneyOnAccBalance(person1, dataInputTopUpAcc, dataInputMoney);
                paymentAccount.checkingAccountsBalance(person1);
            } else if (dataInput.equalsIgnoreCase("withdraw money from the Deposit")) {
                DepositAccount depositAccount = new DepositAccount();
                System.out.println("Укажите айди вклада");
                String idDep = scanner.nextLine();
                System.out.println("Укажите куда хотите перечислить вклад");
                String accTo = person1.getPaymentAccountNumber();
                System.out.println(accTo);
                System.out.println("Откуда взять деньги?");
                String accFrom = person1.getDepositAccountNumber();
                System.out.println(accFrom);
                depositAccount.getMoneyFromDeposit(idDep, person1, accTo, accFrom);
            } else if (dataInput.equalsIgnoreCase("put Money On Deposit")) {
                DepositAccount depositAccount = new DepositAccount();
                System.out.println("Откуда списать деньги?");
                String accFrom = person1.getPaymentAccountNumber();
                System.out.println("Куда перечислить деньги?");
                String accTo = person1.getDepositAccountNumber();
                System.out.println("Сколько денег положить на депозит?");
                String money = scanner.nextLine();
                depositAccount.putMoneyOnDeposit(person1, accFrom, accTo, money);
            } else if (dataInput.equalsIgnoreCase("Take money from Card account")) {
                CardAccount cardAccount = new CardAccount();
                System.out.println("С какого счёта снять деньги?");
                String accNumber = person1.getCardAccountNumber();
                System.out.println("Какая сумма?");
                String moneyAmount = scanner.nextLine();
                cardAccount.withdrawAccountWithCommission(person1, accNumber, moneyAmount);
                person1.getAllAccountBalance();
            } else if (dataInput.equalsIgnoreCase("see all list in bank")) {
                JointAccount.getOnPrintAllClient();
            }else if (dataInput.equalsIgnoreCase("open card acc")) {
                CardAccount cardAccount = new CardAccount(person1);
            }
        }
    }

}
