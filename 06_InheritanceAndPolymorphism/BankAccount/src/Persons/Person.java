package Persons;

import Accounts.*;

import java.math.BigDecimal;
import java.util.*;

public class Person {


    private String fullName;
    private int snilsNumber;
    private String id = "";

    private Map<String, BigDecimal> accountsNumberAndBalance = new TreeMap<>(); // AllaccNumber + money;

    private String paymentAccountNumber = ""; //Payment accNumber
    private String depositAccountNumber = ""; // accNumber ;
    private String cardAccountNumber = ""; // accNumber ;

    private Map<String, Map<String, Long>> depositId = new TreeMap<>(); // idDeposit + (Depnumber + dateDepIn)
    private Map<String, Long> depositNumberAndDepositTimeIn = new TreeMap<>(); //Depnumber + dateDepIn

    public Person() {

    }

    public Person(String fullName, int snilsNumber) {
        if (!JointAccount.checkPersonSnilsInBase(snilsNumber)) {
            this.fullName = fullName;
            this.snilsNumber = snilsNumber;
            setId();
        } else {
            this.snilsNumber = snilsNumber;
            this.id = "";
            System.out.println("Такой клиент уже есть!");
        }
    }

    private void setId() {
        boolean q = true;
        while (q) {
            int idNumber = (int) (Math.random() * 100_000);
            this.id = "id" + " " + idNumber;
            if (!JointAccount.checkingIdInBase(this.id)) {
                JointAccount.setIdInListIdAccount(this.id);
                System.out.println("id присвоен! - " + this.id);
                break;
            }
        }
    }

    public void getListAccountNumber() {
        if (!this.accountsNumberAndBalance.isEmpty()) {
            System.out.println("Счета клиента " + getId() + " " + getFullName() + " ");
            for (String accNumber : this.accountsNumberAndBalance.keySet()) {
                System.out.println(accNumber);
            }
        } else {
            System.out.println("У клиента " + this.fullName + " " + " нет счета!");
        }
    }

    public void setAccountNumberAndBalance(String accountNumber, BigDecimal money) {
        if (money.compareTo(BigDecimal.ZERO) <= 0) {
            accountsNumberAndBalance.put(accountNumber, new BigDecimal(0));
        } else {
            accountsNumberAndBalance.put(accountNumber, money);
        }
    }

    public void setPaymentAccountNumber(String accountNumber) {
        paymentAccountNumber = accountNumber;
    }

    public void setDepositAccountNumber(String accountNumber) {
        depositAccountNumber = accountNumber;
    }

    public void setCardAccountNumber(String accountNumber) {
        cardAccountNumber = accountNumber;
    }

    public String getPaymentAccountNumber() {
        return this.paymentAccountNumber;
    }

    public String getDepositAccountNumber() {
        return this.depositAccountNumber;
    }

    public String getCardAccountNumber() {
        return this.cardAccountNumber;
    }

    public void setDepositNumberAndDepositTimeIn(String depAccNumber, Long timeDepIn) {
        this.depositNumberAndDepositTimeIn.put(depAccNumber, timeDepIn);
    }

    public void setDepositId(String depositId, Map<String, Long> depositNumberAndDepositTimeIn) {
        this.depositId.put(depositId, depositNumberAndDepositTimeIn);
    }

    public void getAllAccountBalance() {
        for (String acc : this.accountsNumberAndBalance.keySet()) {
            System.out.println(acc + " " + this.accountsNumberAndBalance.get(acc));
        }
    }

    public Map <String, BigDecimal> getMapAccountBalance() {
        return this.accountsNumberAndBalance;
    }

    public Map getDepositIdAndDepositNumberAndDepositTimeIn() {
        return this.depositId;
    }

    public Map <String, Long> getDepositNumberAndDepositTimeIn() {
        return this.depositNumberAndDepositTimeIn;
    }

    public int getSnilsNumber() {
        return this.snilsNumber;
    }

    public String getFullName() {
        return this.fullName;
    }

    public String getId() {
        if (this.id.isEmpty()) {
            id = "Такого клиента нет!";
            return this.id;
        } else {
            return this.id;
        }
    }
}
