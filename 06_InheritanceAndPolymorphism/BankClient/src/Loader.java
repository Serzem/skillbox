import Accounts.*;
import Questionnaire.*;

import java.util.Scanner;


public class Loader {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        Questionnaire questionnaire;
        Client client;

        boolean start = true;

        while (start) {
            System.out.println("В какой Вам отдел?\nдля Физических лиц, Юридических лиц, ИП?");
            String dataInput = scanner.nextLine();

            if (dataInput.equalsIgnoreCase("Физических лиц")) {
                client = new PhysicalPerson();
//                Questionnaire questionnairePhysicalPerson = new QuestionnairePhysicalPerson();
//                PhysicalPerson physicalPerson = new PhysicalPerson();
                while (true) {
                    System.out.println("Какие операции хотите совершить?");
                    dataInput = scanner.nextLine();

                    if (dataInput.equalsIgnoreCase("Открыть счет")) {
                        System.out.println("Ваше имя?");
                        String name = scanner.nextLine();
                        System.out.println("Ваш СНИЛС?");
                        String snils = scanner.nextLine();
                        questionnaire = new QuestionnairePhysicalPerson(name, snils);
                        client = new PhysicalPerson(questionnaire);

                    } else if (dataInput.equalsIgnoreCase("exit")) {
                        System.out.println("Вы вышли из отдела для Физических лиц!");
                        break;

                    } else if (dataInput.equalsIgnoreCase("Проверить баланс")) {
                        System.out.println("Ваш id?");
                        dataInput = scanner.nextLine();
                        questionnaire = client.findPerson(dataInput);
                        System.out.println(questionnaire.getBalance());

                    } else if (dataInput.equalsIgnoreCase("Пополнить счет")) {
                        System.out.println("Ваш id?");
                        dataInput = scanner.nextLine();
                        System.out.println("Сколько денег положить?");
                        String money = scanner.nextLine();
                        System.out.println("Баланс до пополнения - " + client.getBalance(dataInput));
                        client.printTermsOfReplenishment(money);
                        System.out.println("Выполнить операцию?");
                        String question = scanner.nextLine();
                        if (question.equalsIgnoreCase("да")) {
                            client.putMoneyOnAccount(dataInput, money);

                        } else {
                            System.out.println("Операция отменена!");
                        }
                    } else if (dataInput.equalsIgnoreCase("Снять со счета")) {
                        System.out.println("Ваш id?");
                        dataInput = scanner.nextLine();
                        System.out.println("Сколько снять денег?");
                        String money = scanner.nextLine();
                        System.out.println("Баланс до снятия - " + client.getBalance(dataInput));
                        client.printTermsOfWithdraw(money);
                        System.out.println("Выполнить операцию?");
                        String question = scanner.nextLine();
                        if (question.equalsIgnoreCase("да")) {
                            client.withdrawMoneyFromAccount(dataInput, money);
                        } else {
                            System.out.println("Операция отменена!");
                        }
                    }
                }
            } else if (dataInput.equalsIgnoreCase("Юридических лиц")) {
//                QuestionnaireLegalEntity questionnaireLegalEntity = new QuestionnaireLegalEntity();
                client = new LegalEntity();
                while (true) {
                    System.out.println("Какие операции хотите совершить?");
                    dataInput = scanner.nextLine();

                    if (dataInput.equalsIgnoreCase("Открыть счет")) {
                        System.out.println("Название Вашей компании?");
                        String title = scanner.nextLine();
                        System.out.println("ИНН Вашей компании?");
                        String inn = scanner.nextLine();
                        questionnaire = new QuestionnaireLegalEntity(title, inn);
                        client = new LegalEntity(questionnaire);

                    } else if (dataInput.equalsIgnoreCase("exit")) {
                        System.out.println("Вы вышли из отдела для Юридических лиц!");
                        break;

                    } else if (dataInput.equalsIgnoreCase("Проверить баланс")) {
                        System.out.println("id Вашей компании?");
                        dataInput = scanner.nextLine();
                        questionnaire = client.findPerson(dataInput);
                        System.out.println(questionnaire.getBalance());

                    } else if (dataInput.equalsIgnoreCase("Пополнить счет")) {
                        System.out.println("id Вашей компании?");
                        dataInput = scanner.nextLine();
                        System.out.println("Сколько денег положить?");
                        String money = scanner.nextLine();
                        System.out.println("Баланс до пополнения - " + client.getBalance(dataInput));
                        client.printTermsOfReplenishment(money);
                        System.out.println("Выполнить операцию?");
                        String question = scanner.nextLine();
                        if (question.equalsIgnoreCase("да")) {
                            client.putMoneyOnAccount(dataInput, money);

                        } else {
                            System.out.println("Операция отменена!");
                        }
                    } else if (dataInput.equalsIgnoreCase("Снять со счета")) {
                        System.out.println("id Вашей компании?");
                        dataInput = scanner.nextLine();
                        System.out.println("Сколько снять денег?");
                        String money = scanner.nextLine();
                        System.out.println("Баланс до снятия - " + client.getBalance(dataInput));
                        client.printTermsOfWithdraw(money);
                        System.out.println("Выполнить операцию?");
                        String question = scanner.nextLine();
                        if (question.equalsIgnoreCase("да")) {
                            client.withdrawMoneyFromAccount(dataInput, money);
                        } else {
                            System.out.println("Операция отменена!");
                        }
                    }
                }
            } else if (dataInput.equalsIgnoreCase("ИП")) {
//                QuestionnaireIndividualEntrepreneur questionnaireIndividualEntrepreneur = new QuestionnaireIndividualEntrepreneur();
                client = new IndividualEntrepreneur();
                while (true) {
                    System.out.println("Какие операции хотите совершить?");
                    dataInput = scanner.nextLine();

                    if (dataInput.equalsIgnoreCase("Открыть счет")) {
                        System.out.println("Название Вашей компании?");
                        String title = scanner.nextLine();
                        System.out.println("ИНН Вашей компании?");
                        String inn = scanner.nextLine();
                        questionnaire = new QuestionnaireIndividualEntrepreneur(title, inn);
                        client = new IndividualEntrepreneur(questionnaire);

                    } else if (dataInput.equalsIgnoreCase("exit")) {
                        System.out.println("Вы вышли из отдела для Индивидульных предпринимателей!");
                        break;

                    } else if (dataInput.equalsIgnoreCase("Проверить баланс")) {
                        System.out.println("id Вашей компании?");
                        dataInput = scanner.nextLine();
                        questionnaire = client.findPerson(dataInput);
                        System.out.println(questionnaire.getBalance());

                    } else if (dataInput.equalsIgnoreCase("Пополнить счет")) {
                        System.out.println("id Вашего ИП?");
                        dataInput = scanner.nextLine();
                        System.out.println("Сколько денег положить?");
                        String money = scanner.nextLine();
                        System.out.println("Баланс до пополнения - " + client.getBalance(dataInput));
                        client.printTermsOfReplenishment(money);
                        System.out.println("Выполнить операцию?");
                        String question = scanner.nextLine();
                        if (question.equalsIgnoreCase("да")) {
                            client.putMoneyOnAccount(dataInput, money);

                        } else {
                            System.out.println("Операция отменена!");
                        }
                    } else if (dataInput.equalsIgnoreCase("Снять со счета")) {
                        System.out.println("id Вашего ИП?");
                        dataInput = scanner.nextLine();
                        System.out.println("Сколько снять денег?");
                        String money = scanner.nextLine();
                        System.out.println("Баланс до снятия - " + client.getBalance(dataInput));
                        client.printTermsOfWithdraw(money);
                        System.out.println("Выполнить операцию?");
                        String question = scanner.nextLine();
                        if (question.equalsIgnoreCase("да")) {
                            client.withdrawMoneyFromAccount(dataInput, money);
                        } else {
                            System.out.println("Операция отменена!");
                        }
                    }
                }

            } else if (dataInput.equalsIgnoreCase("exit")) {
                System.out.println("Вы вышли из приложения!");
                break;
            }
        }
    }
}
