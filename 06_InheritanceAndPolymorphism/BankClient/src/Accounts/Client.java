package Accounts;

import Questionnaire.Questionnaire;
import Questionnaire.QuestionnairePhysicalPerson;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public abstract class Client {


    private static Set<String> idBase = new HashSet<>();


    abstract protected void createNewAccountNumber(Questionnaire questionnaire);

    abstract protected void setAccountNumber();

    abstract protected void setInfoInBase(String id, Questionnaire questionnaire);

    abstract protected double getDepositingCommission(double amount);

    abstract protected double getWithdrawalComission(double amount);

    abstract public Questionnaire findPerson(String id);

    abstract public String getBalance(String id);

    abstract public void printTermsOfReplenishment(String money);

    abstract public void printTermsOfWithdraw(String money);

    public static boolean checkIdInBase(String id) {
        return idBase.contains(id);
    }

    public static void setIdInBase(String id) {
        if (!checkIdInBase(id)) {
            idBase.add(id);
        } else {
            System.out.println("Такой id уже есть в базе!");
        }
    }

    public void putMoneyOnAccount(String id, String money) {
        Questionnaire questionnaire = findPerson(id);
        Double money2 = Double.parseDouble(money);
        money2 = (money2 + getDepositingCommission(money2));
        questionnaire.setAccountBalance(BigDecimal.valueOf(money2));
        System.out.println("Баланс после пополнения - " + getBalance(questionnaire.getId()));
    }

    public void withdrawMoneyFromAccount(String id, String money) {
        double money2 = Double.parseDouble(money);
        money2 = (money2 + getWithdrawalComission(money2));
        Questionnaire questionnairePhysicalPerson = findPerson(id);
        if (getIsPossibleWithdrawMoney(questionnairePhysicalPerson, BigDecimal.valueOf(money2))) {
            questionnairePhysicalPerson.setAccountBalance(questionnairePhysicalPerson.getBigDecimalBalance().subtract(BigDecimal.valueOf(money2)));
            System.out.println("Баланс после снятия - " + getBalance(questionnairePhysicalPerson.getId()));
        } else {
            System.out.println("Недостаточно средств для списания!");
        }
    }

    private boolean getIsPossibleWithdrawMoney(Questionnaire questionnaire, BigDecimal money) {
        return (questionnaire.getBigDecimalBalance().subtract(money).compareTo(BigDecimal.ZERO) == 0) ||
                (questionnaire.getBigDecimalBalance().subtract(money).compareTo(BigDecimal.ZERO) > 0);
    }
}
