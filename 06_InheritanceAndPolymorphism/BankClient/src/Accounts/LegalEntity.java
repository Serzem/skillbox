package Accounts;

import Questionnaire.Questionnaire;

import java.math.BigDecimal;
import java.util.Map;
import java.util.TreeMap;

public class LegalEntity extends Client {

    private static Map<String, Questionnaire> baseLegalEntity = new TreeMap<String, Questionnaire>(); // id + QuestionnairePhysicalPerson
    private String accountNumber = "";
    private static final double COMISSIONFORPUTMONEY = 0;
    private static final double COMISSIONFORWITHDRAWMONEY = 1;

    public LegalEntity() {
    }

    public LegalEntity(Questionnaire questionnaire) {
        createNewAccountNumber(questionnaire);
    }

    @Override
    protected void createNewAccountNumber(Questionnaire questionnaire) {
        setAccountNumber();
        questionnaire.setAccountNumber(accountNumber);
        questionnaire.setAccountBalance(BigDecimal.valueOf(0));
        setInfoInBase(questionnaire.getId(), questionnaire);
    }

    @Override
    protected void setAccountNumber() {
        if (baseLegalEntity.isEmpty()) {
            accountNumber = "" + 1;
        } else {
            accountNumber = "" + (baseLegalEntity.size() + 1);
        }
    }

    @Override
    protected void setInfoInBase(String id, Questionnaire questionnaire) {
        baseLegalEntity.put(id, questionnaire);
    }

    @Override
    protected double getDepositingCommission(double money) {
        return ((money / 100) * COMISSIONFORPUTMONEY);
    }

    @Override
    protected double getWithdrawalComission(double money) {
        return ((money / 100) * COMISSIONFORWITHDRAWMONEY);
    }

    @Override
    public Questionnaire findPerson(String id) {
        return baseLegalEntity.get(id);
    }

    @Override
    public String getBalance(String id) {
        Questionnaire questionnaire = baseLegalEntity.get(id);
        return questionnaire.getBalance();
    }

    @Override
    public void printTermsOfReplenishment(String money) {
        Double money2 = Double.parseDouble(money);
        System.out.println("За выполнение данной операции с Вас будет удержана комиссия в - " + COMISSIONFORPUTMONEY + "%\nВ размере - " + getDepositingCommission(money2) + " руб.");
    }

    @Override
    public void printTermsOfWithdraw(String money) {
        Double money2 = Double.parseDouble(money);
        System.out.println("За выполнение данной операции с Вас будет удержана комиссия в - " + COMISSIONFORWITHDRAWMONEY + "%\nВ размере - " + getWithdrawalComission(money2) + " руб.");
    }
}
