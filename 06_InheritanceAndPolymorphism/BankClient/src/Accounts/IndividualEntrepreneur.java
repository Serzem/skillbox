package Accounts;

import Questionnaire.Questionnaire;

import java.math.BigDecimal;
import java.util.Map;
import java.util.TreeMap;

public class IndividualEntrepreneur extends Client {

    private static Map<String, Questionnaire> baseIndividualEntrepreneur = new TreeMap<String, Questionnaire>(); // id + QuestionnairePhysicalPerson
    private String accountNumber = "";
    private static final double COMISSIONFORPUTMONEY_LESS1000 = 1;
    private static final double COMISSIONFORPUTMONEY_OTHER = 0.5;
    private static final double COMISSIONFORWITHDRAWMONEY = 0;

    public IndividualEntrepreneur() {
    }

    public IndividualEntrepreneur(Questionnaire questionnaire) {
        createNewAccountNumber(questionnaire);
    }

    @Override
    protected void createNewAccountNumber(Questionnaire questionnaire) {
        setAccountNumber();
        questionnaire.setAccountNumber(accountNumber);
        questionnaire.setAccountBalance(BigDecimal.valueOf(0));
        setInfoInBase(questionnaire.getId(), questionnaire);
    }

    @Override
    protected void setAccountNumber() {
        if (baseIndividualEntrepreneur.isEmpty()) {
            accountNumber = "" + 1;
        } else {
            accountNumber = "" + (baseIndividualEntrepreneur.size() + 1);
        }
    }

    @Override
    protected void setInfoInBase(String id, Questionnaire questionnaire) {
        baseIndividualEntrepreneur.put(id, questionnaire);
    }

    @Override
    protected double getDepositingCommission(double money) {
        if (money < 1000) {
            return ((money / 100) * COMISSIONFORPUTMONEY_LESS1000);
        } else if (money >= 1000) {
            return ((money / 100) * COMISSIONFORPUTMONEY_OTHER);
        } else {
            return 0;
        }
    }

    @Override
    protected double getWithdrawalComission(double money) {
        return ((money / 100) * COMISSIONFORWITHDRAWMONEY);
    }

    @Override
    public Questionnaire findPerson(String id) {
        return baseIndividualEntrepreneur.get(id);
    }

    @Override
    public String getBalance(String id) {
       Questionnaire questionnaire = baseIndividualEntrepreneur.get(id);
        return questionnaire.getBalance();
    }

    @Override
    public void printTermsOfWithdraw(String money) {
        Double money2 = Double.parseDouble(money);
        System.out.println("За выполнение данной операции с Вас будет удержана комиссия в - " + COMISSIONFORWITHDRAWMONEY + "%\nВ размере - " + getWithdrawalComission(money2) + " руб.");
    }

    @Override
    public void printTermsOfReplenishment(String money) {
        Double money2 = Double.parseDouble(money);
        System.out.println("За выполнение данной операции с Вас будет удержана комиссия в - "
                + (getDepositingCommission(money2) >= 10 ? COMISSIONFORPUTMONEY_OTHER : COMISSIONFORPUTMONEY_LESS1000) + "%\nВ размере - " + getDepositingCommission(money2) + " руб.");
    }
}
