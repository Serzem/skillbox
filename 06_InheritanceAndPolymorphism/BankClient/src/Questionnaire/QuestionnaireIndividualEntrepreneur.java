package Questionnaire;

import java.math.BigDecimal;

public class QuestionnaireIndividualEntrepreneur extends Questionnaire {
    private String id = "";
    private String title = "";
    private String inn = "";

    private String accountNumber = "";
    private BigDecimal balance;

    public QuestionnaireIndividualEntrepreneur() {
    }

    public QuestionnaireIndividualEntrepreneur(String title, String inn) {
        if (!checkInnInBase(inn)) {
            this.title = title;
            this.inn = inn;
            setInnNumberInBase(inn);
            this.id = setId();
        } else {
            this.title = title;
            this.inn = inn;
            this.id = "";
            System.out.println("Такой предприниматель уже есть в базе!");
        }
    }

    @Override
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    @Override
    public void setAccountBalance(BigDecimal money) {
        if (money.compareTo(BigDecimal.ZERO) <= 0) {
            this.balance = new BigDecimal(0);
        } else {
            this.balance = money;
        }
    }

    @Override
    public String getId() {
        return this.id;
    }

    public String getBalance() {
        return this.accountNumber + " - " + this.balance;
    }

    public BigDecimal getBigDecimalBalance() {
        return this.balance;
    }
}
