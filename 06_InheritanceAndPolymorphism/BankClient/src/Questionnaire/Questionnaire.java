package Questionnaire;

import Accounts.Client;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

abstract public class Questionnaire {

    private static Set<String> snilsNumber = new HashSet<>();
    private static Set<String> innBase = new HashSet<>();

    abstract public void setAccountNumber(String accountNumber);

    abstract public String getId();

    abstract public void setAccountBalance(BigDecimal money);

    abstract public BigDecimal getBigDecimalBalance();

    abstract public String getBalance();

    protected void setSnilsNumberInBase (String snils){
        snilsNumber.add(snils);
    }

    protected void setInnNumberInBase (String snils){
        innBase.add(snils);
    }

    protected String setId() {
        boolean q = true;
        String id = "";
        while (q) {
            int idNumber = (int) (Math.random() * 100_000);
            id = "" + idNumber;
            if (!Client.checkIdInBase(id)) {
                Client.setIdInBase(id);
                System.out.println("id присвоен! - " + id);
                break;
            }
        }
        return id;
    }

    protected static boolean checkSnilsInBase(String snils) {
        return snilsNumber.contains(snils);
    }

    protected static boolean checkInnInBase(String id) {
        return innBase.contains(id);
    }


}
