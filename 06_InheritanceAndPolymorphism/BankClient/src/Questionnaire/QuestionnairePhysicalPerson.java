package Questionnaire;

//import Accounts.Client;
//import Accounts.PhysicalPerson;

import java.math.BigDecimal;

public class QuestionnairePhysicalPerson extends Questionnaire {

    private String id = "";
    private String name = "";
    private String snils = "";

    private String accountNumber = "";
    private BigDecimal balance;

    public QuestionnairePhysicalPerson() {
    }

    public QuestionnairePhysicalPerson(String name, String snils) {
        if (!checkSnilsInBase(snils)) {
            this.name = name;
            this.snils = snils;
            setSnilsNumberInBase(snils);
            this.id = setId();
        } else {
            this.name = name;
            this.snils = snils;
            this.id = "";
            System.out.println("Такой клиент уже есть в базе!");
        }
    }

    @Override
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    @Override
    public void setAccountBalance(BigDecimal money) {
        if (money.compareTo(BigDecimal.ZERO) <= 0) {
            this.balance = new BigDecimal(0);
        } else {
            this.balance = money;
        }
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public String getBalance(){
        return this.accountNumber + " - " + this.balance;
    }

    @Override
    public BigDecimal getBigDecimalBalance(){
        return this.balance;
    }
}
