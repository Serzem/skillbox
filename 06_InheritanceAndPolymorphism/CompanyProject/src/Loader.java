import Company.*;
import Staff.Employee;
import Staff.Manager;
import Staff.Operator;
import Staff.TopManager;
import org.w3c.dom.ls.LSOutput;

import javax.swing.text.html.HTMLDocument;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class Loader {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String dataInput;
        Company company;

        while (true) {
            System.out.println("Найти компанию по её названию или создать новую.");
            dataInput = scanner.nextLine();
            if (dataInput.equalsIgnoreCase("Создать новую")) {
                System.out.println("Напишите наименование компании");
                String nameCompany = scanner.nextLine();
                company = new Company(nameCompany);
                DatabaseOfCompanies.setCompaniesList(company);
                if (!company.getNameCompany().isEmpty()) {
                    System.out.println("Вы создали компанию - " + nameCompany);
                }
                while (true) {
                    System.out.println("Что сделать?");
                    dataInput = scanner.nextLine();
                    if (dataInput.equalsIgnoreCase("Нанять персонал")) {
                        System.out.println("Введите номер пункта персонала и его количество\n1 - Менеджер;\n2 - Операционист;\n3 - Топ Менеджер;\n4 - Всех сразу");
                        String option = scanner.nextLine();
                        Employee employee;
                        if (option.equalsIgnoreCase("1")) {
                            System.out.println("Сколько нанять Менеджеров?");
                            int count = Integer.parseInt(scanner.nextLine());
                            for (int i = 0; i < count; i++) {
                                employee = new Manager(company, ("Манагер " + i + " - " + (int) (Math.random() * 1000)));
                                company.hire(employee);
                                company.setCompanyAllMonthlyIncome(employee);
                            }
                        } else if (option.equalsIgnoreCase("2")) {
                            System.out.println("Сколько нанять Операционистов?");
                            int count = Integer.parseInt(scanner.nextLine());
                            for (int i = 0; i < count; i++) {
                                company.hire(new Operator(company, ("Опер " + i + " - " + (int) (Math.random() * 1000))));
                            }
                        } else if (option.equalsIgnoreCase("3")) {
                            System.out.println("Сколько нанять Топ Менеджеров?");
                            int count = Integer.parseInt(scanner.nextLine());
                            for (int i = 0; i < count; i++) {
                                company.hire(new TopManager(company, ("Топчик " + i + (int) (Math.random() * 1000))));
                            }
                        } else if (option.equalsIgnoreCase("4")) {
                            List<Employee> staffs = new ArrayList<>();
                            System.out.println("Сколько нанять Менеджеров?");
                            int count = Integer.parseInt(scanner.nextLine());
                            for (int i = 0; i < count; i++) {
                                employee = new Manager(company, ("Манагер " + i + " - " + (int) (Math.random() * 1000)));
                                staffs.add(employee);
                                company.setCompanyAllMonthlyIncome(employee);
                            }
                            System.out.println("Сколько нанять Операционистов?");
                            count = Integer.parseInt(scanner.nextLine());
                            for (int i = 0; i < count; i++) {
                                employee = new Operator(company, ("Опер " + i + " - " + (int) (Math.random() * 1000)));
                                staffs.add(employee);
                            }
                            System.out.println("Сколько нанять Топ Менеджеров?");
                            count = Integer.parseInt(scanner.nextLine());
                            for (int i = 0; i < count; i++) {
                                employee = new TopManager(company, ("Топчик " + i + " - " + (int) (Math.random() * 1000)));
                                staffs.add(employee);
                            }
                            company.hireAll(staffs);
                        }
                    } else if (dataInput.equalsIgnoreCase("Список - кто больше заработал")) {
                        System.out.println("Введите размер списка");
                        String listCount = scanner.nextLine();
                        int count = Integer.parseInt(listCount);
                        company.printListCountStaffBase(company.getTopSalaryStaff(), count);
                    } else if (dataInput.equalsIgnoreCase("Список - кто меньше заработал")) {
                        System.out.println("Введите размер списка");
                        String listCount = scanner.nextLine();
                        int count = Integer.parseInt(listCount);
                        company.printListCountStaffBase(company.getLowestSalaryStaff(), count);
                    } else if (dataInput.equalsIgnoreCase("Доход компании")) {
                        company.getIncome();
                    } else if (dataInput.equalsIgnoreCase("Уволить сотрудников")) {
                        System.out.println("Какой процент сотрудников уволить?");
                        String option = scanner.nextLine();
                        double percent = Double.parseDouble(option);

                        List<Employee> employees = company.getLowestSalaryStaff();

                        double countManager = 0;
                        double countOperator = 0;
                        double countTopManager = 0;

                        for (Employee empl : employees) {
                            if (empl instanceof Manager) {
                                countManager++;
                            }
                            if (empl instanceof Operator) {
                                countOperator++;
                            }
                            if (empl instanceof TopManager) {
                                countTopManager++;
                            }
                        }

                        int countToFireManager = (int) (countManager * (percent / 100.));
                        int countToFireOperator = (int) (countOperator * (percent / 100.));
                        int countToFireTopManager = (int) (countTopManager * (percent / 100.));

                        int sumPercentFromCountStaff = (int) (countManager + countOperator + countTopManager);
                        int sumPercentsFronCountToFireStaff = countToFireManager + countToFireOperator + countToFireTopManager;

                        if (sumPercentFromCountStaff != sumPercentsFronCountToFireStaff) {
                            countToFireOperator += (sumPercentFromCountStaff * (percent / 100.) - sumPercentsFronCountToFireStaff);
                        }

                        Iterator<Employee> iterator = company.getLowestSalaryStaff().iterator();
                        while (company.getLowestSalaryStaff().iterator().hasNext()) {
                            Employee employee = iterator.next();
                            if (countToFireManager == 0 && countToFireOperator == 0 && countToFireTopManager == 0) {
                                break;
                            } else if (employee instanceof TopManager && countToFireTopManager != 0) {
                                company.fire(employee);
                                countToFireTopManager--;
                            } else if (employee instanceof Operator && countToFireOperator != 0) {
                                company.fire(employee);
                                countToFireOperator--;
                            } else if (employee instanceof Manager && countToFireManager != 0) {
                                company.fire(employee);
                                countToFireManager--;
                            }
                        }
                    } else if (dataInput.equalsIgnoreCase("exit")) {
                        System.out.println("Вы вышли из карточки компании - " + company.getNameCompany());
                        break;
                    }
                }
            } else if (dataInput.equalsIgnoreCase("Найти компанию")) {
                System.out.println("Напишите название компании");
                String nameCompany = scanner.nextLine();
                company = DatabaseOfCompanies.findCompany(nameCompany);
                while (true) {
                    if (company.getNameCompany().isEmpty()) {
                        break;
                    }
                    System.out.println("Что сделать?");
                    dataInput = scanner.nextLine();
                    if (dataInput.equalsIgnoreCase("Нанять персонал")) {
                        System.out.println("Введите номер пункта персонала и его количество\n1 - Менеджер;\n2 - Операционист;\n3 - Топ Менеджер;\n4 - Всех сразу");
                        String option = scanner.nextLine();
                        Employee employee;
                        if (option.equalsIgnoreCase("1")) {
                            System.out.println("Сколько нанять Менеджеров?");
                            int count = Integer.parseInt(scanner.nextLine());
                            for (int i = 0; i < count; i++) {
                                employee = new Manager(company, ("Манагер " + i + " - " + (int) (Math.random() * 1000)));
                                company.hire(employee);
                                company.setCompanyAllMonthlyIncome(employee);
                            }
                        } else if (option.equalsIgnoreCase("2")) {
                            System.out.println("Сколько нанять Операционистов?");
                            int count = Integer.parseInt(scanner.nextLine());
                            for (int i = 0; i < count; i++) {
                                company.hire(new Operator(company, ("Опер " + i + " - " + (int) (Math.random() * 1000))));
                            }
                        } else if (option.equalsIgnoreCase("3")) {
                            System.out.println("Сколько нанять Топ Менеджеров?");
                            int count = Integer.parseInt(scanner.nextLine());
                            for (int i = 0; i < count; i++) {
                                company.hire(new TopManager(company, ("Топчик " + i + " - " + (int) (Math.random() * 1000))));
                            }
                        } else if (option.equalsIgnoreCase("4")) {
                            List<Employee> staffs = new ArrayList<>();
                            System.out.println("Сколько нанять Менеджеров?");
                            int count = Integer.parseInt(scanner.nextLine());
                            for (int i = 0; i < count; i++) {
                                employee = new Manager(company, ("Манагер " + i + " - " + (int) (Math.random() * 1000)));
                                staffs.add(employee);
                                company.setCompanyAllMonthlyIncome(employee);
                            }

                            System.out.println("Сколько нанять Операционистов?");
                            count = Integer.parseInt(scanner.nextLine());
                            for (int i = 0; i < count; i++) {
                                employee = new Operator(company, ("Опер " + i + " - " + (int) (Math.random() * 1000)));
                                staffs.add(employee);
                            }
                            System.out.println("Сколько нанять Топ Менеджеров?");
                            count = Integer.parseInt(scanner.nextLine());
                            for (int i = 0; i < count; i++) {
                                employee = new TopManager(company, ("Топчик " + i + " - " + (int) (Math.random() * 1000)));
                                staffs.add(employee);
                            }
                            company.hireAll(staffs);
                        }
                    } else if (dataInput.equalsIgnoreCase("Список - кто больше заработал")) {
                        System.out.println("Введите размер списка");
                        String listCount = scanner.nextLine();
                        int count = Integer.parseInt(listCount);
                        company.printListCountStaffBase(company.getTopSalaryStaff(), count);
                    } else if (dataInput.equalsIgnoreCase("Список - кто меньше заработал")) {
                        System.out.println("Введите размер списка");
                        String listCount = scanner.nextLine();
                        int count = Integer.parseInt(listCount);
                        company.printListCountStaffBase(company.getLowestSalaryStaff(), count);
                    } else if (dataInput.equalsIgnoreCase("Доход компании")) {
                        company.getIncome();
                    } else if (dataInput.equalsIgnoreCase("Уволить сотрудников")) {
                        System.out.println("Какой процент сотрудников уволить?");
                        String option = scanner.nextLine();
                        double percent = Double.parseDouble(option);

                        List<Employee> employees = company.getLowestSalaryStaff();

                        double countManager = 0;
                        double countOperator = 0;
                        double countTopManager = 0;

                        for (Employee empl : employees) {
                            if (empl instanceof Manager) {
                                countManager++;
                            }
                            if (empl instanceof Operator) {
                                countOperator++;
                            }
                            if (empl instanceof TopManager) {
                                countTopManager++;
                            }
                        }

                        int countToFireManager = (int) (countManager * (percent / 100.));
                        int countToFireOperator = (int) (countOperator * (percent / 100.));
                        int countToFireTopManager = (int) (countTopManager * (percent / 100.));

                        int sumPercentFromCountStaff = (int) (countManager + countOperator + countTopManager);
                        int sumPercentsFronCountToFireStaff = countToFireManager + countToFireOperator + countToFireTopManager;

                        if (sumPercentFromCountStaff != sumPercentsFronCountToFireStaff) {
                            countToFireOperator += (sumPercentFromCountStaff * (percent / 100.) - sumPercentsFronCountToFireStaff);
                        }

                        Iterator<Employee> iterator = company.getLowestSalaryStaff().iterator();
                        while (company.getLowestSalaryStaff().iterator().hasNext()) {
                            Employee employee = iterator.next();
                            if (countToFireManager == 0 && countToFireOperator == 0 && countToFireTopManager == 0) {
                                break;
                            } else if (employee instanceof TopManager && countToFireTopManager != 0) {
                                company.fire(employee);
                                countToFireTopManager--;
                            } else if (employee instanceof Operator && countToFireOperator != 0) {
                                company.fire(employee);
                                countToFireOperator--;
                            } else if (employee instanceof Manager && countToFireManager != 0) {
                                company.fire(employee);
                                countToFireManager--;
                            }
                        }
                    } else if (dataInput.equalsIgnoreCase("exit")) {
                        System.out.println("Вы вышли из карточки компании - " + company.getNameCompany());
                        break;
                    }
                }
            } else if (dataInput.equalsIgnoreCase("exit")) {
                System.out.println("Вы вышли из приложения!");
                break;
            }
        }

    }
}
