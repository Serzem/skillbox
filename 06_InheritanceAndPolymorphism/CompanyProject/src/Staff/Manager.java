package Staff;

import Company.Company;
import Company.*;

import java.text.DecimalFormat;

public class Manager implements Employee {

    private String name = "";

    private final double PERCENT = 0.05; /* 5% */
    private final int SALARY_ACCORDING_REGULAR_SCHEDULE = 55_000;

    private Company company;

    private double earningsForCompanyMonthly = 0.;

    public Manager() {
    }

    public Manager(Company company, String name) {
        this.name = name;
        this.company = company;
        setEarningsForCompanyMonthly();
    }

    private double generateCompanyMonthlyIncome() {
        double generateMoney = 0;
        while (true) {
            double random1 = Math.random() * 100_000_000;
            int gg = (int) random1;
            double hh = (double) gg / 100;
            if (hh >= 115_000 && hh <= 140_000) {
               generateMoney  = hh;
               break;
            }

        }
        return generateMoney;
    }

    @Override
    public void setEarningsForCompanyMonthly(){
        this.earningsForCompanyMonthly = generateCompanyMonthlyIncome();
    }

    @Override
    public double getEarningsFromStaffForCompanyMonthly(){
        return this.earningsForCompanyMonthly;
    }

    @Override
    public Double getCompanyAllMonthlyIncome() {
        return company.getCompanyAllMonthlyIncome();
    }

    @Override
    public Double getMonthSalary() {
        DecimalFormat decimalFormat = new DecimalFormat("###.##");
        return Double.parseDouble(decimalFormat.format((this.SALARY_ACCORDING_REGULAR_SCHEDULE + getMonthlyPercentIncome())).replace(",", "."));
    }

    @Override
    public double getMonthlyPercentIncome() {
        return this.earningsForCompanyMonthly * this.PERCENT;
    }

    @Override
    public String getName() {
        return this.name;
    }

}
