package Staff;

public interface Employee extends Comparable<Employee> {


    Double getMonthSalary();

    String getName();

    double getMonthlyPercentIncome();

    Double getCompanyAllMonthlyIncome();

    default double getEarningsFromStaffForCompanyMonthly() {
        return 0;
    }

    default void setEarningsForCompanyMonthly() {
    }

    @Override
    default int compareTo(Employee employee) {

        if (getMonthSalary() > employee.getMonthSalary()) {
            return -1;
        } else if (getMonthSalary() < employee.getMonthSalary()) {
            return 1;
        } else {
            return 0;
        }
    }
}
