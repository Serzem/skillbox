package Staff;


import Company.Company;

public class Operator implements Employee {

    private String name = "";

    private final double PERCENT = 0;
    private final int SALARY_ACCORDING_REGULAR_SCHEDULE = 40_000;
    private Company company;

    public Operator() {
    }

    public Operator(Company company, String name) {
        this.name = name;
        this.company = company;
    }

    @Override
    public Double getCompanyAllMonthlyIncome(){
        return company.getCompanyAllMonthlyIncome();
    }

    @Override
    public Double getMonthSalary() {
        return this.SALARY_ACCORDING_REGULAR_SCHEDULE + getMonthlyPercentIncome();
    }

    @Override
    public double getMonthlyPercentIncome() {
        return this.SALARY_ACCORDING_REGULAR_SCHEDULE * this.PERCENT;
    }

    @Override
    public String getName() {
        return this.name;
    }

}
