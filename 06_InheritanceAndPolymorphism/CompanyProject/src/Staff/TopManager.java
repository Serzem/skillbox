package Staff;


import Company.Company;

public class TopManager implements Employee {

    private String name = "";

    private final double COUNT_FOR_PREMIUM = 10_000_000.;
    private final double PERCENT = 1.5;
    private final int SALARY_ACCORDING_REGULAR_SCHEDULE = 60_000;
    private Company company;

    public TopManager() {
    }

    public TopManager(Company company, String name) {
        this.name = name;
        this.company = company;
    }

    @Override
    public Double getCompanyAllMonthlyIncome(){
        return company.getCompanyAllMonthlyIncome();
    }

    @Override
    public Double getMonthSalary() {
        return this.SALARY_ACCORDING_REGULAR_SCHEDULE + getMonthlyPercentIncome();
    }

    @Override
    public double getMonthlyPercentIncome() {
        if (getCompanyAllMonthlyIncome() > this.COUNT_FOR_PREMIUM) {
            return this.SALARY_ACCORDING_REGULAR_SCHEDULE * this.PERCENT;
        } else {
            return 0;
        }
    }

    @Override
    public String getName() {
        return this.name;
    }

}
