package Company;

import java.util.ArrayList;
import java.util.List;

public class DatabaseOfCompanies {
    private static List<Company> companiesList = new ArrayList<>();

    public static List<Company> getCompaniesList() {
        return companiesList;
    }

    public static void setCompaniesList(Company company) {
        companiesList.add(company);
    }

    public static Company findCompany(String nameCompany) {

        for (Company q : getCompaniesList()) {
            if (q.getNameCompany().equalsIgnoreCase(nameCompany)) {
                System.out.println("Вы открыли карточку компании - " + q.getNameCompany());
                return q;
            }
        }
        System.out.println("Компании с таким именем не найдена.");
        return new Company();
    }

    protected static boolean checkCompanyInBase(String nameCompany) {
        for (Company q : getCompaniesList()) {
            if (q.getNameCompany().equalsIgnoreCase(nameCompany)) {
                System.out.println("Компания с таким именем уже есть, выберете другое имя!");
                return true;
            }
        }
        return false;
    }

}
