package Company;

import Staff.*;

import java.text.DecimalFormat;
import java.util.*;

public class Company {

    private Double companyAllMonthlyIncome = 0.;
    private List<Employee> staffList = new ArrayList<Employee>();

    private String nameCompany = "";

    private DecimalFormat decimalFormat = new DecimalFormat("###,###.##");

    public Company() {
    }

    public Company(String name) {
        if (!DatabaseOfCompanies.checkCompanyInBase(name)) {
            this.nameCompany = name;
        }
    }

    public void setCompanyAllMonthlyIncome(Employee employee) {
        this.companyAllMonthlyIncome += employee.getEarningsFromStaffForCompanyMonthly();
    }

    public Double getCompanyAllMonthlyIncome() {
        return this.companyAllMonthlyIncome;
    }

    public String getNameCompany() {
        return this.nameCompany;
    }

    public void hire(Employee employee) {
        setStaffList(employee);
    }

    public void hireAll(List<Employee> staffs) {
        for (Employee employee : staffs) {
            setStaffList(employee);
        }
    }

    public void fire(Employee employee) {
        staffList.remove(employee);
    }

    public void getIncome() {
        System.out.println(decimalFormat.format(getCompanyAllMonthlyIncome()).replace(",", ".") + " руб.");
    }

    public List<Employee> getTopSalaryStaff() {
        Collections.sort(staffList);
        return staffList;
    }

    public List<Employee> getLowestSalaryStaff() {
        List<Employee> q = new ArrayList<>(staffList);
        q.sort(new Comparator<Employee>() {
            @Override
            public int compare(Employee employee, Employee t1) {
                if (employee.getMonthSalary().compareTo(t1.getMonthSalary()) != 0) {
                    return employee.getMonthSalary().compareTo(t1.getMonthSalary());
                }
                return 0;
            }
        });
        staffList = q;
        return staffList;
    }

    public void printListCountStaffBase(List<Employee> staffBase, int count) {

        if (count <= 0) {
            System.out.println("Вводимое число должно быть больше нуля!");
        } else if (staffBase.size() < count) {
            System.out.println("В этой компании нет столько сотрудников!");
        } else {
            for (Employee asd : staffBase) {
                if (count == 0) {
                    break;
                }
                System.out.println(asd.getName() + " - " + decimalFormat.format(asd.getMonthSalary()).replace(",", ".") + " руб.");
                count--;
            }
        }
    }

    private void setStaffList(Employee employee) {
        staffList.add(employee);
    }
}
