import ProjectExceptions.EmailException;
import ProjectExceptions.NumberItemsCommandException;
import ProjectExceptions.TelefonNumberException;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    private static String addCommand = "add Василий Петров " +
            "vasily.petrov@gmail.com +79215637722";
    private static String commandExamples = "\t" + addCommand + "\n" +
            "\tlist\n\tcount\n\tremove Василий Петров";
    private static String commandError = "Wrong command! Available command examples: \n" +
            commandExamples;
    private static String helpText = "Command examples:\n" + commandExamples;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        CustomerStorage executor = new CustomerStorage();

        for (; ; ) {
            String command = scanner.nextLine();
            try {
                String[] tokens = command.split("\\s+", 2);
                if (tokens[0].equals("add")) {
                    executor.addCustomer(tokens[1]);
                } else if (tokens[0].equals("list")) {
                    executor.listCustomers();
                } else if (tokens[0].equals("remove")) {
                    executor.removeCustomer(tokens[1]);
                } else if (tokens[0].equals("count")) {
                    System.out.println("There are " + executor.getCount() + " customers");
                } else if (tokens[0].equals("help")) {
                    System.out.println(helpText);
                } else {
                    System.out.println(commandError);
                }
            } catch (ArrayIndexOutOfBoundsException exception) {
                System.out.println(exception.getMessage());
                try {
                    throw new NumberItemsCommandException("Wrong format for entering! Available format for entering examples:\nadd Василий Петров vasily.petrov@gmail.com +79215637722", command);
                } catch (NumberItemsCommandException e) {
                    System.out.println("Your option - " + e.getCommand() + ".");
                    System.out.println(e.getMessage());
                }
            } catch (EmailException exception) {
                System.out.println("Your option - " + exception.getComponent() + ".");
                System.out.println(exception.getMessage());
            } catch (TelefonNumberException exception) {
                System.out.println("Your option - " + exception.getComponent() + ".");
                System.out.println(exception.getMessage());
            } catch (NumberItemsCommandException exception) {
                System.out.println("Your option - " + exception.getCommand() + ".");
                System.out.println(exception.getMessage());
            }
        }
    }
}
