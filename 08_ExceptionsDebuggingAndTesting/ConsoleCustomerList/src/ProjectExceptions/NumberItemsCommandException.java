package ProjectExceptions;

public class NumberItemsCommandException extends Exception{
    private String command;
    public String getCommand(){
        return command;
    }
    public NumberItemsCommandException(String message, String command){
        super(message);
        this.command = command;
    }
}
