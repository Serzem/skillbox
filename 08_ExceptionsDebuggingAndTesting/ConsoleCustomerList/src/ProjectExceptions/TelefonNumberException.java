package ProjectExceptions;

public class TelefonNumberException extends Exception {
    private String component;
    public String getComponent(){
        return component;
    }

    public TelefonNumberException(String message, String component){
        super(message);
        this.component = component;
    }
}
