package ProjectExceptions;

public class EmailException extends Exception {
    private String component;

    public String getComponent() {
        return component;
    }
    public EmailException(String message, String component){
        super(message);
        this.component = component;
    }
}
