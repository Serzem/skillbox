import ProjectExceptions.*;

import java.util.HashMap;

public class CustomerStorage {
    private HashMap<String, Customer> storage;

    public CustomerStorage() {
        storage = new HashMap<>();
    }

    public void addCustomer(String data) throws EmailException, TelefonNumberException, NumberItemsCommandException {
        String[] components = data.split("\\s+");
        if (components.length != 3){
            throw new NumberItemsCommandException("Wrong format for entering! Available format for entering examples:\nadd Василий Петров vasily.petrov@gmail.com +79215637722", data);
        }
        String name = components[0] + " " + components[1];
        if (!Formats.checkEmail(components[2])) {
            throw new EmailException("Wrong format e-mail! Available format e-mail examples:\nvasily.petrov@gmail.com", components[2]);
        }
        if (!Formats.checkTelNumber(components[3])) {
            throw new TelefonNumberException("Wrong format e-mail! Available format e-mail examples:\n+79215637722", components[3]);
        }
        storage.put(name, new Customer(name, components[3], components[2]));
    }

    public void listCustomers() {
        storage.values().forEach(System.out::println);
    }

    public void removeCustomer(String name) {
        storage.remove(name);
    }

    public int getCount() {
        return storage.size();
    }
}