import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Formats {

    private static Pattern pattern;

    public static boolean checkEmail(String email){
        pattern = Pattern.compile ("^([a-zA-Z0-9_.-]+)@([a-zA-Z0-9_.-]+)\\.([a-z.]{2,7})$");
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static boolean checkTelNumber(String email){
        pattern = Pattern.compile ("^\\+7(\\d{10})$");
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

}
