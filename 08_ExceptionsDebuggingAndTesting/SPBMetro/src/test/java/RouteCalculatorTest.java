import core.Line;
import core.Station;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class RouteCalculatorTest {
    StationIndex stationIndex = new StationIndex();
    RouteCalculator calculator;
    List<Station> route;
    List<Station> routeConnections = new ArrayList<>();
    Station a, b, c, d, e;

    List<Station> expect_1, expect_2, expect_3, expect_4 = new ArrayList<>();

    Line line1, line2, line3, line4, line5;

    @Before
    public void setUp() throws Exception {

        route = new ArrayList<>();

        //create lines
        line1 = new Line(1, "Первая");
        line2 = new Line(2, "Вторая");
        line3 = new Line(3, "Третья");
        line4 = new Line(4, "Четвертая");
        line5 = new Line(5, "Пятая");

        //create Station and add List route
        route.add(a = new Station("Площадь Ленина", line1));
        route.add(new Station("Чернышевская", line1));
        route.add(new Station("Площадь Восстания", line1));/* 1  3 */
        route.add(new Station("Владимирская", line1));/* 1  4 */
        route.add(b = new Station("Пушкинская", line1));
        route.add(new Station("Технологический институт", line1));

        route.add(new Station("Маяковская", line3)); /* 1  3*/
        route.add(c = new Station("Гостиный двор", line3));/* 2  3*/

        route.add(e = new Station("Горьковская", line2));
        route.add(d = new Station("Невский проспект", line2)); /* 2  3*/
        route.add(new Station("Сенная Площадь", line2));/* 4  2*/
        route.add(new Station("Технологический институт", line2));

        //add Stations and Lines in StationIndex
        for (Station q : route) {
            stationIndex.addStation(q);
            stationIndex.addLine(q.getLine());
        }

        //add Stations in lines
        route.forEach(station -> {
            if (station.getLine().compareTo(line1) == 0) {
                line1.addStation(station);
            }
            if (station.getLine().compareTo(line2) == 0) {
                line2.addStation(station);
            }
            if (station.getLine().compareTo(line3) == 0) {
                line3.addStation(station);
            }
            if (station.getLine().compareTo(line4) == 0) {
                line4.addStation(station);
            }
        });

        //add Connection Stations
        routeConnections.add(new Station("Площадь Восстания", line1));/* 1  3 */
        routeConnections.add(new Station("Маяковская", line3)); /* 1  3*/
        stationIndex.addConnection(routeConnections);
        routeConnections.clear();

        routeConnections.add(new Station("Невский проспект", line2)); /* 2  3*/
        routeConnections.add(new Station("Гостиный двор", line3));/* 2  3*/
        stationIndex.addConnection(routeConnections);
        routeConnections.clear();

        routeConnections.add(new Station("Сенная Площадь", line2));/* 2  4*/
        routeConnections.add(new Station("Спасская", line4)); /* 2  4*/
        stationIndex.addConnection(routeConnections);
        routeConnections.clear();

        routeConnections.add(new Station("Достоевская", line4)); /* 1  4*/
        routeConnections.add(new Station("Владимирская", line1));/* 1  4 */
        stationIndex.addConnection(routeConnections);
        routeConnections.clear();
    }

    public List<Station> getShortestRoute_No_Connections() {
        calculator = new RouteCalculator(stationIndex);

        System.out.println("|}{}{}{a, b}{}{}{|");
        expect_1 = calculator.getShortestRoute(a, b);
        expect_1.forEach(System.out::println);
        System.out.println("|}{}{}{}{}{}{}{}{|");
        return expect_1;
    }

    @Test
    public void calculateDuration_No_Connections() {
        double actual = RouteCalculator.calculateDuration(getShortestRoute_No_Connections());
        double expected = 10.0;
        Assert.assertEquals(expected, actual, 0);
    }

    public List<Station> getShortestRoute_With_One_Connection() {
        calculator = new RouteCalculator(stationIndex);

        System.out.println("|}{}{}{a, c}{}{}{|");
        expect_2 = calculator.getShortestRoute(a, c);
        expect_2.forEach(System.out::println);
        System.out.println("|}{}{}{}{}{}{}{}{|");
        return expect_2;
    }

    @Test
    public void calculateDuration_With_One_Connection() {
        double actual = RouteCalculator.calculateDuration(getShortestRoute_With_One_Connection());
        double expected = 8.5;
        Assert.assertEquals(expected, actual, 0);
    }

    public List<Station> getShortestRoute_With_Two_Connection_And_No_Stations() {
        calculator = new RouteCalculator(stationIndex);

        System.out.println("|}{}{}{a, d}{}{}{|");
        expect_3 = calculator.getShortestRoute(a, d);
        expect_3.forEach(System.out::println);
        System.out.println("|}{}{}{}{}{}{}{}{|");
        return expect_3;
    }

    @Test
    public void calculateDuration_With_Two_Connection_And_No_Stations() {
        double actual = RouteCalculator.calculateDuration(getShortestRoute_With_Two_Connection_And_No_Stations());
        double expected = 9.5;
        Assert.assertEquals(expected, actual, 0);
    }

    public List<Station> getShortestRoute_With_Two_Connection_And_One_Stations() {
        calculator = new RouteCalculator(stationIndex);

        System.out.println("|}{}{}{a, e}{}{}{|");
        expect_4 = calculator.getShortestRoute(a, e);
        expect_4.forEach(System.out::println);
        System.out.println("|}{}{}{}{}{}{}{}{|");
        return expect_4;
    }

    @Test
    public void calculateDuration_With_Two_Connection_And_One_Stations() {
        double actual = RouteCalculator.calculateDuration(getShortestRoute_With_Two_Connection_And_One_Stations());
        double expected = 12.0;
        Assert.assertEquals(expected, actual, 0);
    }

    @After
    public void tearDown() throws Exception {

    }


}
